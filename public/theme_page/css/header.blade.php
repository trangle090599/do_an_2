<div class="top-nav-fixed" style="position: fixed;width: 100%;top: 0;left: 0;right: 0;z-index: 1000;">
    <div class="top-contact">
        <div class="max-top-head">
            <div class="uk-clearfix">
                <ul class="infor-top" style="float: left;">
                    <li>
                        <a href="tel:(949) 480-1639"><i class="icon-phone2"></i>
                            <span>(+84<span></span>) 936<span></span> 160 019</span></a></li>
                    <li>
                        <a class="trigger-scroll" href="mailto:contact@thuanthanhtech.com"><i class="icon-paper-plane"
                                                                                              aria-hidden="true"></i>
                            contact@thuanthanhtech.com</a>
                    </li>
                </ul>

            </div>
        </div>
    </div>
    <div class="top-menu">
        <div class="max-top-head">
            <div class="row">
                <div class="col-md-2 d-flex align-items-center align-middle">
                    <a class="navbar-brand" href="{{route('home')}}"><img src="theme_page/images/logo.png" style="height: 50px;"></a>
                </div>
                <nav class="col-md-10 navbar navbar-expand-lg navbar-dark  ftco-navbar-light" id="ftco-navbar">
                    <div class="container d-flex align-items-center">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                                aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="oi oi-menu"></span> Menu
                        </button>
                        <div class="collapse navbar-collapse" id="ftco-nav">
                            <ul class="navbar-nav mr-auto">
                                {{--<li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Về Chúng Tôi
                                    </a>

                                    <div class="dropdown-menu bg-light" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{route('about')}}">Giới thiệu chung</a>
                                        <a class="dropdown-item" href="{{route('giatri')}}">Giá trị cốt lõi</a>
                                        <a class="dropdown-item" href="{{route('baomat')}}">Thông tin bảo mật</a>
                                        <a class="dropdown-item" href="{{route('dieukhoan')}}">Điều khoản sử dụng</a>
                                    </div>
                                </li>--}}
                            
                               {{--  @foreach($categories as $category)
                                    @if(count($category->type_article)==1)
                                        @foreach($category->type_article as $ta)
                                            <li class="nav-item"><a href="loaitin/{{$ta->id}}/{{$ta->a_slug_type}}.html"
                                                                    class="nav-link">{{$ta->a_name_type}}</a></li>
                                        @endforeach
                                    @endif
                                @endforeach --}}

                        
                    @if(isset($categories))
                                @foreach($categories as $category)
                                    {{-- @if(count($category->type_article)>0) --}}
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                {{$category->c_name}}
                                            </a>

                                            <div class="dropdown-menu bg-light" aria-labelledby="navbarDropdown">
                                                @foreach($category->type_article as $ta)
                                                    <a class="dropdown-item"
                                                       href="loaitin/{{$ta->id}}/{{$ta->a_slug_type}}.html">{{$ta->a_name_type}}</a>
                                                @endforeach
                                            </div>
                                        </li>
                                    {{-- @endif --}}
                                @endforeach

                    @endif
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('blog')}}">Blog</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('lienhe')}}">Liên Hệ</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
    </div>
    </div>
</div>

<!-- Load Facebook SDK for JavaScript -->
      <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v6.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your customer chat code -->
      <div class="fb-customerchat"
        attribution=setup_tool
        page_id="1883103911971569"
  logged_in_greeting="Chào bạn !"
  logged_out_greeting="Chào bạn !">
      </div>
