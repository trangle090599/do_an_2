<?php


namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\slide;
use App\sach;
use App\the_loai_sach;
use App\Cart;
use App\user;
use App\hoa_don;
use App\hoa_don_chi_tiet;
use App\nganh;
use App\phong;
use Session;

class Homepage_Controller extends Controller
{
	 
    public function getIndex()
	{
		$slide= slide::all();
		$sach=sach::all();
		$sach_moi = sach::where('trang_thai',1)->paginate(4);
		$sach_khuyen_mai = sach::where('trang_thai',3)->paginate(4);
		return view('homepase.trang_chu', compact('slide','sach_moi','sach_khuyen_mai'));
	}
	public function getThe_loai_sach($type)
	{
		$the_loai_sach= sach::where('ma_the_loai_sach',$type)->paginate(3); 
		$menu_loai=the_loai_sach::all();
		$ten_loai = the_loai_sach::where('ma_the_loai_sach',$type)->first();
		$ten_phong = phong::all();
		return view('homepase.loai_sach',compact('the_loai_sach','menu_loai','ten_loai','ten_phong'));
	}
	public function getChi_tiet_sach(Request $req)
	{
		$chi_tiet_san_pham= sach::where('ma_sach',$req->ma_sach )->first();
		$san_pham_cung_loai =sach::where('ma_the_loai_sach',$chi_tiet_san_pham->ma_the_loai_sach)->paginate(3);
		$ten_phong = phong::all();
		return view('homepase.chi_tiet_sach',compact('chi_tiet_san_pham','san_pham_cung_loai','ten_phong'));
	}
	public function getAdd_to_cart(Request $req,$ma_sach)
	{
		$product = sach::find($ma_sach);
		$oldcart = Session('cart')?Session::get('cart'):null;
		$cart = new Cart($oldcart);
		$cart ->add($product, $ma_sach);
		$req ->session()->put('cart', $cart);
		return redirect()->back();
		
	}

	public function getDelitemcart($ma_sach)
	{
		$oldcart = Session::has('cart')?Session::get('cart'):null;
		$cart = new Cart($oldcart);
		$cart->removeItem($ma_sach);
		if(count($cart->items)>0){
			Session::put('cart',$cart);
		}
		else{
			Session::forget('cart');
		}
		
		return redirect()->back();
	}

	public function getCheckout()
	{
		$hoa_don = hoa_don::all();
		$user = user::all();
		
		return view('homepase.dat_hang',[
			'user'=> $user,
			'hoa_don'=> $hoa_don,
			
		]);
	}

	public function postCheckout(Request $request)
	{
		
		$cart= Session::get('cart');
		
		$hd =new hoa_don;
		$hd->ma_user = $request->user;
		$hd->ngay_mua = $request->ngay_mua;
		$hd->sdt = $request->sdt;
		$hd->tong_tien= $cart->totalPrice;
		$hd->hinh_thuc_thanh_toan= $request->payment_method;
		$hd->chu_thich=$request->notes;
		$hd->save();

		foreach ($cart->items as $key => $value) {
			$hdct= new hoa_don_chi_tiet;
			$hdct->ma_hoa_don =$hd->ma_hoa_don;
			$hdct->ma_sach =$key;
			$hdct->so_luong = $value['qty'];
			$hdct->gia= $value['price']/= $value['qty'];
			$hdct->save();
		}
		Session::forget('cart');
	 return redirect()->back()->with('thongbao','đặt hàng thành công');
		
	}
	
	public function getDangky()
	{
		$user = user::all();
		$nganh = nganh::all();
		return view('homepase.dang_ky',[
			'user'=> $user,
			'nganh'=> $nganh 
		]);

	}
	public function postDangky(Request $request)
	{
		$this->validate($request, [
			'nganh'=>'required',
			'ten_user' =>'required|min:3',
			'email' =>'required|email|unique:user,email',
			'dia_chi' => 'required|min:5|max:100',
			'ngay_sinh' => 'required',
			'sdt' => 'required',
			'gioi_tinh' => 'required',
			'cap_do' => 'required',
			'password' =>'required|min:5|max:20',
			'passwordAgain' =>'required|same:password',
			
		],
		[
			'nganh.required' =>'Bạn chưa chọn tên ngành ',
			'ten_user.required' =>'Bạn chưa nhập tên ',
			'ten_user.min' =>'Tên người dùng phải có ít nhất 3 kí tự',

			'email.required' =>'Bạn chưa nhập Email',
			'email.email' =>'Bạn chưa nhập Đúng Email',
			'email.unique' =>'Email của bạn không tồn tại hoặc đã có tài khoản ',
			'dia_chi.min.required' =>'Bạn chưa nhập địa chỉ',
			'dia_chi.min' =>'Địa chỉ của bạn phải có ít nhất 3 kí tự',
			'ngay_sinh.required' =>'Bạn chưa nhập ngày sinh',
			'sdt.required' =>'Bạn chưa nhập số điện thoại ',	
			'gioi_tinh.required' =>'Bạn chưa nhập giới tính',
			'cap_do.required' =>'Bạn chưa nhập cấp độ',
			'password.required' =>'Bạn chưa nhập mật khẩu',
			'password.min' =>'Mật khẩu phải có ít nhất 5 kí tự',
			'password.max' =>'Mật khẩu phải có tối đa 20 kí tự',
			'passwordAgain.required' =>'Bạn chưa nhập lại mật khẩu',
			'passwordAgain.same' =>'Mật khẩu nhập lại chưa khớp',
			
		]);
		$dg = new user;
		$dg->ma_nganh = $request->nganh;
		$dg->ten_user = $request->ten_user;
		$dg->email = $request->email;
		$dg->dia_chi = $request->dia_chi;
		$dg->ngay_sinh= $request->ngay_sinh;
		
		$dg->gioi_tinh = $request->gioi_tinh;
		$dg->cap_do = $request->cap_do;
		$dg->sdt = $request->sdt;
		$dg->password = bcrypt($request->password);
		$dg->save();

		return redirect()->back()->with('thongbao','Đăng ký thành công');
	}
	

public function Dang_nhap_user()
	{
		return view('homepase.dang_nhap');
	}
	public function postDang_nhap_user(Request $request)
	{
		$this->validate($request, [
			
			'email' =>'required',
			'password' =>'required|min:5|max:20',

			
		],
		[

			'email.required' =>'Bạn chưa nhập Email',
			'password.required' =>'Bạn chưa nhập mật khẩu',
			'password.min' =>'Mật khẩu phải có ít nhất 5 kí tự',
			'password.max' =>'Mật khẩu phải có tối đa 20 kí tự',
			
		]);

		if(Auth::attempt(['email'=>$request->email,'password'=>$request->password]))
		{
			return redirect('gioi_thieu');
			
		}	
		else
		{
			return redirect('dang_nhap')->with('thongbao','đăng nhập không thành công');
		}
	}
	public function getLogout() {
		Auth::logout();
		return redirect('index');
	}

	public function getLien_he()
	{
		return view('homepase.lien_he');
	}
	public function getGioi_thieu()
	{
		return view('homepase.gioi_thieu');
	}

}
