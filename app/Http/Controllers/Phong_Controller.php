<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\phong;
use App\vat_chat;
use Illuminate\Support\Facades\HTML;
class Phong_Controller extends Controller
{
    public function Danh_sach_phong()
	{

		$phong = phong::all();
		
		
		return view('admin/phong/danh_sach_phong', ['phong'=> $phong]);

		// dd($phong);

	}
	
	public function Them_phong()

	{
		$vat_chat= vat_chat::all();
		$phong= phong::all();
		return view('admin/phong/them_phong',['vat_chat'=> $vat_chat]);
	}
	public function postThem_phong(Request $request)
	{
		$this->validate($request, [
			
			'ten_phong' =>'required|min:3',
			'vat_chat' =>'required',
			'so_luong_vc' =>'required',
			
			 
		],
		[	
			'ten_phong.required' =>'Bạn chưa nhập tên phòng ',

			'ten_phong.min' =>'Tên phòng phải có ít nhất 3 kí tự',
			'vat_chat.required' =>'Bạn chưa chọn tên dụng cụ ',
			'so_luong_vc.required' =>'Bạn chưa số lượng ',
			
		]);
		$phong = new phong;
		
		$phong->ten_phong = $request->ten_phong;
		$phong->ma_vat_chat = $request->vat_chat;
		$phong->so_luong_vc = $request->so_luong_vc;
		$phong->save();

		return redirect('tong/phong/them_phong')->with('thongbao','Thêm thành công');
	}
	public function Sua_phong($ma_phong)
	{
		$vat_chat= vat_chat::all();
		$phong = phong::find($ma_phong);
		return view('admin/phong/sua_phong',['phong' => $phong,
			'vat_chat'=> $vat_chat
	]);
	}

	public function postSua_phong(Request $request,$ma_phong)
	{
		$this->validate($request, [
			'ma_vat_chat' =>'required',
			'ten_phong' =>'required|min:3',
			'so_luong_vc' =>'required',
			
			 
		],
		
			
		[	'ma_vat_chat.required' =>'Bạn chưa chọn tên dụng cụ ',
			'ten_phong.required' =>'Bạn chưa nhập tên phòng ',
			'ten_phong.min' =>'Tên phòng phải có ít nhất 3 kí tự',
			
			'so_luong_vc.required' =>'Bạn chưa số lượng ',
			
		]);
		$phong = phong::find($ma_phong);
		$phong->ma_vat_chat = $request->vat_chat;
		$phong->ten_phong = $request->ten_phong;
		$phong->so_luong_vc = $request->so_luong_vc;
		
		$phong->save();

		return redirect('tong/phong/sua_phong/'.$ma_phong)->with('thongbao','sửa thành công');
	}
	public function Xoa_phong($id)
	{
		$phong = phong::find($id);
		$phong->delete();
		return redirect('tong/phong/danh_sach_phong')->with('thongbao','xóa thành công');
	}
}
