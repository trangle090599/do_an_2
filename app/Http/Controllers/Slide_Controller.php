<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\slide;

class Slide_controller extends Controller
{
    public function Danh_sach_slide()
	{
		$slide= slide::all();
		return view('admin/slide/danh_sach_slide', ['slide'=> $slide]);
	}
	public function Them_slide()
	{	
		
		return view('admin/slide/them_slide');
	}
	public function postThem_slide(Request $request)

	{
		$this->validate($request, [
			
			'link' =>'required|min:3',
			'anh'=>'required',
			
		],
		[
			
			'link.required' =>'Bạn chưa link ',
			
			
		]);
		$sl = new slide;
		
		$sl->link = $request->link;
		if($request->has('anh')){
			$file = $request->file('anh');

			$duoi = $file->getClientOriginalExtension();
			if($duoi != 'jpg' && $duoi != 'png'){

			}

			$name = $file->getClientOriginalName();
			$anh = str_random(4)."_".$name;
			while(file_exists("img/".$anh)){
				$anh = str_random(4)."_".$name;
			}

			$file->move('img_slide', $anh);
			$sl->anh = $anh;
		}else{
			$sl->anh = "";
		}
		
		$sl->save();

		return redirect('tong/slide/them_slide')->with('thongbao','Thêm thành công');
	}
	public function Sua_slide($ma_slide)
	{
		
		$slide = slide::find($ma_slide);
		return view('admin/slide/sua_slide',[
			
			'slide'=>$slide
		]);
	}
	public function postSua_slide(Request $request,$ma_slide)
	{
		$this->validate($request, [
			
			'link' =>'required|min:3',
			'anh'=>'required',
			
		],
		[
			
			'link.required' =>'Bạn chưa link ',
			
			
		]);

		$sl = slide::find($ma_slide);
		
		$sl->link = $request->link;
		if($request->has('anh')){
			$file = $request->file('anh');

			$duoi = $file->getClientOriginalExtension();
			if($duoi != 'jpg' && $duoi != 'png'){

			}

			$name = $file->getClientOriginalName();
			$anh = str_random(4)."_".$name;
			while(file_exists("img/".$anh)){
				$anh = str_random(4)."_".$name;
			}
			$file->move('img_slide', $anh);
			$sl->anh = $anh;
		}

		$sl->save();


		return redirect('tong/slide/sua_slide/'.$ma_slide)->with('thongbao','sửa thành công');
	}
	public function Xoa_slide($id)
	{
		$sl = slide::find($id);
		$sl->delete();
		return redirect('tong/slide/danh_sach_slide')->with('thongbao','xóa thành công');
	}

}
