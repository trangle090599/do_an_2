<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\vat_chat;
use App\phong;
use Illuminate\Support\Facades\HTML;
class Vat_chat_Controller extends Controller
{
      public function Danh_sach_vat_chat()
	{
		$vat_chat= vat_chat::all();
		return view('admin/vat_chat/danh_sach_vat_chat', ['vat_chat'=> $vat_chat]);
	}
	public function Them_vat_chat()
	{

		return view('admin/vat_chat/them_vat_chat');
	}
	public function postThem_vat_chat(Request $request)

	{
		$this->validate($request, [
			'ten_vat_chat'=>'required',
			'so_luong' => 'required',
			'gia_tien' => 'required',


		],
		[
			'ten_vat_chat.required' =>'Bạn chưa chọn tên vật chất ',
			'so_luong.required' =>'Bạn chưa nhập số lương',

			'gia_tien.required' =>'Bạn chưa nhập giá tiền',


		]);
		$vc = new vat_chat;

		$vc->ten_vat_chat = $request->ten_vat_chat;
		$vc->so_luong = $request->so_luong;
		$vc->gia_tien = $request->gia_tien;
		$vc->save();

		return redirect('tong/vat_chat/them_vat_chat')->with('thongbao','Thêm thành công');
	}

public function Sua_vat_chat($ma_vat_chat)
	{
		$vat_chat = vat_chat::find($ma_vat_chat);
		return view('admin/vat_chat/sua_vat_chat',['vat_chat' => $vat_chat]);
	}

	public function postSua_vat_chat(Request $request,$ma_vat_chat)
	{
		$this->validate($request, [
			'ten_vat_chat' =>'required|min:2',
			'so_luong'	=>'required',
			'gia_tien' => 'required',
		],
		[
			'ten_vat_chat.required' =>'Bạn chưa nhập tên dụng cụ ',
			'ten_vat_chat.min' =>'Tên dụng cụ có ít nhất 2 kí tự',
			'so_luong.required' =>'Bạn chưa nhập số lương ',
			'gia_tien.required' =>'Bạn chưa nhập giá tiền',

		]);
		$vat_chat = vat_chat::find($ma_vat_chat);
		$vat_chat->ten_vat_chat = $request->ten_vat_chat;
		$vat_chat->so_luong = $request->so_luong;
		$vat_chat->gia_tien = $request->gia_tien;
		$vat_chat->save();

		return redirect('tong/vat_chat/sua_vat_chat/'.$ma_vat_chat)->with('thongbao','sửa thành công');
	}
	public function Xoa_vat_chat($id)
	{
		$vat_chat = vat_chat::find($id);
		$vat_chat->delete();
		return redirect('tong/vat_chat/danh_sach_vat_chat')->with('thongbao','xóa thành công');
	}
}

