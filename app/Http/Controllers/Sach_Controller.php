<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\sach;
use App\the_loai_sach;
use App\tac_gia;
use App\nha_xuat_ban;
use App\phong;
use Illuminate\Support\Facades\HTML;


class Sach_Controller extends Controller
{
     public function Danh_sach_sach()
	{
		$sach= sach::all();
		return view('admin/sach/danh_sach_sach', ['sach'=> $sach]);
	}
	public function Them_sach()
	{	
		$the_loai_sach= the_loai_sach::all();
		$tac_gia= tac_gia::all();
		$nha_xuat_ban= nha_xuat_ban::all();
		$phong= phong::all();
		
		return view('admin/sach/them_sach',[
			'the_loai_sach'=> $the_loai_sach, 
			'tac_gia'=> $tac_gia,
			'nha_xuat_ban'=> $nha_xuat_ban,
			'phong'=>$phong,
			
		]);
	}
	public function postThem_sach(Request $request)

	{
		$this->validate($request, [
			'the_loai_sach'=>'required',
			'tac_gia'=>'required',
			'nha_xuat_ban'=>'required',
			'phong'=>'required',
			'ten_sach' =>'required|min:3',
			'anh'=>'required',
			'gioi_thieu' =>'required|min:10',
			'so_luong' => 'required|min:1',
			'ngay_nhap' => 'required',
			
			'gia_tien_cu' => 'required',
			'noi_dung' => 'required',
			'trang_thai'=>'required',

			
		],
		[
			'the_loai_sach.required' =>'Bạn chưa chọn tên thể loại sách ',
			'tac_gia.required' =>'Bạn chưa chọn tên tác giả ',
			'nha_xuat_ban.required' =>'Bạn chưa chọn tên nhà xuất bản ',
			'phong.required'=>'Bạn chưa chọn tên phòng ',
			'ten_sach.required' =>'Bạn chưa nhập tên ',
			'ten_sach.min' =>'Tên sách phải có ít nhất 3 kí tự',
			'gioi_thieu.min.required' =>'Bạn chưa nhập giới thiệu',
			'gioi_thieu.min' =>'Giới thiệu của bạn phải có ít nhất 3 kí tự',
			'ngay_nhap.required' =>'Bạn chưa nhập ngày nhập kho',
			
			'gia_tien_cu.required' =>'Bạn chưa nhập giá tiền',
			'noi_dung.required' =>'Bạn chưa nhập nội dung',
			'trang_thai.required' =>'Bạn chưa nhập trạng thái sản phẩm',
			
		]);
		$sh = new sach;
		$sh->ma_the_loai_sach = $request->the_loai_sach;
		$sh->ma_tac_gia = $request->tac_gia;
		$sh->ma_nha_xuat_ban = $request->nha_xuat_ban;
		$sh->ma_phong=$request->phong;
		$sh->ten_sach = $request->ten_sach;
		if($request->has('anh')){
			$file = $request->file('anh');

			$duoi = $file->getClientOriginalExtension();
			if($duoi != 'jpg' && $duoi != 'png'){

			}

			$name = $file->getClientOriginalName();
			$anh = str_random(4)."_".$name;
			while(file_exists("img/".$anh)){
				$anh = str_random(4)."_".$name;
			}

			$file->move('img', $anh);
			$sh->anh = $anh;
		}else{
			$sh->anh = "";
		}
		$sh->gioi_thieu = $request->gioi_thieu;
		$sh->so_luong = $request->so_luong;
		$sh->ngay_nhap= $request->ngay_nhap;
		$sh->gia_tien_moi = $request->gia_tien_moi;
		$sh->gia_tien_cu = $request->gia_tien_cu;
		$sh->noi_dung = $request->noi_dung;
		$sh->trang_thai = $request->trang_thai;
		$sh->save();

		return redirect('tong/sach/them_sach')->with('thongbao','Thêm thành công');
	}
	public function Sua_sach($ma_sach)
	{
		$the_loai_sach= the_loai_sach::all();
		$tac_gia= tac_gia::all();
		$nha_xuat_ban= nha_xuat_ban::all();
		$phong= phong::all();
		$sach = sach::find($ma_sach);
		return view('admin/sach/sua_sach',[
			'the_loai_sach'=> $the_loai_sach, 
			'tac_gia'=> $tac_gia,
			'nha_xuat_ban'=> $nha_xuat_ban,
			'phong'=>$phong,
			'sach'=>$sach
		]);
	}
	public function postSua_sach(Request $request,$ma_sach)
	{
		$this->validate($request, [
			'ma_the_loai_sach'=>'required',
			'ma_tac_gia'=>'required',
			'ma_nha_xuat_ban'=>'required',
			'ma_phong'=>'required',
			'ten_sach' =>'required|min:3',
			'gioi_thieu' =>'required|min:10',
			'so_luong' => 'required|min:1',
			'ngay_nhap' => 'required',
			
			'gia_tien_cu' => 'required',
			'noi_dung' => 'required',
			'trang_thai'=>'required',
		],
		[
			'ma_the_loai_sach.required' =>'Bạn chưa chọn tên thể loại sách ',
			'ma_tac_gia.required' =>'Bạn chưa chọn tên tác giả ',
			'ma_nha_xuat_ban.required' =>'Bạn chưa chọn tên nhà xuất bản ',
			'ma_phong.required'=>'Bạn chưa chọn tên phòng ',
			'ten_sach.required' =>'Bạn chưa nhập tên ',
			'ten_sach.min' =>'Tên sách phải có ít nhất 3 kí tự',
			'gioi_thieu.min.required' =>'Bạn chưa nhập giới thiệu',
			'gioi_thieu.min' =>'Giới thiệu của bạn phải có ít nhất 3 kí tự',
			'ngay_nhap.required' =>'Bạn chưa nhập ngày nhập kho',
			
			'gia_tien_cu.required' =>'Bạn chưa nhập giá tiền',
			'noi_dung.required' =>'Bạn chưa nhập nội dung',
			'trang_thai.required' =>'Bạn chưa nhập trạng thái sản phẩm',
			
		]);
		$sh = sach::find($ma_sach);
		$sh->ma_the_loai_sach = $request->ma_the_loai_sach;
		$sh->ma_nha_xuat_ban = $request->ma_nha_xuat_ban;
		$sh->ma_tac_gia = $request->ma_tac_gia;
		$sh->ma_phong = $request->ma_phong;
		$sh->ten_sach = $request->ten_sach;
		if($request->has('anh')){
			$file = $request->file('anh');

			$duoi = $file->getClientOriginalExtension();
			if($duoi != 'jpg' && $duoi != 'png'){

			}

			$name = $file->getClientOriginalName();
			$anh = str_random(4)."_".$name;
			while(file_exists("img/".$anh)){
				$anh = str_random(4)."_".$name;
			}
			$file->move('img', $anh);
			$sh->anh = $anh;
		}
		$sh->gioi_thieu = $request->gioi_thieu;
		$sh->so_luong = $request->so_luong;
		$sh->ngay_nhap= $request->ngay_nhap;
		$sh->gia_tien_moi = $request->gia_tien_moi;
		$sh->gia_tien_cu = $request->gia_tien_cu;
		$sh->noi_dung = $request->noi_dung;
		$sh->trang_thai = $request->trang_thai;
		$sh->save();


		return redirect('tong/sach/sua_sach/'.$ma_sach)->with('thongbao','sửa thành công');
	}

	public function Xoa_sach($id)
	{
		$sh = sach::find($id);
		$sh->delete();
		return redirect('tong/sach/danh_sach_sach')->with('thongbao','xóa thành công');
	}
	public function Xem_chi_tiet_sach(Request $req)

	{
		$xem_chi_tiet_sach = sach::where('ma_sach', $req->id)->select('*')->first();

		return view('khach_hang/sach_kh/xem_chi_tiet_sach', compact('xem_chi_tiet_sach'));
	}
}
