<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\user;
use App\nganh;
use Illuminate\Support\Facades\Hash;


class User_Controller extends Controller
{
    public function Danh_sach_user()
	{
		$user = user::all();
		$nganh = nganh::all();
		return view('admin/use/danh_sach_user',[
			'user'=> $user,
			'nganh'=> $nganh
		]);

	}
	public function Them_user()
	{
		$user = user::all();
		$nganh = nganh::all();
		return view('admin/use/them_user',[
			'user'=> $user,
			'nganh'=> $nganh

		]);
	}
	public function postThem_user(Request $request)

	{
		$this->validate($request, [
			'nganh'=>'required',
			'ten_user' =>'required|min:3',
			'email' =>'required|email|unique:user,email',
			'dia_chi' => 'required|min:5|max:100',
			'ngay_sinh' => 'required',
			'sdt' => 'required',
			'gioi_tinh' => 'required',
			'cap_do' => 'required',
			'password' =>'required|min:5|max:20',
			'passwordAgain' =>'required|same:password',

		],
		[
			'nganh.required' =>'Bạn chưa chọn tên ngành ',
			'ten_user.required' =>'Bạn chưa nhập tên ',
			'ten_user.min' =>'Tên người dùng phải có ít nhất 3 kí tự',

			'email.required' =>'Bạn chưa nhập Email',
			'email.email' =>'Bạn chưa nhập Đúng Email',
			'email.unique' =>'Email của bạn không tồn tại hoặc đã có tài khoản ',
			'password.required' =>'Bạn chưa nhập mật khẩu',
			'password.min' =>'Mật khẩu phải có ít nhất 5 kí tự',
			'password.max' =>'Mật khẩu phải có tối đa 20 kí tự',
			'passwordAgain.required' =>'Bạn chưa nhập lại mật khẩu',
			'passwordAgain.same' =>'Mật khẩu nhập lại chưa khớp',
			'dia_chi.min.required' =>'Bạn chưa nhập địa chỉ',
			'dia_chi.min' =>'Địa chỉ của bạn phải có ít nhất 3 kí tự',
			'ngay_sinh.required' =>'Bạn chưa nhập ngày sinh',
			'sdt.required' =>'Bạn chưa nhập số điện thoại ',
			'gioi_tinh.required' =>'Bạn chưa nhập giới tính',
			'cap_do.required' =>'Bạn chưa nhập cấp độ',
			'password.required' =>'Bạn chưa nhập mật khẩu',
			'password.min' =>'Mật khẩu phải có ít nhất 5 kí tự',
			'password.max' =>'Mật khẩu phải có tối đa 20 kí tự',
			'passwordAgain.required' =>'Bạn chưa nhập lại mật khẩu',
			'passwordAgain.same' =>'Mật khẩu nhập lại chưa khớp',

		]);
		$dg = new user;

		$dg->ten_user = $request->ten_user;
		$dg->email = $request->email;
		$dg->password = $request->password;
		$dg->dia_chi = $request->dia_chi;
		$dg->ngay_sinh= $request->ngay_sinh;

		$dg->gioi_tinh = $request->gioi_tinh;
		$dg->cap_do = $request->cap_do;
		$dg->sdt = $request->sdt;
		$dg->password = bcrypt($request->password);
		$dg->ma_nganh = $request->nganh;
		$dg->save();

		return redirect('tong/user/them_user')->with('thongbao','Thêm thành công');
	}
	public function Sua_user($ma_user)
	{
		$nganh= nganh::all();
		$user = user::find($ma_user);
		return view('admin/use/sua_user',[

			'nganh'=> $nganh,
			'user'=>$user

		]);
	}
	public function postSua_user(Request $request,$ma_user)
	{
		$this->validate($request, [

			'ten_user' =>'required|min:3',
			'email' =>'required|email',
			'dia_chi' => 'required|min:5|max:100',
			'ngay_sinh' => 'required',
			'sdt' => 'required',
			'gioi_tinh' => 'required',
			'cap_do' => 'required',
			// 'password' =>'required|min:5|max:20',

		],
		[

			'ten_user.required' =>'Bạn chưa nhập tên ',
			'ten_user.min' =>'Tên người dùng phải có ít nhất 3 kí tự',

			'email.required' =>'Bạn chưa nhập Email',
			'email.email' =>'Bạn chưa nhập Đúng Email',

			'dia_chi.min.required' =>'Bạn chưa nhập địa chỉ',
			'dia_chi.min' =>'Địa chỉ của bạn phải có ít nhất 3 kí tự',
			'ngay_sinh.required' =>'Bạn chưa nhập ngày sinh',
			'sdt.required' =>'Bạn chưa nhập số điện thoại ',
			'gioi_tinh.required' =>'Bạn chưa nhập giới tính',
			'cap_do.required' =>'Bạn chưa nhập cấp độ',
			// 'password.required' =>'Bạn chưa nhập mật khẩu',
			// 'password.min' =>'Mật khẩu phải có ít nhất 5 kí tự',
			// 'password.max' =>'Mật khẩu phải có tối đa 20 kí tự',

		]);
		$dg = user::find($ma_user);
		$dg->ma_nganh = $request->nganh;
		$dg->ten_user = $request->ten_user;
		$dg->email = $request->email;
		$dg->dia_chi = $request->dia_chi;
		$dg->ngay_sinh= $request->ngay_sinh;

		$dg->gioi_tinh = $request->gioi_tinh;
		$dg->cap_do = $request->cap_do;
		$dg->sdt = $request->sdt;
		// $dg->password = bcrypt($request->password);
		$dg->save();

		return redirect('tong/user/sua_user/'.$ma_user)->with('thongbao','Sửa thành công');
	}

	public function Sua_thongtincanhan_user($ma_user)
	{
		$nganh= nganh::all();
		$user = user::find($ma_user);
		return view('admin/use/sua_thongtincanhan_user',[

			'nganh'=> $nganh,
			'user'=>$user
		]);
	}
	public function postSua_thongtincanhan_user(Request $request, $ma_user)
	{
		$this->validate($request, [
			'nganh'=>'required',
			'ten_user' =>'required|min:3',
			'email' =>'required|email',
			'dia_chi' => 'required|min:5|max:100',
			'ngay_sinh' => 'required',
			'sdt' => 'required',
			'password' =>'required|min:5|max:20',
			'passwordAgain' =>'required|same:password',

		],
		[
			'nganh.required' =>'Bạn chưa chọn tên ngành ',
			'ten_user.required' =>'Bạn chưa nhập tên ',
			'ten_user.min' =>'Tên người dùng phải có ít nhất 3 kí tự',

			'email.required' =>'Bạn chưa nhập Email',
			'email.email' =>'Bạn chưa nhập Đúng Email',
			
			'password.required' =>'Bạn chưa nhập mật khẩu',
			'password.min' =>'Mật khẩu phải có ít nhất 5 kí tự',
			'password.max' =>'Mật khẩu phải có tối đa 20 kí tự',
			'passwordAgain.required' =>'Bạn chưa nhập lại mật khẩu',
			'passwordAgain.same' =>'Mật khẩu nhập lại chưa khớp',
			'dia_chi.min.required' =>'Bạn chưa nhập địa chỉ',
			'dia_chi.min' =>'Địa chỉ của bạn phải có ít nhất 3 kí tự',
			'dia_chi.max' => 'Địa chỉ của bạn quá 100 kí tự',
			'ngay_sinh.required' =>'Bạn chưa nhập ngày sinh',
			'sdt.required' =>'Bạn chưa nhập số điện thoại ',
			'password.required' =>'Bạn chưa nhập mật khẩu',
			'password.min' =>'Mật khẩu phải có ít nhất 5 kí tự',
			'password.max' =>'Mật khẩu phải có tối đa 20 kí tự',
			'passwordAgain.required' =>'Bạn chưa nhập lại mật khẩu',
			'passwordAgain.same' =>'Mật khẩu nhập lại chưa khớp',

		]);
		$dg = user::find($ma_user);
		$dg->ten_user = $request->ten_user;
		$dg->email = $request->email;
		$dg->password = $request->password;
		$dg->dia_chi = $request->dia_chi;
		$dg->ngay_sinh= $request->ngay_sinh;
		$dg->sdt = $request->sdt;
		$dg->password = bcrypt($request->password);
		$dg->ma_nganh = $request->nganh;
		$dg->save();

		return redirect('tong/user/sua_thongtincanhan_user/'.$ma_user)->with('thongbao','Sửa thông tin cá nhân thành công');
	}

	public function Xoa_user($id)
	{
		$dg = user::find($id);
		$dg->delete();
		return redirect('tong/user/danh_sach_user')->with('thongbao','xóa thành công');
	}

	public function Dang_nhap_user()
	{
		return view('admin/dang_nhap_user');
	}
	public function postDang_nhap_user(Request $request)
	{
		$this->validate($request, [

			'email' =>'required',
			'password' =>'required|min:5|max:20',

		],
		[

			'email.required' =>'Bạn chưa nhập Email',
			'password.required' =>'Bạn chưa nhập mật khẩu',
			'password.min' =>'Mật khẩu phải có ít nhất 5 kí tự',
			'password.max' =>'Mật khẩu phải có tối đa 20 kí tự',

		]);

		if(Auth::attempt(['email'=>$request->email,'password'=>$request->password]))
		{
			return redirect('tong/sach/danh_sach_sach');

		}
		else
		{
			return redirect('user/dang_nhap_user')->with('thongbao','đăng nhập không thành công');
		}
	}
	public function getLogout() {
		Auth::logout();
		return redirect('user/dang_nhap_user');
	}

}
