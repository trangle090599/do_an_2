<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\dat_muon;
use App\user;
use App\sach;
class Dat_muon_Controller extends Controller
{
	public function Danh_sach_dat_muon()
	{
		$user= user::all();
		$dat_muon= dat_muon::all();
		return view('admin/dat_muon/danh_sach_dat_muon', ['dat_muon'=> $dat_muon, 'user'=> $user]);
	}
	
	public function Them_dat_muon()

	{
		$user= user::all();
		$sach = sach::all();
		return view('admin/dat_muon/them_dat_muon',[
			 
			'user'=> $user, 'sach'=> $sach]);	
	}
	public function postThem_dat_muon(Request $request)
	{
		$this->validate($request, [
			'user'=>'required',
			'sach'=>'required',
			'ngay_dat' => 'required',
			'so_luong'=>'required',
			 'trang_thai'=>'required'
		],
		[
			'user.required' =>'Chưa chọn độc giả ',
			'sach.required' =>'Chưa chọn sách ',
			'ngay_dat.required' =>'Chưa nhập ngày đặt mượn',
			'so_luong.required' =>'Chưa chọn số Lượng ',
			'trang_thai.required' =>'Chưa chọn trạng thái ',
			
		]);
		$dm = new dat_muon;
		$dm->ma_sach = $request->sach;
		$dm->ma_user = $request->user;
		$dm->ngay_dat = $request->ngay_dat;
		$dm->so_luong = $request->so_luong;
		$dm->trang_thai = $request->trang_thai;
		$dm->save();
		
		return redirect('tong/dat_muon/them_dat_muon')->with('thongbao','Thêm thành công');
	}
	public function Sua_dat_muon($ma_dat_muon)
	{
		$user= user::all();
		$sach=sach::all();
		$dat_muon = dat_muon::find($ma_dat_muon);
		return view('admin/dat_muon/sua_dat_muon',[
			'sach'=>$sach,
			'user'=> $user,
			'dat_muon' => $dat_muon
		]);
	}

	public function postSua_dat_muon(Request $request,$ma_dat_muon)
	{
		$this->validate($request, [
			'user'=>'required',
			'sach'=>'required',
			'ngay_dat' => 'required',
			'so_luong'=>'required',
			 'trang_thai'=>'required'
		],
		[
			'user.required' =>'Chưa chọn độc giả ',
			'sach.required' =>'Chưa chọn sách ',
			'ngay_dat.required' =>'Chưa nhập ngày đặt mượn',
			'so_luong.required' =>'Chưa chọn số Lượng ',
			'trang_thai.required' =>'Chưa chọn trạng thái ',
			
		]);
		$dm = dat_muon::find($ma_dat_muon);
		$dm->ma_sach = $request->sach;
		$dm->ma_user = $request->user;
		$dm->ngay_dat = $request->ngay_dat;
		$dm->so_luong = $request->so_luong;
		$dm->trang_thai = $request->trang_thai;
		$dm->save();
		
		return redirect('tong/dat_muon/sua_dat_muon/'.$ma_dat_muon)->with('thongbao','sửa thành công');
	}
	public function Xoa_dat_muon($id)
	{
		$dat_muon = dat_muon::find($id);
		$dat_muon->delete();
		return redirect('tong/dat_muon/danh_sach_dat_muon')->with('thongbao','xóa thành công');
	}
}


