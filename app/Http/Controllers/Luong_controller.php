<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\user;
use App\luong;
use Illuminate\Support\Facades\HTML;

class Luong_controller extends Controller
{
     public function Danh_sach_luong()
	{
		$luong= luong::all();
		return view('admin/luong/danh_sach_luong', ['luong'=> $luong]);
	}
	public function Them_luong()
	{
		$user= user::all();
		return view('admin/luong/them_luong',['user'=> $user]);
	}
	public function postThem_luong(Request $request)

	{
		$this->validate($request, [
			'user'=>'required',
			'thang' => 'required',
			'tien' => 'required',
			'ngay_nhan' => 'required',


		],
		[
			'user.required' =>'Bạn chưa chọn tên nhân viên ',
			'thang.required' =>'Bạn chưa nhập tên lương ',
			'tien.required' =>'Bạn chưa nhập tiền',
			'ngay_nhan.required' =>'Bạn chưa nhập ngày nhận',



		]);
		$lg = new luong;
		$lg->ma_user = $request->user;
		$lg->thang = $request->thang;
		$lg->tien = $request->tien;
		$lg->ngay_nhan= $request->ngay_nhan;


		$lg->save();

		return redirect('tong/luong/them_luong')->with('thongbao','Thêm thành công');
	}
	public function Sua_luong($ma_luong)
	{
		$user= user::all();
		$luong = luong::find($ma_luong);

		return view('admin/luong/sua_luong',[
			'user'=> $user,
			'luong'=>$luong

		]);
	}
	public function postSua_luong(Request $request,$ma_luong)
	{
		$this->validate($request, [
			'user'=>'required',
			'thang' => 'required',
			'tien' => 'required',
			'ngay_nhan' => 'required',

		],
		[
			'user.required' =>'Bạn chưa chọn tên nhân viên ',
			'thang.required' =>'Bạn chưa nhập tháng nhận',
			'tien.required' =>'Bạn chưa nhập tiền',
			'ngay_nhan.required' =>'Bạn chưa nhập ngày nhận',
		]);
		$lg = luong::find($ma_luong);
		$lg->ma_user = $request->user;
		$lg->thang = $request->thang;
		$lg->tien = $request->tien;
		$lg->ngay_nhan= $request->ngay_nhan;


		$lg->save();

		return redirect('tong/luong/sua_luong/'.$ma_luong)->with('thongbao','Thêm thành công');
	}

	public function Xoa_luong($id)
	{
		$lg = luong::find($id);
		$lg->delete();
		return redirect('tong/luong/danh_sach_luong')->with('thongbao','xóa thành công');
	}
}
