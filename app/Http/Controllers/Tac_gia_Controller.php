<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tac_gia;

class Tac_gia_Controller extends Controller
{
    public function Danh_sach_tac_gia()
	{
		$tac_gia= tac_gia::all();
		return view('admin/tac_gia/danh_sach_tac_gia', ['tac_gia'=> $tac_gia]);
	}

	public function Them_tac_gia()

	{
		return view('admin/tac_gia/them_tac_gia');
	}
	public function postThem_tac_gia(Request $request)
	{
		$this->validate($request, [
			'ten_tac_gia' =>'required|min:3',
			'gioi_thieu'	=>'required|min:3',

		],
		[
			'ten_tac_gia.required' =>'Bạn chưa nhập tên tác giả ',
			'ten_tac_gia.min'      =>'Tên tác giả phải có ít nhất 3 kí tự',
			'gioi_thieu.required'  =>'Bạn chưa nhập giới thiệu ',
			'gioi_thieu.min'       =>'Giới thiệu phải có ít nhất 3 kí tự',


		]);
		$tac_gia = new tac_gia;
		$tac_gia->ten_tac_gia = $request->ten_tac_gia;
		$tac_gia->gioi_thieu  = $request->gioi_thieu;
		$tac_gia->save();

		return redirect('tong/tac_gia/them_tac_gia')->with('thongbao','Thêm thành công');
	}
	public function Sua_tac_gia($ma_tac_gia)
	{
		$tac_gia = tac_gia::find($ma_tac_gia);
		return view('admin/tac_gia/sua_tac_gia',['tac_gia' => $tac_gia]);
	}

	public function postSua_tac_gia(Request $request,$ma_tac_gia)
	{
		$this->validate($request, [
			'ten_tac_gia'   =>'required|min:3',
			'gioi_thieu'	=>'required|min:3',
		],
		[
			'ten_tac_gia.required' =>'Bạn chưa nhập tên tác giả ',
			'ten_tac_gia.min'      =>'Tên tác giả có ít nhất 3 kí tự',
			'gioi_thieu.required'  =>'Bạn chưa nhập giới thiệu ',
			'gioi_thieu.min'       =>'Giới thiệu phải có ít nhất 3 kí tự',
		]);
		$tac_gia = tac_gia::find($ma_tac_gia);
		$tac_gia->ten_tac_gia = $request->ten_tac_gia;
		$tac_gia->gioi_thieu = $request->gioi_thieu;
		$tac_gia->save();

		return redirect('tong/tac_gia/sua_tac_gia/'.$ma_tac_gia)->with('thongbao','sửa thành công');
	}
	public function Xoa_tac_gia($id)
	{
		$tac_gia = tac_gia::find($id);
		$tac_gia->delete();
		return redirect('tong/tac_gia/danh_sach_tac_gia')->with('thongbao','xóa thành công');
	}
}

