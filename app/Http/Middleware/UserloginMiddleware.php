<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;


class UserloginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check())
        {
           $user = Auth::user();
           if($user->cap_do != 3 )
           {

            return $next($request);

           }
         else
          return redirect('index');
        }
        else return redirect('user/dang_nhap_user');

    }
}
