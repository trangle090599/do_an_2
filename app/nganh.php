<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class nganh extends Model
{
   protected $table = "nganh";
	protected $primaryKey = 'ma_nganh';
	public $timestamps = false;
   public function user()
   {
   	return $this->hasOne('App\user','ma_nganh','ma_user');
   }
}
