<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sach extends Model
{
    protected $table = "sach";
    protected $primaryKey = 'ma_sach';
    public $timestamps = false;

    public function the_loai_sach()
    {
    	return $this->belongsTo('App\the_loai_sach','ma_the_loai_sach','ma_the_loai_sach');
    }
    public function nha_xuat_ban()
    {
    	return $this->belongsTo('App\nha_xuat_ban','ma_nha_xuat_ban','ma_nha_xuat_ban');
    } 
    public function tac_gia()
    {
    	return $this->belongsTo('App\tac_gia','ma_tac_gia','ma_tac_gia');
    }
     public function phong()
    {
        return $this->belongsTo('App\phong','ma_phong','ma_phong');
    }
    public function arr_don_dat_muon()
    {
    	return $this->hasMany('App\don_dat_muon','ma_sach','ma_sach');
    } 
    
}
