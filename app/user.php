<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class user extends Authenticatable

{
    protected $table = "user";
    protected $primaryKey = 'ma_user';
    public $timestamps = false;

    public function don_dat_muon()
    {
    	return $this->hasMany('App\don_dat_muon','ma_user','ma_dat_muon');
    }
    public function nganh()
    {
    	return $this->belongsTo('App\nganh','ma_nganh','ma_nganh');
    }
    public function hoa_don()
    {
        return $this->hasMany('App\hoa_don','ma_user','ma_hoa_don');
    }
     public function luong()
   {
        return $this->hasMany('App\luong','ma_user', 'ma_luong');
   }
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
}
