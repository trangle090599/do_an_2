<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class luong extends Model
{
    protected $table="luong";
    protected $primaryKey = 'ma_luong';
    public $timestamps = false;
    public function user()
   {
   		return $this->belongsTo('App\user', 'ma_user', 'ma_user');
}
}
