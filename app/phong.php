<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class phong extends Model
{
    protected $table="phong";

    protected $primaryKey = 'ma_phong';
     public function vat_chat()
   {
   		return $this->belongsTo('App\vat_chat','ma_vat_chat');
   }
   public function sach()
   {
   		return $this->hasMany('App\sach','ma_phong','ma_sach');
   }

}
