<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dat_muon extends Model
{
   protected $table = "dat_muon";

   protected $primaryKey = 'ma_dat_muon';
   public $timestamps = false;

   public function don_dat_muon()
   {
   		return $this->hasMany('App\don_dat_muon','ma_dat_muon','ma_dat_muon');
   }
   
    public function user()
    {
    	return $this->belongsTo('App\user','ma_user','ma_user');
    } 
    public function sach() 
    {
     return $this->belongsTo('App\sach','ma_sach','ma_sach');
    }  
}
