<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\the_loai_sach;
use App\cart;
use Session;
class AppServiceProvider extends ServiceProvider
{
     public function boot()
    {
        view()->composer('header', function($view){
        $the_loai_sach = the_loai_sach::all();
       
        $view->with('the_loai_sach', $the_loai_sach);

		});

        view()->composer(['header','homepase.dat_hang'], function($view){
        
        if(Session('cart')){
        	$oldcart = Session::get('cart');
        	$cart = new Cart($oldcart) ; 
        	$view->with(['cart'=>Session::get('cart'),'product_cart'=>$cart->items,'totalPrice'=>$cart->totalPrice,
        		'totalQty'=>$cart->totalQty ]);
    	}
    });
    
   		
   
	}
}
