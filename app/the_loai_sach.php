<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class the_loai_sach extends Model
{
	protected $table = "the_loai_sach";
	protected $primaryKey = 'ma_the_loai_sach';
	public $timestamps = false;
	public function sach()
	{
		return $this->hasMany('App\sach','ma_the_loai_sach','ma_sach');
	}
}
