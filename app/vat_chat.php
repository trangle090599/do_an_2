<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vat_chat extends Model
{
     protected $table="vat_chat";

    protected $primaryKey = 'ma_vat_chat';
    public function phong()
    {
         return $this->hasMany('App\phong','ma_vat_chat','ma_phong');
    }
}
