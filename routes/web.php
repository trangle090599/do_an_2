<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('index', 'Homepage_Controller@getIndex');


Route::get('dang_nhap', 'Homepage_Controller@Dang_nhap_user');
Route::post('dang_nhap', 'Homepage_Controller@postDang_nhap_user');
Route::get('logout_kh','User_Controller@getLogout');

Route::get('dang_ky',[
	'as' => 'dang_ky',

 	'uses'=>'Homepage_Controller@getDangky']);

Route::post('dang_ky',[
	'as' => 'dang_ky',

 	'uses'=>'Homepage_Controller@postDangky']);



Route::get('the_loai_sach/{type}',[
	'as' => 'the_loai_sach',

 	'uses'=>'Homepage_Controller@getThe_loai_sach']);


Route::get('chi_tiet_sach/{ma_sach}',[
	'as' => 'chi_tiet_sach',
 	'uses'=>'Homepage_Controller@getChi_tiet_sach']);

Route::get('add_to_cart/{ma_sach}',[
	'as' => 'them_gioi_hang',

 	'uses'=>'Homepage_Controller@getAdd_to_cart']);

Route::get('del_cart/{ma_sach}',[
	'as' => 'xoa_gioi_hang',

 	'uses'=>'Homepage_Controller@getDelitemcart']);

Route::get('dat_hang',[
	'as' => 'dat_hang',

 	'uses'=>'Homepage_Controller@getCheckout']);

Route::post('dat_hang',[
	'as' => 'dat_hang',

 	'uses'=>'Homepage_Controller@postCheckout']);


Route::get('lien_he', 'Homepage_Controller@getLien_he');
Route::get('gioi_thieu', 'Homepage_Controller@getGioi_thieu');

Route::get('user/dang_nhap_user', 'User_Controller@Dang_nhap_user');
Route::post('user/dang_nhap_user', 'User_Controller@postDang_nhap_user');
Route::get('logout','User_Controller@getLogout');


Route::group(['prefix'=>'tong', 'middleware'=>'userLogin'], function(){

	//Đặt mượn
	Route::group(['prefix'=>'dat_muon'], function(){
		Route::get('danh_sach_dat_muon','Dat_muon_Controller@Danh_sach_dat_muon');

		Route::get('sua_dat_muon/{id}','Dat_muon_Controller@Sua_dat_muon');
		Route::post('sua_dat_muon/{id}','Dat_muon_Controller@postSua_dat_muon');

		Route::get('them_dat_muon','Dat_muon_Controller@Them_dat_muon');
		Route::post('them_dat_muon','Dat_muon_Controller@postThem_dat_muon');

		Route::get('xoa_dat_muon/{id}','Dat_muon_Controller@Xoa_dat_muon');
	});

	//Vật chất
	Route::group(['prefix'=>'vat_chat'], function(){
		Route::get('danh_sach_vat_chat','Vat_chat_Controller@Danh_sach_vat_chat');

		Route::get('sua_vat_chat/{id}','Vat_chat_Controller@Sua_vat_chat');
		Route::post('sua_vat_chat/{id}','Vat_chat_Controller@postSua_vat_chat');

		Route::get('them_vat_chat','Vat_chat_Controller@Them_vat_chat');
		Route::post('them_vat_chat','Vat_chat_Controller@postThem_vat_chat');

		Route::get('xoa_vat_chat/{id}','Vat_chat_Controller@Xoa_vat_chat');
	});

	//Slide
	Route::group(['prefix'=>'slide'], function(){
		Route::get('danh_sach_slide','Slide_Controller@Danh_sach_slide');

		Route::get('sua_slide/{id}','Slide_Controller@Sua_slide');
		Route::post('sua_slide/{id}','Slide_Controller@postSua_slide');

		Route::get('them_slide','Slide_Controller@Them_slide');
		Route::post('them_slide','Slide_Controller@postThem_slide');

		Route::get('xoa_slide/{id}','Slide_Controller@Xoa_slide');
	});


	//Người dùng
	Route::group(['prefix'=>'user'], function(){
		Route::get('danh_sach_user','User_Controller@Danh_sach_user');

		Route::get('sua_user/{id}','User_Controller@Sua_user');
		Route::post('sua_user/{id}','User_Controller@postSua_user');

		Route::get('sua_thongtincanhan_user/{id}','User_Controller@Sua_thongtincanhan_user');
		Route::post('sua_thongtincanhan_user/{id}','User_Controller@postSua_thongtincanhan_user');

		Route::get('them_user/','User_Controller@Them_user');
		Route::post('them_user/','User_Controller@postThem_user');

		Route::get('xoa_user/{id}','User_Controller@Xoa_user');
	});



	Route::group(['prefix'=>'don_dat_muon'], function(){
		Route::get('danh_sach_don_dat_muon','Don_dat_muon_Controller@Danh_sach_don_dat_muon');

		Route::get('sua_don_dat_muon/{id}','Don_dat_muon_Controller@Sua_don_dat_muon');
		Route::post('sua_don_dat_muon/{id}','Don_dat_muon_Controller@postSua_don_dat_muon');

		Route::get('them_don_dat_muon','Don_dat_muon_Controller@Them_don_dat_muon');
		Route::post('them_don_dat_muon','Don_dat_muon_Controller@postThem_don_dat_muon');

		Route::get('xoa_don_dat_muon/{id}','Don_dat_muon_Controller@Xoa_don_dat_muon');
	});

	//Ngành
	Route::group(['prefix'=>'nganh'], function(){
		Route::get('danh_sach_nganh','Nganh_Controller@Danh_sach_nganh');

		Route::get('sua_nganh/{id}','Nganh_Controller@Sua_nganh');
		Route::post('sua_nganh/{id}','Nganh_Controller@postSua_nganh');

		Route::get('them_nganh','Nganh_Controller@Them_nganh');
		Route::post('them_nganh','Nganh_Controller@postThem_nganh');

		Route::get('xoa_nganh/{id}','Nganh_Controller@Xoa_nganh');
	});

	//Nhà xuất bản
	Route::group(['prefix'=>'nha_xuat_ban'], function(){
		Route::get('danh_sach_nha_xuat_ban','Nha_xuat_ban_Controller@Danh_sach_nha_xuat_ban');

		Route::get('sua_nha_xuat_ban/{id}','Nha_xuat_ban_Controller@Sua_nha_xuat_ban');
		Route::post('sua_nha_xuat_ban/{id}','Nha_xuat_ban_Controller@postSua_nha_xuat_ban');

		Route::get('them_nha_xuat_ban','Nha_xuat_ban_Controller@Them_nha_xuat_ban');
		Route::post('them_nha_xuat_ban','Nha_xuat_ban_Controller@postThem_nha_xuat_ban');

		Route::get('xoa_nha_xuat_ban/{id}','Nha_xuat_ban_Controller@Xoa_nha_xuat_ban');
	});

	//Sách
	Route::group(['prefix'=>'sach'], function(){
		Route::get('danh_sach_sach','Sach_Controller@Danh_sach_sach');

		Route::get('sua_sach/{id}','Sach_Controller@Sua_sach');
		Route::post('sua_sach/{id}','Sach_Controller@postSua_sach');

		Route::get('them_sach','Sach_Controller@Them_sach');
		Route::post('them_sach','Sach_Controller@postThem_sach');

		Route::get('xoa_sach/{id}','Sach_Controller@Xoa_sach');


	});

	//Lương
	Route::group(['prefix'=>'luong'], function(){
		Route::get('danh_sach_luong','Luong_Controller@Danh_sach_luong');

		Route::get('sua_luong/{id}','Luong_Controller@Sua_luong');
		Route::post('sua_luong/{id}','Luong_Controller@postSua_luong');

		Route::get('them_luong','Luong_Controller@Them_luong');
		Route::post('them_luong','Luong_Controller@postThem_luong');

		Route::get('xoa_luong/{id}','Luong_Controller@Xoa_luong');


	});


	//Tác giả
	Route::group(['prefix'=>'tac_gia'], function(){
		Route::get('danh_sach_tac_gia','Tac_gia_Controller@Danh_sach_tac_gia');

		Route::get('sua_tac_gia/{id}','Tac_gia_Controller@Sua_tac_gia');
		Route::post('sua_tac_gia/{id}','Tac_gia_Controller@postSua_tac_gia');

		Route::get('them_tac_gia','Tac_gia_Controller@Them_tac_gia');
		Route::post('them_tac_gia','Tac_gia_Controller@postThem_tac_gia');

		Route::get('xoa_tac_gia/{id}','Tac_gia_Controller@Xoa_tac_gia');

	});


	//Thể loại sách
	Route::group(['prefix'=>'the_loai_sach'], function(){
		Route::get('danh_sach_the_loai_sach','The_loai_sach_Controller@Danh_sach_the_loai_sach');

		Route::get('sua_the_loai_sach/{id}','The_loai_sach_Controller@Sua_the_loai_sach');
		Route::post('sua_the_loai_sach/{id}','The_loai_sach_Controller@postSua_the_loai_sach');

		Route::get('them_the_loai_sach','The_loai_sach_Controller@Them_the_loai_sach');
		Route::post('them_the_loai_sach','The_loai_sach_Controller@postThem_the_loai_sach');

		Route::get('xoa_the_loai_sach/{id}','The_loai_sach_Controller@Xoa_the_loai_sach');
	});

	//Phòng
	Route::group(['prefix'=>'phong'], function(){
		Route::get('danh_sach_phong','Phong_Controller@Danh_sach_phong');

		Route::get('sua_phong/{id}','Phong_Controller@Sua_phong');
		Route::post('sua_phong/{id}','Phong_Controller@postSua_phong');

		Route::get('them_phong','Phong_Controller@Them_phong');
		Route::post('them_phong','Phong_Controller@postThem_phong');

		Route::get('xoa_phong/{id}','Phong_Controller@Xoa_phong');
	});

	//Thống kê
	Route::group(['prefix'=>'thong_ke'], function(){
		Route::get('thong_ke_sach','Thong_ke_sach_Controller@Thong_ke_sach');


	});





	// Route::group(['prefix'=>'admin'], function(){
	// 	Route::get('danh_sach_admin','Admin_Controller@Danh_sach_admin');

	// 	Route::get('sua_admin/{id}','Admin_Controller@Sua_admin');
	// 	Route::post('sua_admin/{id}','Admin_Controller@postSua_admin');

	// 	Route::get('them_admin','Admin_Controller@Them_admin');
	// 	Route::post('them_admin','Admin_Controller@postThem_admin');

	// 	Route::get('xoa_admin/{id}','Admin_Controller@Xoa_admin');

	// });


	Auth::routes();
	Route::get('/changePassword','HomeController@showChangePasswordForm');
	Route::post('/changePassword','HomeController@changePassword')->name('changePassword');



});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
