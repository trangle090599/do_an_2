
<!DOCTYPE html>
<!--[if IE 9 ]> <html lang="vi-VN" class="ie9 loading-site no-js"> <![endif]-->
<!--[if IE 8 ]> <html lang="vi-VN" class="ie8 loading-site no-js"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="vi-VN" class="loading-site no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="http://hptmedia.vn/matong/xmlrpc.php" />
	
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>
<title>Trang chủ - Mật ong</title>

<!-- This site is optimized with the Yoast SEO plugin v12.3 - https://yoast.com/wordpress/plugins/seo/ -->
<meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
<link rel="canonical" href="http://hptmedia.vn/matong/" />
<meta property="og:locale" content="vi_VN" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Trang chủ - Mật ong" />
<meta property="og:url" content="http://hptmedia.vn/matong/" />
<meta property="og:site_name" content="Mật ong" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:title" content="Trang chủ - Mật ong" />
<script type='application/ld+json' class='yoast-schema-graph yoast-schema-graph--main'>{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"http://hptmedia.vn/matong/#website","url":"http://hptmedia.vn/matong/","name":"M\u1eadt ong","potentialAction":{"@type":"SearchAction","target":"http://hptmedia.vn/matong/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"WebPage","@id":"http://hptmedia.vn/matong/#webpage","url":"http://hptmedia.vn/matong/","inLanguage":"vi-VN","name":"Trang ch\u1ee7 - M\u1eadt ong","isPartOf":{"@id":"http://hptmedia.vn/matong/#website"},"datePublished":"2019-01-28T03:11:38+00:00","dateModified":"2019-10-21T02:50:13+00:00"}]}</script>
<!-- / Yoast SEO plugin. -->

<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Dòng thông tin Mật ong &raquo;" href="http://hptmedia.vn/matong/feed/" />
<link rel="alternate" type="application/rss+xml" title="Dòng phản hồi Mật ong &raquo;" href="http://hptmedia.vn/matong/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/hptmedia.vn\/matong\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.7"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
	<link rel='stylesheet' id='wp-block-library-css'  href='http://hptmedia.vn/matong/wp-includes/css/dist/block-library/style.min.css?ver=5.2.7' type='text/css' media='all' />
<link rel='stylesheet' id='wc-block-style-css'  href='http://hptmedia.vn/matong/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/style.css?ver=2.3.0' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='http://hptmedia.vn/matong/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.4' type='text/css' media='all' />
<style id='woocommerce-inline-inline-css' type='text/css'>
.woocommerce form .form-row .required { visibility: visible; }
</style>
<link rel='stylesheet' id='flatsome-main-css'  href='http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/css/flatsome.css?ver=3.7.2' type='text/css' media='all' />
<link rel='stylesheet' id='rt-main-css'  href='http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/css/rt-main.css?ver=3.7.2' type='text/css' media='all' />
<link rel='stylesheet' id='flatsome-shop-css'  href='http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/css/flatsome-shop.css?ver=3.7.2' type='text/css' media='all' />
<link rel='stylesheet' id='flatsome-style-css'  href='http://hptmedia.vn/matong/wp-content/themes/flatsome-child/style.css?ver=3.0' type='text/css' media='all' />
<script type='text/javascript' src='http://hptmedia.vn/matong/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp'></script>
<script type='text/javascript' src='http://hptmedia.vn/matong/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<link rel='https://api.w.org/' href='http://hptmedia.vn/matong/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://hptmedia.vn/matong/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://hptmedia.vn/matong/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 5.2.7" />
<meta name="generator" content="WooCommerce 3.7.1" />
<link rel='shortlink' href='http://hptmedia.vn/matong/' />
<link rel="alternate" type="application/json+oembed" href="http://hptmedia.vn/matong/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fhptmedia.vn%2Fmatong%2F" />
<link rel="alternate" type="text/xml+oembed" href="http://hptmedia.vn/matong/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fhptmedia.vn%2Fmatong%2F&#038;format=xml" />
<style>.bg{opacity: 0; transition: opacity 1s; -webkit-transition: opacity 1s;} .bg-loaded{opacity: 1;}</style><!--[if IE]><link rel="stylesheet" type="text/css" href="http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/css/ie-fallback.css"><script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.1/html5shiv.js"></script><script>var head = document.getElementsByTagName('head')[0],style = document.createElement('style');style.type = 'text/css';style.styleSheet.cssText = ':before,:after{content:none !important';head.appendChild(style);setTimeout(function(){head.removeChild(style);}, 0);</script><script src="http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/libs/ie-flexibility.js"></script><![endif]-->    <script type="text/javascript">
    WebFontConfig = {
      google: { families: [ "Roboto:regular,regular","Roboto:regular,regular","Roboto:regular,regular","Roboto:regular,regular", ] }
    };
    (function() {
      var wf = document.createElement('script');
      wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
      wf.type = 'text/javascript';
      wf.async = 'true';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(wf, s);
    })(); </script>
  <style>.product-gallery img.lazy-load, .product-small img.lazy-load, .product-small img[data-lazy-srcset]:not(.lazyloaded){ padding-top: 119.83805668016%;}</style>	<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
	<style id="custom-css" type="text/css">:root {--primary-color: #fdc215;}/* Site Width */.full-width .ubermenu-nav, .container, .row{max-width: 1170px}.row.row-collapse{max-width: 1140px}.row.row-small{max-width: 1162.5px}.row.row-large{max-width: 1200px}.sticky-add-to-cart--active, #wrapper,#main,#main.dark{background-color: #fffdf1}.header-main{height: 110px}#logo img{max-height: 110px}#logo{width:200px;}.header-top{min-height: 30px}.has-transparent + .page-title:first-of-type,.has-transparent + #main > .page-title,.has-transparent + #main > div > .page-title,.has-transparent + #main .page-header-wrapper:first-of-type .page-title{padding-top: 110px;}.header.show-on-scroll,.stuck .header-main{height:70px!important}.stuck #logo img{max-height: 70px!important}.search-form{ width: 90%;}.header-bg-color, .header-wrapper {background-color: #ffffff}.header-bottom {background-color: #f1f1f1}.header-main .nav > li > a{line-height: 16px }@media (max-width: 549px) {.header-main{height: 70px}#logo img{max-height: 70px}}/* Color */.accordion-title.active, .has-icon-bg .icon .icon-inner,.logo a, .primary.is-underline, .primary.is-link, .badge-outline .badge-inner, .nav-outline > li.active> a,.nav-outline >li.active > a, .cart-icon strong,[data-color='primary'], .is-outline.primary{color: #fdc215;}/* Color !important */[data-text-color="primary"]{color: #fdc215!important;}/* Background Color */[data-text-bg="primary"]{background-color: #fdc215;}/* Background */.scroll-to-bullets a,.featured-title, .label-new.menu-item > a:after, .nav-pagination > li > .current,.nav-pagination > li > span:hover,.nav-pagination > li > a:hover,.has-hover:hover .badge-outline .badge-inner,button[type="submit"], .button.wc-forward:not(.checkout):not(.checkout-button), .button.submit-button, .button.primary:not(.is-outline),.featured-table .title,.is-outline:hover, .has-icon:hover .icon-label,.nav-dropdown-bold .nav-column li > a:hover, .nav-dropdown.nav-dropdown-bold > li > a:hover, .nav-dropdown-bold.dark .nav-column li > a:hover, .nav-dropdown.nav-dropdown-bold.dark > li > a:hover, .is-outline:hover, .tagcloud a:hover,.grid-tools a, input[type='submit']:not(.is-form), .box-badge:hover .box-text, input.button.alt,.nav-box > li > a:hover,.nav-box > li.active > a,.nav-pills > li.active > a ,.current-dropdown .cart-icon strong, .cart-icon:hover strong, .nav-line-bottom > li > a:before, .nav-line-grow > li > a:before, .nav-line > li > a:before,.banner, .header-top, .slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .primary.is-outline:hover, .button.primary:not(.is-outline), input[type='submit'].primary, input[type='submit'].primary, input[type='reset'].button, input[type='button'].primary, .badge-inner{background-color: #fdc215;}/* Border */.nav-vertical.nav-tabs > li.active > a,.scroll-to-bullets a.active,.nav-pagination > li > .current,.nav-pagination > li > span:hover,.nav-pagination > li > a:hover,.has-hover:hover .badge-outline .badge-inner,.accordion-title.active,.featured-table,.is-outline:hover, .tagcloud a:hover,blockquote, .has-border, .cart-icon strong:after,.cart-icon strong,.blockUI:before, .processing:before,.loading-spin, .slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .primary.is-outline:hover{border-color: #fdc215}.nav-tabs > li.active > a{border-top-color: #fdc215}.widget_shopping_cart_content .blockUI.blockOverlay:before { border-left-color: #fdc215 }.woocommerce-checkout-review-order .blockUI.blockOverlay:before { border-left-color: #fdc215 }/* Fill */.slider .flickity-prev-next-button:hover svg,.slider .flickity-prev-next-button:hover .arrow{fill: #fdc215;}/* Background Color */[data-icon-label]:after, .secondary.is-underline:hover,.secondary.is-outline:hover,.icon-label,.button.secondary:not(.is-outline),.button.alt:not(.is-outline), .badge-inner.on-sale, .button.checkout, .single_add_to_cart_button{ background-color:#f39402; }[data-text-bg="secondary"]{background-color: #f39402;}/* Color */.secondary.is-underline,.secondary.is-link, .secondary.is-outline,.stars a.active, .star-rating:before, .woocommerce-page .star-rating:before,.star-rating span:before, .color-secondary{color: #f39402}/* Color !important */[data-text-color="secondary"]{color: #f39402!important;}/* Border */.secondary.is-outline:hover{border-color:#f39402}body{font-size: 100%;}@media screen and (max-width: 549px){body{font-size: 100%;}}body{font-family:"Roboto", sans-serif}body{font-weight: 0}.nav > li > a {font-family:"Roboto", sans-serif;}.nav > li > a {font-weight: 0;}h1,h2,h3,h4,h5,h6,.heading-font, .off-canvas-center .nav-sidebar.nav-vertical > li > a{font-family: "Roboto", sans-serif;}h1,h2,h3,h4,h5,h6,.heading-font,.banner h1,.banner h2{font-weight: 0;}.alt-font{font-family: "Roboto", sans-serif;}.alt-font{font-weight: 0!important;}.header:not(.transparent) .header-nav.nav > li > a:hover,.header:not(.transparent) .header-nav.nav > li.active > a,.header:not(.transparent) .header-nav.nav > li.current > a,.header:not(.transparent) .header-nav.nav > li > a.active,.header:not(.transparent) .header-nav.nav > li > a.current{color: #fdc215;}.header-nav.nav-line-bottom > li > a:before,.header-nav.nav-line-grow > li > a:before,.header-nav.nav-line > li > a:before,.header-nav.nav-box > li > a:hover,.header-nav.nav-box > li.active > a,.header-nav.nav-pills > li > a:hover,.header-nav.nav-pills > li.active > a{color:#FFF!important;background-color: #fdc215;}@media screen and (min-width: 550px){.products .box-vertical .box-image{min-width: 247px!important;width: 247px!important;}}.footer-2{background-image: url('https://hptmedia.vn/matong/wp-content/uploads/2019/01/footer.jpg');}.page-title-small + main .product-container > .row{padding-top:0;}.label-new.menu-item > a:after{content:"New";}.label-hot.menu-item > a:after{content:"Hot";}.label-sale.menu-item > a:after{content:"Sale";}.label-popular.menu-item > a:after{content:"Popular";}</style></head>

<body class="home page-template page-template-page-blank page-template-page-blank-php page page-id-11 theme-flatsome woocommerce-no-js lightbox lazy-icons nav-dropdown-has-arrow">


<a class="skip-link screen-reader-text" href="#main">Skip to content</a>

<div id="wrapper">


<header id="header" class="header has-sticky sticky-jump">
   <div class="header-wrapper">
	<div id="masthead" class="header-main ">
      <div class="header-inner flex-row container logo-left medium-logo-center" role="navigation">

          <!-- Logo -->
          <div id="logo" class="flex-col logo">
            <!-- Header logo -->
<a href="http://hptmedia.vn/matong/" title="Mật ong - Rừng" rel="home">
    <img width="200" height="110" src="https://hptmedia.vn/matong/wp-content/uploads/2019/10/logo_2.png" class="header_logo header-logo" alt="Mật ong"/><img  width="200" height="110" src="https://hptmedia.vn/matong/wp-content/uploads/2019/10/logo_2.png" class="header-logo-dark" alt="Mật ong"/></a>
          </div>

          <!-- Mobile Left Elements -->
          <div class="flex-col show-for-medium flex-left">
            <ul class="mobile-nav nav nav-left ">
              <li class="nav-icon has-icon">
  		<a href="#" data-open="#main-menu" data-pos="left" data-bg="main-menu-overlay" data-color="" class="is-small" aria-controls="main-menu" aria-expanded="false">
		
		  <i class="icon-menu" ></i>
		  		</a>
	</li>            </ul>
          </div>

          <!-- Left Elements -->
          <div class="flex-col hide-for-medium flex-left
            flex-grow">
            <ul class="header-nav header-nav-main nav nav-left  nav-uppercase" >
              <li class="header-search-form search-form html relative has-icon">
	<div class="header-search-form-wrapper">
		<div class="searchform-wrapper ux-search-box relative form- is-normal"><form role="search" method="get" class="searchform" action="http://hptmedia.vn/matong/">
		<div class="flex-row relative">
						<div class="flex-col search-form-categories">
			<select class="search_categories resize-select mb-0" name="product_cat"><option value="" selected='selected'>Tất cả</option><option value="san-pham">Sản phẩm</option></select>			</div><!-- .flex-col -->
									<div class="flex-col flex-grow">
			  <input type="search" class="search-field mb-0" name="s" value="" placeholder="Tìm kiếm&hellip;" />
		    <input type="hidden" name="post_type" value="product" />
        			</div><!-- .flex-col -->
			<div class="flex-col">
				<button type="submit" class="ux-search-submit submit-button secondary button icon mb-0">
					<i class="icon-search" ></i>				</button>
			</div><!-- .flex-col -->
		</div><!-- .flex-row -->
	 <div class="live-search-results text-left z-top"></div>
</form>
</div>	</div>
</li>            </ul>
          </div>

          <!-- Right Elements -->
          <div class="flex-col hide-for-medium flex-right">
            <ul class="header-nav header-nav-main nav nav-right  nav-uppercase">
              <li id="menu-item-126" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-11 current_page_item active  menu-item-126"><a href="http://hptmedia.vn/matong/" class="nav-top-link">Trang chủ</a></li>
<li id="menu-item-127" class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-127"><a href="http://hptmedia.vn/matong/gioi-thieu/" class="nav-top-link">Giới thiệu</a></li>
<li id="menu-item-129" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat  menu-item-129"><a href="http://hptmedia.vn/matong/danh-muc/san-pham/" class="nav-top-link">Sản phẩm</a></li>
<li id="menu-item-128" class="menu-item menu-item-type-taxonomy menu-item-object-category  menu-item-128"><a href="http://hptmedia.vn/matong/tin-tuc/" class="nav-top-link">Tin tức</a></li>
<li id="menu-item-243" class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-243"><a href="http://hptmedia.vn/matong/lien-he/" class="nav-top-link">Liên hệ</a></li>
            </ul>
          </div>

          <!-- Mobile Right Elements -->
          <div class="flex-col show-for-medium flex-right">
            <ul class="mobile-nav nav nav-right ">
              <li class="cart-item has-icon">

      <a href="http://hptmedia.vn/matong/gio-hang/" class="header-cart-link off-canvas-toggle nav-top-link is-small" data-open="#cart-popup" data-class="off-canvas-cart" title="Giỏ hàng" data-pos="right">
  
    <span class="cart-icon image-icon">
    <strong>0</strong>
  </span> 
  </a>


  <!-- Cart Sidebar Popup -->
  <div id="cart-popup" class="mfp-hide widget_shopping_cart">
  <div class="cart-popup-inner inner-padding">
      <div class="cart-popup-title text-center">
          <h4 class="uppercase">Giỏ hàng</h4>
          <div class="is-divider"></div>
      </div>
      <div class="widget_shopping_cart_content">
          

	<p class="woocommerce-mini-cart__empty-message">Chưa có sản phẩm trong giỏ hàng.</p>


      </div>
             <div class="cart-sidebar-content relative"></div>  </div>
  </div>

</li>
            </ul>
          </div>

      </div><!-- .header-inner -->
     
            <!-- Header divider -->
      <div class="container"><div class="top-divider full-width"></div></div>
      </div><!-- .header-main -->
<div class="header-bg-container fill"><div class="header-bg-image fill"></div><div class="header-bg-color fill"></div></div><!-- .header-bg-container -->   </div><!-- header-wrapper-->
</header>


<main id="main" class="">


<div id="content" role="main" class="content-area">

		
			<div class="slider-wrapper relative " id="slider-1015300165" >
    <div class="slider slider-nav-circle slider-nav-large slider-nav-light slider-style-normal"
        data-flickity-options='{
            "cellAlign": "center",
            "imagesLoaded": true,
            "lazyLoad": 1,
            "freeScroll": false,
            "wrapAround": true,
            "autoPlay": 6000,
            "pauseAutoPlayOnHover" : true,
            "prevNextButtons": true,
            "contain" : true,
            "adaptiveHeight" : true,
            "dragThreshold" : 10,
            "percentPosition": true,
            "pageDots": true,
            "rightToLeft": false,
            "draggable": true,
            "selectedAttraction": 0.1,
            "parallax" : 0,
            "friction": 0.6        }'
        >
        
	<div class="img has-hover x md-x lg-x y md-y lg-y" id="image_690638145">
						<div data-animate="fadeInLeft">		<div class="img-inner image-cover dark" style="padding-top:450px;">
			<img width="1002" height="342" src="http://hptmedia.vn/matong/wp-content/uploads/2019/10/banner_1.jpg" class="attachment-original size-original" alt="" srcset="http://hptmedia.vn/matong/wp-content/uploads/2019/10/banner_1.jpg 1002w, http://hptmedia.vn/matong/wp-content/uploads/2019/10/banner_1-300x102.jpg 300w, http://hptmedia.vn/matong/wp-content/uploads/2019/10/banner_1-768x262.jpg 768w, http://hptmedia.vn/matong/wp-content/uploads/2019/10/banner_1-510x174.jpg 510w" sizes="(max-width: 1002px) 100vw, 1002px" />						
					</div>
		</div>						
<style scope="scope">

#image_690638145 {
  width: 100%;
}
</style>
	</div>
	
	<div class="img has-hover x md-x lg-x y md-y lg-y" id="image_1995263263">
						<div data-animate="fadeInRight">		<div class="img-inner image-cover dark" style="padding-top:450px;">
			<img width="1020" height="498" src="http://hptmedia.vn/matong/wp-content/uploads/2019/10/banner_2-1-1024x500.jpg" class="attachment-large size-large" alt="" srcset="http://hptmedia.vn/matong/wp-content/uploads/2019/10/banner_2-1.jpg 1024w, http://hptmedia.vn/matong/wp-content/uploads/2019/10/banner_2-1-300x146.jpg 300w, http://hptmedia.vn/matong/wp-content/uploads/2019/10/banner_2-1-768x375.jpg 768w, http://hptmedia.vn/matong/wp-content/uploads/2019/10/banner_2-1-510x249.jpg 510w" sizes="(max-width: 1020px) 100vw, 1020px" />						
					</div>
		</div>						
<style scope="scope">

#image_1995263263 {
  width: 100%;
}
</style>
	</div>
	
	<div class="img has-hover x md-x lg-x y md-y lg-y" id="image_1425857831">
						<div data-animate="bounceInUp">		<div class="img-inner image-cover dark" style="padding-top:450px;">
			<img width="1349" height="430" src="http://hptmedia.vn/matong/wp-content/uploads/2019/10/banner_honey4.jpg" class="attachment-original size-original" alt="" srcset="http://hptmedia.vn/matong/wp-content/uploads/2019/10/banner_honey4.jpg 1349w, http://hptmedia.vn/matong/wp-content/uploads/2019/10/banner_honey4-300x96.jpg 300w, http://hptmedia.vn/matong/wp-content/uploads/2019/10/banner_honey4-768x245.jpg 768w, http://hptmedia.vn/matong/wp-content/uploads/2019/10/banner_honey4-1024x326.jpg 1024w, http://hptmedia.vn/matong/wp-content/uploads/2019/10/banner_honey4-510x163.jpg 510w" sizes="(max-width: 1349px) 100vw, 1349px" />						
					</div>
		</div>						
<style scope="scope">

#image_1425857831 {
  width: 100%;
}
</style>
	</div>
	
     </div>

     <div class="loading-spin dark large centered"></div>

     <style scope="scope">
             </style>
</div><!-- .ux-slider-wrapper -->


<div class="row row_tieuchi"  id="row-1986902024">
<div class="col medium-4 small-12 large-4"  data-animate="fadeInLeft"><div class="col-inner"  >
	<div class="box has-hover has-hover box-label box-text-bottom" >

		<div class="box-image" >
						<div class="image-cover" style="padding-top:250px;">
				<img width="480" height="300" src="http://hptmedia.vn/matong/wp-content/uploads/2019/10/image_1.jpg" class="attachment- size-" alt="" srcset="http://hptmedia.vn/matong/wp-content/uploads/2019/10/image_1.jpg 480w, http://hptmedia.vn/matong/wp-content/uploads/2019/10/image_1-300x188.jpg 300w" sizes="(max-width: 480px) 100vw, 480px" />											</div>
					</div><!-- box-image -->

		<div class="box-text text-center" >
			<div class="box-text-inner">
				
<h4>Mật ong nguyên chất</h4>
			</div><!-- box-text-inner -->
		</div><!-- box-text -->
	</div><!-- .box  -->
	
</div></div>
<div class="col medium-4 small-12 large-4"  data-animate="fadeInLeft"><div class="col-inner"  >
	<div class="box has-hover has-hover box-label box-text-bottom" >

		<div class="box-image" >
						<div class="image-cover" style="padding-top:250px;">
				<img width="600" height="400" src="http://hptmedia.vn/matong/wp-content/uploads/2019/10/image_2.jpg" class="attachment- size-" alt="" srcset="http://hptmedia.vn/matong/wp-content/uploads/2019/10/image_2.jpg 600w, http://hptmedia.vn/matong/wp-content/uploads/2019/10/image_2-300x200.jpg 300w, http://hptmedia.vn/matong/wp-content/uploads/2019/10/image_2-510x340.jpg 510w" sizes="(max-width: 600px) 100vw, 600px" />											</div>
					</div><!-- box-image -->

		<div class="box-text text-center" >
			<div class="box-text-inner">
				
<h4>Phấn hoa tự nhiên</h4>
			</div><!-- box-text-inner -->
		</div><!-- box-text -->
	</div><!-- .box  -->
	
</div></div>
<div class="col medium-4 small-12 large-4"  data-animate="fadeInLeft"><div class="col-inner"  >
	<div class="box has-hover has-hover box-label box-text-bottom" >

		<div class="box-image" >
						<div class="image-cover" style="padding-top:250px;">
				<img width="624" height="351" src="http://hptmedia.vn/matong/wp-content/uploads/2019/10/image_3.jpg" class="attachment- size-" alt="" srcset="http://hptmedia.vn/matong/wp-content/uploads/2019/10/image_3.jpg 624w, http://hptmedia.vn/matong/wp-content/uploads/2019/10/image_3-300x169.jpg 300w, http://hptmedia.vn/matong/wp-content/uploads/2019/10/image_3-510x287.jpg 510w" sizes="(max-width: 624px) 100vw, 624px" />											</div>
					</div><!-- box-image -->

		<div class="box-text text-center" >
			<div class="box-text-inner">
				
<h4>Chất lượng quốc tế</h4>
			</div><!-- box-text-inner -->
		</div><!-- box-text -->
	</div><!-- .box  -->
	
</div></div>

<style scope="scope">

</style>
</div>
<div class="gap-element" style="display:block; height:auto; padding-top:40px" class="clearfix"></div>
<div class="container section-title-container" style="margin-bottom:0px;"><h3 class="section-title section-title-center"><b></b><span class="section-title-main" >Sản phẩm của chúng tôi</span><b></b></h3></div><!-- .section-title -->
<div class="row"  id="row-735823400">
<div class="col small-12 large-12"  ><div class="col-inner"  >
<p style="text-align: center;">Mật ong lấy từ rừng U Minh và Cao nguyên hùng vĩ</p>
</div></div>
</div>

  
    <div class="row large-columns-4 medium-columns- small-columns-2 row-small">
  	
	     
					
<div class="product-small col has-hover product type-product post-60 status-publish first instock product_cat-san-pham has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
	<div class="col-inner">
	
<div class="badge-container absolute left top z-1">
		<div class="callout badge badge-square"><div class="badge-inner secondary on-sale"><span class="onsale">-44%</span></div></div>
</div>
	<div class="product-small box ">
		<div class="box-image">
			<div class="image-fade_in_back">
				<a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai/">
					<img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/product-247x296.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" /><img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/img/lazy.png" data-src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/5-247x296.jpg" class="lazy-load show-on-hover absolute fill hide-for-small back-image" alt="" />				</a>
			</div>
			<div class="image-tools is-small top right show-on-hover">
							</div>
			<div class="image-tools is-small hide-for-small bottom left show-on-hover">
							</div>
			<div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
							</div>
					</div><!-- box-image -->

		<div class="box-text box-text-products text-center grid-style-2">
			<div class="title-wrapper"><p class="name product-title"><a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai/">Mật ong hoa nhãn loại</a></p></div><div class="price-wrapper">
	<span class="price"><del><span class="woocommerce-Price-amount amount">1,690,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></del> <ins><span class="woocommerce-Price-amount amount">950,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></ins></span>
</div><div class="add-to-cart-button"><a href="?add-to-cart=60" rel="nofollow" data-product_id="60" class="ajax_add_to_cart add_to_cart_button product_type_simple button primary is-outline mb-0 is-small">Mua hàng</a></div>		</div><!-- box-text -->
	</div><!-- box -->
		</div><!-- .col-inner -->
</div><!-- col -->
	            
					
<div class="product-small col has-hover product type-product post-62 status-publish instock product_cat-san-pham has-post-thumbnail shipping-taxable purchasable product-type-simple">
	<div class="col-inner">
	
<div class="badge-container absolute left top z-1">
</div>
	<div class="product-small box ">
		<div class="box-image">
			<div class="image-fade_in_back">
				<a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-5/">
					<img width="225" height="224" src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/product_2.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="http://hptmedia.vn/matong/wp-content/uploads/2019/01/product_2.jpg 225w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/product_2-150x150.jpg 150w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/product_2-100x100.jpg 100w" sizes="(max-width: 225px) 100vw, 225px" /><img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/img/lazy.png" data-src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/product_4-247x296.jpg" class="lazy-load show-on-hover absolute fill hide-for-small back-image" alt="" />				</a>
			</div>
			<div class="image-tools is-small top right show-on-hover">
							</div>
			<div class="image-tools is-small hide-for-small bottom left show-on-hover">
							</div>
			<div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
							</div>
					</div><!-- box-image -->

		<div class="box-text box-text-products text-center grid-style-2">
			<div class="title-wrapper"><p class="name product-title"><a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-5/">Mật ong hoa nhãn loại 5</a></p></div><div class="price-wrapper">
	<span class="price"><span class="woocommerce-Price-amount amount">1,690,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
</div><div class="add-to-cart-button"><a href="?add-to-cart=62" rel="nofollow" data-product_id="62" class="ajax_add_to_cart add_to_cart_button product_type_simple button primary is-outline mb-0 is-small">Mua hàng</a></div>		</div><!-- box-text -->
	</div><!-- box -->
		</div><!-- .col-inner -->
</div><!-- col -->
	            
					
<div class="product-small col has-hover product type-product post-63 status-publish instock product_cat-san-pham has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
	<div class="col-inner">
	
<div class="badge-container absolute left top z-1">
		<div class="callout badge badge-square"><div class="badge-inner secondary on-sale"><span class="onsale">-44%</span></div></div>
</div>
	<div class="product-small box ">
		<div class="box-image">
			<div class="image-fade_in_back">
				<a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-6/">
					<img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/10-247x296.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" /><img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/img/lazy.png" data-src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/product-247x296.jpg" class="lazy-load show-on-hover absolute fill hide-for-small back-image" alt="" />				</a>
			</div>
			<div class="image-tools is-small top right show-on-hover">
							</div>
			<div class="image-tools is-small hide-for-small bottom left show-on-hover">
							</div>
			<div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
							</div>
					</div><!-- box-image -->

		<div class="box-text box-text-products text-center grid-style-2">
			<div class="title-wrapper"><p class="name product-title"><a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-6/">Mật ong hoa nhãn loại 6</a></p></div><div class="price-wrapper">
	<span class="price"><del><span class="woocommerce-Price-amount amount">1,690,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></del> <ins><span class="woocommerce-Price-amount amount">950,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></ins></span>
</div><div class="add-to-cart-button"><a href="?add-to-cart=63" rel="nofollow" data-product_id="63" class="ajax_add_to_cart add_to_cart_button product_type_simple button primary is-outline mb-0 is-small">Mua hàng</a></div>		</div><!-- box-text -->
	</div><!-- box -->
		</div><!-- .col-inner -->
</div><!-- col -->
	            
					
<div class="product-small col has-hover product type-product post-64 status-publish last instock product_cat-san-pham has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
	<div class="col-inner">
	
<div class="badge-container absolute left top z-1">
		<div class="callout badge badge-square"><div class="badge-inner secondary on-sale"><span class="onsale">-44%</span></div></div>
</div>
	<div class="product-small box ">
		<div class="box-image">
			<div class="image-fade_in_back">
				<a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-7/">
					<img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/7-247x296.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" /><img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/img/lazy.png" data-src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/2-247x296.jpg" class="lazy-load show-on-hover absolute fill hide-for-small back-image" alt="" />				</a>
			</div>
			<div class="image-tools is-small top right show-on-hover">
							</div>
			<div class="image-tools is-small hide-for-small bottom left show-on-hover">
							</div>
			<div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
							</div>
					</div><!-- box-image -->

		<div class="box-text box-text-products text-center grid-style-2">
			<div class="title-wrapper"><p class="name product-title"><a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-7/">Mật ong hoa nhãn loại 7</a></p></div><div class="price-wrapper">
	<span class="price"><del><span class="woocommerce-Price-amount amount">1,690,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></del> <ins><span class="woocommerce-Price-amount amount">950,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></ins></span>
</div><div class="add-to-cart-button"><a href="?add-to-cart=64" rel="nofollow" data-product_id="64" class="ajax_add_to_cart add_to_cart_button product_type_simple button primary is-outline mb-0 is-small">Mua hàng</a></div>		</div><!-- box-text -->
	</div><!-- box -->
		</div><!-- .col-inner -->
</div><!-- col -->
	            
					
<div class="product-small col has-hover product type-product post-65 status-publish first instock product_cat-san-pham has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
	<div class="col-inner">
	
<div class="badge-container absolute left top z-1">
		<div class="callout badge badge-square"><div class="badge-inner secondary on-sale"><span class="onsale">-44%</span></div></div>
</div>
	<div class="product-small box ">
		<div class="box-image">
			<div class="image-fade_in_back">
				<a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-8/">
					<img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/product_5-247x296.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" /><img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/img/lazy.png" data-src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/product_6-247x296.jpg" class="lazy-load show-on-hover absolute fill hide-for-small back-image" alt="" />				</a>
			</div>
			<div class="image-tools is-small top right show-on-hover">
							</div>
			<div class="image-tools is-small hide-for-small bottom left show-on-hover">
							</div>
			<div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
							</div>
					</div><!-- box-image -->

		<div class="box-text box-text-products text-center grid-style-2">
			<div class="title-wrapper"><p class="name product-title"><a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-8/">Mật ong Đức</a></p></div><div class="price-wrapper">
	<span class="price"><del><span class="woocommerce-Price-amount amount">1,690,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></del> <ins><span class="woocommerce-Price-amount amount">950,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></ins></span>
</div><div class="add-to-cart-button"><a href="?add-to-cart=65" rel="nofollow" data-product_id="65" class="ajax_add_to_cart add_to_cart_button product_type_simple button primary is-outline mb-0 is-small">Mua hàng</a></div>		</div><!-- box-text -->
	</div><!-- box -->
		</div><!-- .col-inner -->
</div><!-- col -->
	            
					
<div class="product-small col has-hover product type-product post-59 status-publish instock product_cat-san-pham has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
	<div class="col-inner">
	
<div class="badge-container absolute left top z-1">
		<div class="callout badge badge-square"><div class="badge-inner secondary on-sale"><span class="onsale">-44%</span></div></div>
</div>
	<div class="product-small box ">
		<div class="box-image">
			<div class="image-fade_in_back">
				<a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-2/">
					<img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/product_3-247x296.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" /><img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/img/lazy.png" data-src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/7-247x296.jpg" class="lazy-load show-on-hover absolute fill hide-for-small back-image" alt="" />				</a>
			</div>
			<div class="image-tools is-small top right show-on-hover">
							</div>
			<div class="image-tools is-small hide-for-small bottom left show-on-hover">
							</div>
			<div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
							</div>
					</div><!-- box-image -->

		<div class="box-text box-text-products text-center grid-style-2">
			<div class="title-wrapper"><p class="name product-title"><a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-2/">Mật ong Anh</a></p></div><div class="price-wrapper">
	<span class="price"><del><span class="woocommerce-Price-amount amount">1,690,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></del> <ins><span class="woocommerce-Price-amount amount">950,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></ins></span>
</div><div class="add-to-cart-button"><a href="?add-to-cart=59" rel="nofollow" data-product_id="59" class="ajax_add_to_cart add_to_cart_button product_type_simple button primary is-outline mb-0 is-small">Mua hàng</a></div>		</div><!-- box-text -->
	</div><!-- box -->
		</div><!-- .col-inner -->
</div><!-- col -->
	            
					
<div class="product-small col has-hover product type-product post-66 status-publish instock product_cat-san-pham has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
	<div class="col-inner">
	
<div class="badge-container absolute left top z-1">
		<div class="callout badge badge-square"><div class="badge-inner secondary on-sale"><span class="onsale">-44%</span></div></div>
</div>
	<div class="product-small box ">
		<div class="box-image">
			<div class="image-fade_in_back">
				<a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-9/">
					<img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/1-247x296.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" /><img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/img/lazy.png" data-src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/product-247x296.jpg" class="lazy-load show-on-hover absolute fill hide-for-small back-image" alt="" />				</a>
			</div>
			<div class="image-tools is-small top right show-on-hover">
							</div>
			<div class="image-tools is-small hide-for-small bottom left show-on-hover">
							</div>
			<div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
							</div>
					</div><!-- box-image -->

		<div class="box-text box-text-products text-center grid-style-2">
			<div class="title-wrapper"><p class="name product-title"><a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-9/">Mật ong Pháp</a></p></div><div class="price-wrapper">
	<span class="price"><del><span class="woocommerce-Price-amount amount">1,690,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></del> <ins><span class="woocommerce-Price-amount amount">950,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></ins></span>
</div><div class="add-to-cart-button"><a href="?add-to-cart=66" rel="nofollow" data-product_id="66" class="ajax_add_to_cart add_to_cart_button product_type_simple button primary is-outline mb-0 is-small">Mua hàng</a></div>		</div><!-- box-text -->
	</div><!-- box -->
		</div><!-- .col-inner -->
</div><!-- col -->
	            
					
<div class="product-small col has-hover product type-product post-67 status-publish last instock product_cat-san-pham has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
	<div class="col-inner">
	
<div class="badge-container absolute left top z-1">
		<div class="callout badge badge-square"><div class="badge-inner secondary on-sale"><span class="onsale">-44%</span></div></div>
</div>
	<div class="product-small box ">
		<div class="box-image">
			<div class="image-fade_in_back">
				<a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-10/">
					<img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/product_6-247x296.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" /><img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/img/lazy.png" data-src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/product_5-247x296.png" class="lazy-load show-on-hover absolute fill hide-for-small back-image" alt="" />				</a>
			</div>
			<div class="image-tools is-small top right show-on-hover">
							</div>
			<div class="image-tools is-small hide-for-small bottom left show-on-hover">
							</div>
			<div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
							</div>
					</div><!-- box-image -->

		<div class="box-text box-text-products text-center grid-style-2">
			<div class="title-wrapper"><p class="name product-title"><a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-10/">Mật ong hoa nhãn Hoa Kỳ</a></p></div><div class="price-wrapper">
	<span class="price"><del><span class="woocommerce-Price-amount amount">1,690,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></del> <ins><span class="woocommerce-Price-amount amount">950,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></ins></span>
</div><div class="add-to-cart-button"><a href="?add-to-cart=67" rel="nofollow" data-product_id="67" class="ajax_add_to_cart add_to_cart_button product_type_simple button primary is-outline mb-0 is-small">Mua hàng</a></div>		</div><!-- box-text -->
	</div><!-- box -->
		</div><!-- .col-inner -->
</div><!-- col -->
	            
	        </div>
<div class="row"  id="row-665576003">
<div class="col small-12 large-12"  ><div class="col-inner text-center"  >
<a data-animate="flipInX" href="https://hptmedia.vn/matong/?product_cat=san-pham" target="_self" class="button primary is-outline lowercase"  style="border-radius:99px;">
    <span>Xem thêm</span>
  </a>

</div></div>
</div>
<div class="gap-element" style="display:block; height:auto; padding-top:0px" class="clearfix"></div>
	<section class="section block__3 hide-for-small" id="section_896156908">
		<div class="bg section-bg fill bg-fill  " >

			
			
			

		</div><!-- .section-bg -->

		<div class="section-content relative">
			
<div class="row row-small align-middle"  id="row-203033681">
<div class="col medium-5 small-12 large-5"  data-animate="bounceInRight"><div class="col-inner" style="margin:0px 0px 320px 0px;" >
<p style="text-align: right; font-weight 500; font-size: 16px;"><span style="color: #000000;">100% MẬT ONG RỪNG</span></p>
<p style="text-align: right; font-size: 13px;"><span style="color: #000000;">Với những vách núi treo leo chúng tôi phải vô cùng khéo léo và mạo hiểm. để có thể mang đến cho quý vị nhũng giọt mật tinh túy nhất</span></p>
<p> </p>
<p style="text-align: right; font-weight 500; font-size: 16px;"><span style="color: #000000;">VỆ SINH AN TOÀN</span></p>
<p style="text-align: right; font-size: 13px;"><span style="color: #000000;">Với những vách núi treo leo chúng tôi phải vô cùng khéo léo và mạo hiểm. để có thể mang đến cho quý vị nhũng giọt mật tinh túy nhất</span></p>
</div></div>
<div class="col medium-2 small-12 large-2"  data-animate="bounceInDown"><div class="col-inner" style="margin:0px 0px 320px 0px;" >
	<div class="img has-hover x md-x lg-x y md-y lg-y" id="image_36273100">
								<div class="img-inner dark" >
			<img width="168" height="132" src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/logob.png" class="attachment-large size-large" alt="" />						
					</div>
								
<style scope="scope">

#image_36273100 {
  width: 100%;
}
</style>
	</div>
	
</div></div>
<div class="col medium-5 small-12 large-5"  data-animate="bounceInLeft"><div class="col-inner" style="margin:0px 0px 320px 0px;" >
<p style="font-weight 500; font-size: 16px;"><span style="color: #000000;">CHỨNG NHẬN HÀNG CHẤT LƯỢNG CAO</span></p>
<p style="font-size: 13px;"><span style="color: #000000;">Với những vách núi treo leo chúng tôi phải vô cùng khéo léo và mạo hiểm. để có thể mang đến cho quý vị nhũng giọt mật tinh túy nhất</span></p>
<p> </p>
<p style="font-weight 500; font-size: 16px;"><span style="color: #000000;">TƯ VẤN TẬN TÌNH &#8211; GIAO HÀNG NHANH</span></p>
<p style="font-size: 13px;"><span style="color: #000000;">Với những vách núi treo leo chúng tôi phải vô cùng khéo léo và mạo hiểm. để có thể mang đến cho quý vị nhũng giọt mật tinh túy nhất</span></p>
</div></div>

<style scope="scope">

</style>
</div>
		</div><!-- .section-content -->

		
<style scope="scope">

#section_896156908 {
  padding-top: 40px;
  padding-bottom: 40px;
}
#section_896156908 .section-bg.bg-loaded {
  background-image: url(http://hptmedia.vn/matong/wp-content/uploads/2019/01/bg1.jpg);
}
</style>
	</section>
	
<div class="gap-element" style="display:block; height:auto; padding-top:45px" class="clearfix"></div>
<div class="container section-title-container" style="margin-bottom:0px;"><h3 class="section-title section-title-center"><b></b><span class="section-title-main" >Sản phẩm bán chạy nhất</span><b></b></h3></div><!-- .section-title -->
<div class="row"  id="row-661320248">
<div class="col small-12 large-12"  ><div class="col-inner"  >
<p style="text-align: center;">Sản phẩm của chúng tôi là sản phẩm 100% tự nhiên</p>
</div></div>
</div>

  
    <div class="row large-columns-4 medium-columns- small-columns-2 row-small">
  	
	     
					
<div class="product-small col has-hover product type-product post-60 status-publish first instock product_cat-san-pham has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
	<div class="col-inner">
	
<div class="badge-container absolute left top z-1">
		<div class="callout badge badge-square"><div class="badge-inner secondary on-sale"><span class="onsale">-44%</span></div></div>
</div>
	<div class="product-small box ">
		<div class="box-image">
			<div class="image-fade_in_back">
				<a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai/">
					<img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/product-247x296.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" /><img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/img/lazy.png" data-src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/5-247x296.jpg" class="lazy-load show-on-hover absolute fill hide-for-small back-image" alt="" />				</a>
			</div>
			<div class="image-tools is-small top right show-on-hover">
							</div>
			<div class="image-tools is-small hide-for-small bottom left show-on-hover">
							</div>
			<div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
							</div>
					</div><!-- box-image -->

		<div class="box-text box-text-products text-center grid-style-2">
			<div class="title-wrapper"><p class="name product-title"><a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai/">Mật ong hoa nhãn loại</a></p></div><div class="price-wrapper">
	<span class="price"><del><span class="woocommerce-Price-amount amount">1,690,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></del> <ins><span class="woocommerce-Price-amount amount">950,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></ins></span>
</div><div class="add-to-cart-button"><a href="?add-to-cart=60" rel="nofollow" data-product_id="60" class="ajax_add_to_cart add_to_cart_button product_type_simple button primary is-outline mb-0 is-small">Mua hàng</a></div>		</div><!-- box-text -->
	</div><!-- box -->
		</div><!-- .col-inner -->
</div><!-- col -->
	            
					
<div class="product-small col has-hover product type-product post-62 status-publish instock product_cat-san-pham has-post-thumbnail shipping-taxable purchasable product-type-simple">
	<div class="col-inner">
	
<div class="badge-container absolute left top z-1">
</div>
	<div class="product-small box ">
		<div class="box-image">
			<div class="image-fade_in_back">
				<a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-5/">
					<img width="225" height="224" src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/product_2.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="http://hptmedia.vn/matong/wp-content/uploads/2019/01/product_2.jpg 225w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/product_2-150x150.jpg 150w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/product_2-100x100.jpg 100w" sizes="(max-width: 225px) 100vw, 225px" /><img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/img/lazy.png" data-src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/product_4-247x296.jpg" class="lazy-load show-on-hover absolute fill hide-for-small back-image" alt="" />				</a>
			</div>
			<div class="image-tools is-small top right show-on-hover">
							</div>
			<div class="image-tools is-small hide-for-small bottom left show-on-hover">
							</div>
			<div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
							</div>
					</div><!-- box-image -->

		<div class="box-text box-text-products text-center grid-style-2">
			<div class="title-wrapper"><p class="name product-title"><a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-5/">Mật ong hoa nhãn loại 5</a></p></div><div class="price-wrapper">
	<span class="price"><span class="woocommerce-Price-amount amount">1,690,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
</div><div class="add-to-cart-button"><a href="?add-to-cart=62" rel="nofollow" data-product_id="62" class="ajax_add_to_cart add_to_cart_button product_type_simple button primary is-outline mb-0 is-small">Mua hàng</a></div>		</div><!-- box-text -->
	</div><!-- box -->
		</div><!-- .col-inner -->
</div><!-- col -->
	            
					
<div class="product-small col has-hover product type-product post-63 status-publish instock product_cat-san-pham has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
	<div class="col-inner">
	
<div class="badge-container absolute left top z-1">
		<div class="callout badge badge-square"><div class="badge-inner secondary on-sale"><span class="onsale">-44%</span></div></div>
</div>
	<div class="product-small box ">
		<div class="box-image">
			<div class="image-fade_in_back">
				<a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-6/">
					<img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/10-247x296.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" /><img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/img/lazy.png" data-src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/product-247x296.jpg" class="lazy-load show-on-hover absolute fill hide-for-small back-image" alt="" />				</a>
			</div>
			<div class="image-tools is-small top right show-on-hover">
							</div>
			<div class="image-tools is-small hide-for-small bottom left show-on-hover">
							</div>
			<div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
							</div>
					</div><!-- box-image -->

		<div class="box-text box-text-products text-center grid-style-2">
			<div class="title-wrapper"><p class="name product-title"><a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-6/">Mật ong hoa nhãn loại 6</a></p></div><div class="price-wrapper">
	<span class="price"><del><span class="woocommerce-Price-amount amount">1,690,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></del> <ins><span class="woocommerce-Price-amount amount">950,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></ins></span>
</div><div class="add-to-cart-button"><a href="?add-to-cart=63" rel="nofollow" data-product_id="63" class="ajax_add_to_cart add_to_cart_button product_type_simple button primary is-outline mb-0 is-small">Mua hàng</a></div>		</div><!-- box-text -->
	</div><!-- box -->
		</div><!-- .col-inner -->
</div><!-- col -->
	            
					
<div class="product-small col has-hover product type-product post-64 status-publish last instock product_cat-san-pham has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
	<div class="col-inner">
	
<div class="badge-container absolute left top z-1">
		<div class="callout badge badge-square"><div class="badge-inner secondary on-sale"><span class="onsale">-44%</span></div></div>
</div>
	<div class="product-small box ">
		<div class="box-image">
			<div class="image-fade_in_back">
				<a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-7/">
					<img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/7-247x296.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" /><img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/img/lazy.png" data-src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/2-247x296.jpg" class="lazy-load show-on-hover absolute fill hide-for-small back-image" alt="" />				</a>
			</div>
			<div class="image-tools is-small top right show-on-hover">
							</div>
			<div class="image-tools is-small hide-for-small bottom left show-on-hover">
							</div>
			<div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
							</div>
					</div><!-- box-image -->

		<div class="box-text box-text-products text-center grid-style-2">
			<div class="title-wrapper"><p class="name product-title"><a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-7/">Mật ong hoa nhãn loại 7</a></p></div><div class="price-wrapper">
	<span class="price"><del><span class="woocommerce-Price-amount amount">1,690,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></del> <ins><span class="woocommerce-Price-amount amount">950,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></ins></span>
</div><div class="add-to-cart-button"><a href="?add-to-cart=64" rel="nofollow" data-product_id="64" class="ajax_add_to_cart add_to_cart_button product_type_simple button primary is-outline mb-0 is-small">Mua hàng</a></div>		</div><!-- box-text -->
	</div><!-- box -->
		</div><!-- .col-inner -->
</div><!-- col -->
	            
					
<div class="product-small col has-hover product type-product post-65 status-publish first instock product_cat-san-pham has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
	<div class="col-inner">
	
<div class="badge-container absolute left top z-1">
		<div class="callout badge badge-square"><div class="badge-inner secondary on-sale"><span class="onsale">-44%</span></div></div>
</div>
	<div class="product-small box ">
		<div class="box-image">
			<div class="image-fade_in_back">
				<a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-8/">
					<img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/product_5-247x296.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" /><img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/img/lazy.png" data-src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/product_6-247x296.jpg" class="lazy-load show-on-hover absolute fill hide-for-small back-image" alt="" />				</a>
			</div>
			<div class="image-tools is-small top right show-on-hover">
							</div>
			<div class="image-tools is-small hide-for-small bottom left show-on-hover">
							</div>
			<div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
							</div>
					</div><!-- box-image -->

		<div class="box-text box-text-products text-center grid-style-2">
			<div class="title-wrapper"><p class="name product-title"><a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-8/">Mật ong Đức</a></p></div><div class="price-wrapper">
	<span class="price"><del><span class="woocommerce-Price-amount amount">1,690,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></del> <ins><span class="woocommerce-Price-amount amount">950,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></ins></span>
</div><div class="add-to-cart-button"><a href="?add-to-cart=65" rel="nofollow" data-product_id="65" class="ajax_add_to_cart add_to_cart_button product_type_simple button primary is-outline mb-0 is-small">Mua hàng</a></div>		</div><!-- box-text -->
	</div><!-- box -->
		</div><!-- .col-inner -->
</div><!-- col -->
	            
					
<div class="product-small col has-hover product type-product post-59 status-publish instock product_cat-san-pham has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
	<div class="col-inner">
	
<div class="badge-container absolute left top z-1">
		<div class="callout badge badge-square"><div class="badge-inner secondary on-sale"><span class="onsale">-44%</span></div></div>
</div>
	<div class="product-small box ">
		<div class="box-image">
			<div class="image-fade_in_back">
				<a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-2/">
					<img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/product_3-247x296.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" /><img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/img/lazy.png" data-src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/7-247x296.jpg" class="lazy-load show-on-hover absolute fill hide-for-small back-image" alt="" />				</a>
			</div>
			<div class="image-tools is-small top right show-on-hover">
							</div>
			<div class="image-tools is-small hide-for-small bottom left show-on-hover">
							</div>
			<div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
							</div>
					</div><!-- box-image -->

		<div class="box-text box-text-products text-center grid-style-2">
			<div class="title-wrapper"><p class="name product-title"><a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-2/">Mật ong Anh</a></p></div><div class="price-wrapper">
	<span class="price"><del><span class="woocommerce-Price-amount amount">1,690,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></del> <ins><span class="woocommerce-Price-amount amount">950,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></ins></span>
</div><div class="add-to-cart-button"><a href="?add-to-cart=59" rel="nofollow" data-product_id="59" class="ajax_add_to_cart add_to_cart_button product_type_simple button primary is-outline mb-0 is-small">Mua hàng</a></div>		</div><!-- box-text -->
	</div><!-- box -->
		</div><!-- .col-inner -->
</div><!-- col -->
	            
					
<div class="product-small col has-hover product type-product post-66 status-publish instock product_cat-san-pham has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
	<div class="col-inner">
	
<div class="badge-container absolute left top z-1">
		<div class="callout badge badge-square"><div class="badge-inner secondary on-sale"><span class="onsale">-44%</span></div></div>
</div>
	<div class="product-small box ">
		<div class="box-image">
			<div class="image-fade_in_back">
				<a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-9/">
					<img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/1-247x296.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" /><img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/img/lazy.png" data-src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/product-247x296.jpg" class="lazy-load show-on-hover absolute fill hide-for-small back-image" alt="" />				</a>
			</div>
			<div class="image-tools is-small top right show-on-hover">
							</div>
			<div class="image-tools is-small hide-for-small bottom left show-on-hover">
							</div>
			<div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
							</div>
					</div><!-- box-image -->

		<div class="box-text box-text-products text-center grid-style-2">
			<div class="title-wrapper"><p class="name product-title"><a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-9/">Mật ong Pháp</a></p></div><div class="price-wrapper">
	<span class="price"><del><span class="woocommerce-Price-amount amount">1,690,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></del> <ins><span class="woocommerce-Price-amount amount">950,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></ins></span>
</div><div class="add-to-cart-button"><a href="?add-to-cart=66" rel="nofollow" data-product_id="66" class="ajax_add_to_cart add_to_cart_button product_type_simple button primary is-outline mb-0 is-small">Mua hàng</a></div>		</div><!-- box-text -->
	</div><!-- box -->
		</div><!-- .col-inner -->
</div><!-- col -->
	            
					
<div class="product-small col has-hover product type-product post-67 status-publish last instock product_cat-san-pham has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
	<div class="col-inner">
	
<div class="badge-container absolute left top z-1">
		<div class="callout badge badge-square"><div class="badge-inner secondary on-sale"><span class="onsale">-44%</span></div></div>
</div>
	<div class="product-small box ">
		<div class="box-image">
			<div class="image-fade_in_back">
				<a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-10/">
					<img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/product_6-247x296.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" /><img width="247" height="296" src="http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/img/lazy.png" data-src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/product_5-247x296.png" class="lazy-load show-on-hover absolute fill hide-for-small back-image" alt="" />				</a>
			</div>
			<div class="image-tools is-small top right show-on-hover">
							</div>
			<div class="image-tools is-small hide-for-small bottom left show-on-hover">
							</div>
			<div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
							</div>
					</div><!-- box-image -->

		<div class="box-text box-text-products text-center grid-style-2">
			<div class="title-wrapper"><p class="name product-title"><a href="http://hptmedia.vn/matong/product/mat-ong-hoa-nhan-loai-10/">Mật ong hoa nhãn Hoa Kỳ</a></p></div><div class="price-wrapper">
	<span class="price"><del><span class="woocommerce-Price-amount amount">1,690,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></del> <ins><span class="woocommerce-Price-amount amount">950,000<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></ins></span>
</div><div class="add-to-cart-button"><a href="?add-to-cart=67" rel="nofollow" data-product_id="67" class="ajax_add_to_cart add_to_cart_button product_type_simple button primary is-outline mb-0 is-small">Mua hàng</a></div>		</div><!-- box-text -->
	</div><!-- box -->
		</div><!-- .col-inner -->
</div><!-- col -->
	            
	        </div>
<div class="gap-element" style="display:block; height:auto; padding-top:20px" class="clearfix"></div>
<div class="container section-title-container" style="margin-bottom:0px;"><h3 class="section-title section-title-center"><b></b><span class="section-title-main" >Chia sẻ của khách hàng</span><b></b></h3></div><!-- .section-title -->
<div class="row"  id="row-1316754184">
<div class="col small-12 large-12"  ><div class="col-inner" style="margin:0px 0px -30px 0px;" >
<p style="text-align: center;">Sản phẩm của chúng tôi là sản phẩm 100% tự nhiên</p>
</div></div>
</div>
<div class="slider-wrapper relative " id="slider-2007996589" >
    <div class="slider slider-nav-dots-square slider-nav-circle slider-nav-large slider-nav-dark slider-style-normal"
        data-flickity-options='{
            "cellAlign": "center",
            "imagesLoaded": true,
            "lazyLoad": 1,
            "freeScroll": false,
            "wrapAround": true,
            "autoPlay": 6000,
            "pauseAutoPlayOnHover" : true,
            "prevNextButtons": false,
            "contain" : true,
            "adaptiveHeight" : true,
            "dragThreshold" : 10,
            "percentPosition": true,
            "pageDots": false,
            "rightToLeft": false,
            "draggable": true,
            "selectedAttraction": 0.1,
            "parallax" : 0,
            "friction": 0.6        }'
        >
        
<div class="row"  id="row-32251924">
<div class="col medium-4 small-12 large-4"  data-animate="bounceInRight"><div class="col-inner box-shadow-2" style="padding:10px 10px 10px 10px;" >
  <div class="icon-box testimonial-box icon-box-center text-center is-small">
                <div class="icon-box-img testimonial-image circle" style="width: 135px">
              <img width="150" height="150" src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/6-1-150x150.jpg" class="attachment-thumbnail size-thumbnail" alt="" srcset="http://hptmedia.vn/matong/wp-content/uploads/2019/01/6-1-150x150.jpg 150w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/6-1-510x510.jpg 510w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/6-1-300x300.jpg 300w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/6-1-768x768.jpg 768w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/6-1-100x100.jpg 100w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/6-1.jpg 960w" sizes="(max-width: 150px) 100vw, 150px" />        </div>
                <div class="icon-box-text p-last-0">
          <div class="star-rating"><span style="width:100%"><strong class="rating"></strong></span></div>  				<div class="testimonial-text line-height-small italic test_text first-reset last-reset is-italic">
            
<p>Mình vô cùng ưng ý sp của Shop. Chất rất đẹp, rẻ hơn những nơi khác mình tham khảo. Thời gian giao hàng nhanh, đóng gói cẩn thận, bên trong còn cho túi đẹp đựng áo đi kèm. Cảm ơn Shop rất nhiều</p>
          </div>
          <div class="testimonial-meta pt-half">
             <strong class="testimonial-name test_name">Trần Thảo</strong>
             <span class="testimonial-name-divider"> / </span>             <span class="testimonial-company test_company">Facebook</span>
          </div>
        </div>
  </div><!-- .icon-box -->

  
</div></div>
<div class="col medium-4 small-12 large-4"  data-animate="bounceInRight"><div class="col-inner box-shadow-2" style="padding:10px 10px 10px 10px;" >
  <div class="icon-box testimonial-box icon-box-center text-center is-small">
                <div class="icon-box-img testimonial-image circle" style="width: 135px">
              <img width="150" height="150" src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/4-1-150x150.jpg" class="attachment-thumbnail size-thumbnail" alt="" srcset="http://hptmedia.vn/matong/wp-content/uploads/2019/01/4-1-150x150.jpg 150w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/4-1-510x508.jpg 510w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/4-1-300x300.jpg 300w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/4-1-100x100.jpg 100w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/4-1.jpg 520w" sizes="(max-width: 150px) 100vw, 150px" />        </div>
                <div class="icon-box-text p-last-0">
          <div class="star-rating"><span style="width:100%"><strong class="rating"></strong></span></div>  				<div class="testimonial-text line-height-small italic test_text first-reset last-reset is-italic">
            
<p>Mình vô cùng ưng ý sp của Shop. Chất rất đẹp, rẻ hơn những nơi khác mình tham khảo. Thời gian giao hàng nhanh, đóng gói cẩn thận, bên trong còn cho túi đẹp đựng áo đi kèm. Cảm ơn Shop rất nhiều</p>
          </div>
          <div class="testimonial-meta pt-half">
             <strong class="testimonial-name test_name">Lan Anh</strong>
             <span class="testimonial-name-divider"> / </span>             <span class="testimonial-company test_company">Facebook</span>
          </div>
        </div>
  </div><!-- .icon-box -->

  
</div></div>
<div class="col medium-4 small-12 large-4"  data-animate="bounceInRight"><div class="col-inner box-shadow-2" style="padding:10px 10px 10px 10px;" >
  <div class="icon-box testimonial-box icon-box-center text-center is-small">
                <div class="icon-box-img testimonial-image circle" style="width: 135px">
              <img width="150" height="150" src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/1-1-150x150.jpg" class="attachment-thumbnail size-thumbnail" alt="" srcset="http://hptmedia.vn/matong/wp-content/uploads/2019/01/1-1-150x150.jpg 150w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/1-1-510x510.jpg 510w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/1-1-300x300.jpg 300w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/1-1-100x100.jpg 100w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/1-1.jpg 640w" sizes="(max-width: 150px) 100vw, 150px" />        </div>
                <div class="icon-box-text p-last-0">
          <div class="star-rating"><span style="width:100%"><strong class="rating"></strong></span></div>  				<div class="testimonial-text line-height-small italic test_text first-reset last-reset is-italic">
            
<p>Mình vô cùng ưng ý sp của Shop. Chất rất đẹp, rẻ hơn những nơi khác mình tham khảo. Thời gian giao hàng nhanh, đóng gói cẩn thận, bên trong còn cho túi đẹp đựng áo đi kèm. Cảm ơn Shop rất nhiều</p>
          </div>
          <div class="testimonial-meta pt-half">
             <strong class="testimonial-name test_name">Bảo Ngọc</strong>
                          <span class="testimonial-company test_company"></span>
          </div>
        </div>
  </div><!-- .icon-box -->

  
</div></div>
</div>
<div class="row"  id="row-1126385822">
<div class="col medium-4 small-12 large-4"  data-animate="bounceInRight"><div class="col-inner box-shadow-2" style="padding:10px 10px 10px 10px;" >
  <div class="icon-box testimonial-box icon-box-center text-center is-small">
                <div class="icon-box-img testimonial-image circle" style="width: 135px">
              <img width="150" height="150" src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/6-1-150x150.jpg" class="attachment-thumbnail size-thumbnail" alt="" srcset="http://hptmedia.vn/matong/wp-content/uploads/2019/01/6-1-150x150.jpg 150w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/6-1-510x510.jpg 510w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/6-1-300x300.jpg 300w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/6-1-768x768.jpg 768w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/6-1-100x100.jpg 100w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/6-1.jpg 960w" sizes="(max-width: 150px) 100vw, 150px" />        </div>
                <div class="icon-box-text p-last-0">
          <div class="star-rating"><span style="width:100%"><strong class="rating"></strong></span></div>  				<div class="testimonial-text line-height-small italic test_text first-reset last-reset is-italic">
            
<p>Mình vô cùng ưng ý sp của Shop. Chất rất đẹp, rẻ hơn những nơi khác mình tham khảo. Thời gian giao hàng nhanh, đóng gói cẩn thận, bên trong còn cho túi đẹp đựng áo đi kèm. Cảm ơn Shop rất nhiều</p>
          </div>
          <div class="testimonial-meta pt-half">
             <strong class="testimonial-name test_name">Trần Thảo</strong>
             <span class="testimonial-name-divider"> / </span>             <span class="testimonial-company test_company">Facebook</span>
          </div>
        </div>
  </div><!-- .icon-box -->

  
</div></div>
<div class="col medium-4 small-12 large-4"  data-animate="bounceInRight"><div class="col-inner box-shadow-2" style="padding:10px 10px 10px 10px;" >
  <div class="icon-box testimonial-box icon-box-center text-center is-small">
                <div class="icon-box-img testimonial-image circle" style="width: 135px">
              <img width="150" height="150" src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/4-1-150x150.jpg" class="attachment-thumbnail size-thumbnail" alt="" srcset="http://hptmedia.vn/matong/wp-content/uploads/2019/01/4-1-150x150.jpg 150w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/4-1-510x508.jpg 510w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/4-1-300x300.jpg 300w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/4-1-100x100.jpg 100w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/4-1.jpg 520w" sizes="(max-width: 150px) 100vw, 150px" />        </div>
                <div class="icon-box-text p-last-0">
          <div class="star-rating"><span style="width:100%"><strong class="rating"></strong></span></div>  				<div class="testimonial-text line-height-small italic test_text first-reset last-reset is-italic">
            
<p>Mình vô cùng ưng ý sp của Shop. Chất rất đẹp, rẻ hơn những nơi khác mình tham khảo. Thời gian giao hàng nhanh, đóng gói cẩn thận, bên trong còn cho túi đẹp đựng áo đi kèm. Cảm ơn Shop rất nhiều</p>
          </div>
          <div class="testimonial-meta pt-half">
             <strong class="testimonial-name test_name">Lan Anh</strong>
             <span class="testimonial-name-divider"> / </span>             <span class="testimonial-company test_company">Facebook</span>
          </div>
        </div>
  </div><!-- .icon-box -->

  
</div></div>
<div class="col medium-4 small-12 large-4"  data-animate="bounceInRight"><div class="col-inner box-shadow-2" style="padding:10px 10px 10px 10px;" >
  <div class="icon-box testimonial-box icon-box-center text-center is-small">
                <div class="icon-box-img testimonial-image circle" style="width: 135px">
              <img width="150" height="150" src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/1-1-150x150.jpg" class="attachment-thumbnail size-thumbnail" alt="" srcset="http://hptmedia.vn/matong/wp-content/uploads/2019/01/1-1-150x150.jpg 150w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/1-1-510x510.jpg 510w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/1-1-300x300.jpg 300w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/1-1-100x100.jpg 100w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/1-1.jpg 640w" sizes="(max-width: 150px) 100vw, 150px" />        </div>
                <div class="icon-box-text p-last-0">
          <div class="star-rating"><span style="width:100%"><strong class="rating"></strong></span></div>  				<div class="testimonial-text line-height-small italic test_text first-reset last-reset is-italic">
            
<p>Mình vô cùng ưng ý sp của Shop. Chất rất đẹp, rẻ hơn những nơi khác mình tham khảo. Thời gian giao hàng nhanh, đóng gói cẩn thận, bên trong còn cho túi đẹp đựng áo đi kèm. Cảm ơn Shop rất nhiều</p>
          </div>
          <div class="testimonial-meta pt-half">
             <strong class="testimonial-name test_name">Bảo Ngọc</strong>
                          <span class="testimonial-company test_company"></span>
          </div>
        </div>
  </div><!-- .icon-box -->

  
</div></div>
</div>
     </div>

     <div class="loading-spin dark large centered"></div>

     <style scope="scope">
             </style>
</div><!-- .ux-slider-wrapper -->


<div class="gap-element" style="display:block; height:auto; padding-top:25px" class="clearfix"></div>
<div class="row"  id="row-1145393157">
<div class="col small-12 large-12"  data-animate="flipInY"><div class="col-inner"  >
	<div class="img has-hover x md-x lg-x y md-y lg-y" id="image_791688897">
								<div class="img-inner image-zoom-long dark" >
			<img width="1170" height="209" src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/b1.jpg" class="attachment-original size-original" alt="" srcset="http://hptmedia.vn/matong/wp-content/uploads/2019/01/b1.jpg 1170w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/b1-510x91.jpg 510w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/b1-300x54.jpg 300w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/b1-768x137.jpg 768w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/b1-1024x183.jpg 1024w" sizes="(max-width: 1170px) 100vw, 1170px" />						
					</div>
								
<style scope="scope">

#image_791688897 {
  width: 100%;
}
</style>
	</div>
	
</div></div>
</div>
		
				
</div>



</main><!-- #main -->

<footer id="footer" class="footer-wrapper">

	
<!-- FOOTER 1 -->
<div class="footer-widgets footer footer-1">
		<div class="row large-columns-1 mb-0">
	   		
		<div id="block_widget-2" class="col pb-0 widget block_widget">
				
		<div class="row"  id="row-36561985">

<div class="col medium-8 small-12 large-8"  data-animate="bounceInRight"><div class="col-inner"  >

<div class="container section-title-container" ><h3 class="section-title section-title-bold"><b></b><span class="section-title-main" >Tin tức &amp; Sự kiện</span><b></b></h3></div><!-- .section-title -->


  
    <div class="row large-columns-1 medium-columns-1 small-columns-1 has-shadow row-box-shadow-1">
  		<div class="col post-item" >
			<div class="col-inner">
			<a href="http://hptmedia.vn/matong/mat-ong-bac-ha-dong-van-huong-vi-cao-nguyen-da-4/" class="plain">
				<div class="box box-vertical box-text-bottom box-blog-post has-hover">
            					<div class="box-image" style="width:30%;">
  						<div class="image-cover" style="padding-top:89%;">
  							<img width="300" height="300" src="http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/img/lazy.png" data-src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/9-300x300.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="" srcset="" data-srcset="http://hptmedia.vn/matong/wp-content/uploads/2019/01/9-300x300.jpg 300w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/9-510x510.jpg 510w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/9-150x150.jpg 150w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/9-768x768.jpg 768w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/9-100x100.jpg 100w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/9.jpg 960w" sizes="(max-width: 300px) 100vw, 300px" />  							  							  						</div>
  						  					</div><!-- .box-image -->
          					<div class="box-text text-left is-small" >
					<div class="box-text-inner blog-post-inner">

					
										<h5 class="post-title is-large ">TIÊU CHÍ NÀO ĐỂ ĐÁNH GIÁ MẬT ONG CHẤT LƯỢNG HAY KHÔNG?</h5>
					<div class="post-meta is-small op-8">28 Tháng Một, 2019</div>					<div class="is-divider"></div>
										<p class="from_the_blog_excerpt ">LÀM THẾ NÀO ĐỂ BIẾT MẬT ONG BỊ NHIỄM ĐƯỜNG MÍA? HAY MẬT ONG ĐƯỢC THU HOẠCH TỪ VIỆC CHO ONG ĂN ĐƯỜNG? LÀM SAO ĐỂ MUA ĐƯỢC MẬT ĐẠT TIÊU CHUẨN VÀ TỰ NHIÊN NHƯ MẬT ONG RỪNG? Việc [...]					</p>
					                    
					
					
					</div><!-- .box-text-inner -->
					</div><!-- .box-text -->
									</div><!-- .box -->
				</a><!-- .link -->
			</div><!-- .col-inner -->
		</div><!-- .col -->
		<div class="col post-item" >
			<div class="col-inner">
			<a href="http://hptmedia.vn/matong/mat-ong-bac-ha-dong-van-huong-vi-cao-nguyen-da-3/" class="plain">
				<div class="box box-vertical box-text-bottom box-blog-post has-hover">
            					<div class="box-image" style="width:30%;">
  						<div class="image-cover" style="padding-top:89%;">
  							<img width="300" height="212" src="http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/img/lazy.png" data-src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/Post_3-300x212.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="" srcset="" data-srcset="http://hptmedia.vn/matong/wp-content/uploads/2019/01/Post_3-300x212.jpg 300w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/Post_3-768x544.jpg 768w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/Post_3-1024x725.jpg 1024w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/Post_3-510x361.jpg 510w" sizes="(max-width: 300px) 100vw, 300px" />  							  							  						</div>
  						  					</div><!-- .box-image -->
          					<div class="box-text text-left is-small" >
					<div class="box-text-inner blog-post-inner">

					
										<h5 class="post-title is-large ">KEO ONG TRACYBEE &#8211; GIẢI PHÁP THAY THẾ KHÁNG SINH HỖ TRỢ ĐIỀU TRỊ CÁC BỆNH THƯỜNG GẶP</h5>
					<div class="post-meta is-small op-8">28 Tháng Một, 2019</div>					<div class="is-divider"></div>
										<p class="from_the_blog_excerpt ">Giấc mơ nuôi con không lạm dụng kháng sinh của các bà mẹ nay đã không còn xa vời. Với keo ong xanh Tracybee nỗi lo lắng con trẻ sử dụng thuốc kháng sinh làm giảm sức đề kháng và [...]					</p>
					                    
					
					
					</div><!-- .box-text-inner -->
					</div><!-- .box-text -->
									</div><!-- .box -->
				</a><!-- .link -->
			</div><!-- .col-inner -->
		</div><!-- .col -->
		<div class="col post-item" >
			<div class="col-inner">
			<a href="http://hptmedia.vn/matong/mat-ong-bac-ha-dong-van-huong-vi-cao-nguyen-da-2/" class="plain">
				<div class="box box-vertical box-text-bottom box-blog-post has-hover">
            					<div class="box-image" style="width:30%;">
  						<div class="image-cover" style="padding-top:89%;">
  							<img width="260" height="300" src="http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/img/lazy.png" data-src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/1-260x300.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="" srcset="" data-srcset="http://hptmedia.vn/matong/wp-content/uploads/2019/01/1-260x300.jpg 260w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/1-510x589.jpg 510w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/1.jpg 640w" sizes="(max-width: 260px) 100vw, 260px" />  							  							  						</div>
  						  					</div><!-- .box-image -->
          					<div class="box-text text-left is-small" >
					<div class="box-text-inner blog-post-inner">

					
										<h5 class="post-title is-large ">TIÊU CHÍ NÀO ĐỂ ĐÁNH GIÁ MẬT ONG CHẤT LƯỢNG HAY KHÔNG?</h5>
					<div class="post-meta is-small op-8">28 Tháng Một, 2019</div>					<div class="is-divider"></div>
										<p class="from_the_blog_excerpt ">LÀM THẾ NÀO ĐỂ BIẾT MẬT ONG BỊ NHIỄM ĐƯỜNG MÍA? HAY MẬT ONG ĐƯỢC THU HOẠCH TỪ VIỆC CHO ONG ĂN ĐƯỜNG? LÀM SAO ĐỂ MUA ĐƯỢC MẬT ĐẠT TIÊU CHUẨN VÀ TỰ NHIÊN NHƯ MẬT ONG RỪNG? Việc [...]					</p>
					                    
					
					
					</div><!-- .box-text-inner -->
					</div><!-- .box-text -->
									</div><!-- .box -->
				</a><!-- .link -->
			</div><!-- .col-inner -->
		</div><!-- .col -->
		<div class="col post-item" >
			<div class="col-inner">
			<a href="http://hptmedia.vn/matong/mat-ong-bac-ha-dong-van-huong-vi-cao-nguyen-da/" class="plain">
				<div class="box box-vertical box-text-bottom box-blog-post has-hover">
            					<div class="box-image" style="width:30%;">
  						<div class="image-cover" style="padding-top:89%;">
  							<img width="300" height="196" src="http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/img/lazy.png" data-src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/Post_2-300x196.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="" srcset="" data-srcset="http://hptmedia.vn/matong/wp-content/uploads/2019/01/Post_2-300x196.jpg 300w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/Post_2-768x503.jpg 768w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/Post_2-510x334.jpg 510w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/Post_2.jpg 933w" sizes="(max-width: 300px) 100vw, 300px" />  							  							  						</div>
  						  					</div><!-- .box-image -->
          					<div class="box-text text-left is-small" >
					<div class="box-text-inner blog-post-inner">

					
										<h5 class="post-title is-large ">TĂNG CÂN AN TOÀN VỚI MẬT ONG? BÍ QUYẾT THỨ 3 CỰC KÌ ĐƠN GIẢN</h5>
					<div class="post-meta is-small op-8">28 Tháng Một, 2019</div>					<div class="is-divider"></div>
										<p class="from_the_blog_excerpt ">Trong khi nhà nhà người người đều muốn giảm cân thì vẫn còn rất nhiều chị em “buồn tí cũng giảm cân” mong muốn tăng cân để thoát khỏi nỗi ám ảnh cò hương. TRACYBEE sẽ mách bạn một số [...]					</p>
					                    
					
					
					</div><!-- .box-text-inner -->
					</div><!-- .box-text -->
									</div><!-- .box -->
				</a><!-- .link -->
			</div><!-- .col-inner -->
		</div><!-- .col -->
		<div class="col post-item" >
			<div class="col-inner">
			<a href="http://hptmedia.vn/matong/chao-moi-nguoi/" class="plain">
				<div class="box box-vertical box-text-bottom box-blog-post has-hover">
            					<div class="box-image" style="width:30%;">
  						<div class="image-cover" style="padding-top:89%;">
  							<img width="300" height="199" src="http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/img/lazy.png" data-src="http://hptmedia.vn/matong/wp-content/uploads/2019/01/Post_1-300x199.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="" srcset="" data-srcset="http://hptmedia.vn/matong/wp-content/uploads/2019/01/Post_1-300x199.jpg 300w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/Post_1-768x510.jpg 768w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/Post_1-1024x680.jpg 1024w, http://hptmedia.vn/matong/wp-content/uploads/2019/01/Post_1-510x338.jpg 510w" sizes="(max-width: 300px) 100vw, 300px" />  							  							  						</div>
  						  					</div><!-- .box-image -->
          					<div class="box-text text-left is-small" >
					<div class="box-text-inner blog-post-inner">

					
										<h5 class="post-title is-large ">MẬT ONG NGUYÊN CHẤT 100% CÓ ĐƯỜNG KHÔNG?</h5>
					<div class="post-meta is-small op-8">28 Tháng Một, 2019</div>					<div class="is-divider"></div>
										<p class="from_the_blog_excerpt ">Ó Hai thành phần chính trong mật ong là Glucose và Fructose kết hợp với 70 loại khoáng chất khác nhau. Nhưng đừng vội tẩy chay “thành phần đường” này. Đây là đường tự nhiên trong mật ong nguyên chất [...]					</p>
					                                            <p class="from_the_blog_comments uppercase is-xsmall">
                            1 Comment                        </p>
                    
					
					
					</div><!-- .box-text-inner -->
					</div><!-- .box-text -->
									</div><!-- .box -->
				</a><!-- .link -->
			</div><!-- .col-inner -->
		</div><!-- .col -->
</div>


</div></div>
<div class="col medium-4 small-12 large-4"  data-animate="bounceInLeft"><div class="col-inner"  >

<ul class="sidebar-wrapper ul-reset "><aside id="custom_html-2" class="widget_text widget widget_custom_html"><div class="textwidget custom-html-widget"><div role="form" class="wpcf7" id="wpcf7-f124-o1" lang="vi" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/matong/#wpcf7-f124-o1" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="124" />
<input type="hidden" name="_wpcf7_version" value="5.1.4" />
<input type="hidden" name="_wpcf7_locale" value="vi" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f124-o1" />
<input type="hidden" name="_wpcf7_container_post" value="0" />
</div>
<h3>Tư vấn mua hàng</h3>
<p><span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Họ và tên*" /></span><br />
<span class="wpcf7-form-control-wrap your-tel"><input type="tel" name="your-tel" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Số điện thoại*" /></span><br />
<span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email*" /></span><br />
<span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Nội dung*"></textarea></span></p>
<p><input type="submit" value="Đăng ký" class="wpcf7-form-control wpcf7-submit" /></p>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div></div></aside></ul>


</div></div>

</div>
		</div>		
		        
		</div><!-- end row -->
</div><!-- footer 1 -->


<!-- FOOTER 2 -->
<div class="footer-widgets footer footer-2 ">
		<div class="row large-columns-1 mb-0">
	   		
		<div id="block_widget-3" class="col pb-0 widget block_widget">
				
		<div class="row row-small"  id="row-83669554">

<div class="col hide-for-medium medium-3 small-6 large-3"  data-animate="bounceInLeft"><div class="col-inner"  >

<ul class="sidebar-wrapper ul-reset "><li id="nav_menu-2" class="widget widget_nav_menu"><h2 class="widgettitle">Về chúng tôi</h2>
<div class="menu-ve-chung-toi-container"><ul id="menu-ve-chung-toi" class="menu"><li id="menu-item-136" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-136"><a href="https://#">Giới thiệu về Sweet Bee</a></li>
<li id="menu-item-137" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-137"><a href="https://#">Quy chế hoạt động Sàn TMĐT</a></li>
<li id="menu-item-138" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-138"><a href="https://#">Điều khoản và điều kiện giao dịch</a></li>
<li id="menu-item-139" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-139"><a href="https://#">Thông báo từ Sweet Bee</a></li>
<li id="menu-item-140" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-140"><a href="https://#">Tra cứu hóa đơn</a></li>
<li id="menu-item-141" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-141"><a href="https://#">Liên hệ</a></li>
</ul></div></li></ul>


</div></div>
<div class="col hide-for-medium medium-3 small-6 large-3"  data-animate="bounceInLeft"><div class="col-inner"  >

<ul class="sidebar-wrapper ul-reset "><li id="nav_menu-3" class="widget widget_nav_menu"><h2 class="widgettitle">Chính sách bán hàng</h2>
<div class="menu-chinh-sach-ban-hang-container"><ul id="menu-chinh-sach-ban-hang" class="menu"><li id="menu-item-142" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-142"><a href="https://#">Trung tâm hỗ trợ khách hàng</a></li>
<li id="menu-item-143" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-143"><a href="https://#">Chính sách bảo mật thanh toán</a></li>
<li id="menu-item-144" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-144"><a href="https://#">Chính sách thanh toán</a></li>
<li id="menu-item-145" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-145"><a href="https://#">Chính sách giao hàng</a></li>
<li id="menu-item-146" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-146"><a href="https://#">Chính sách đổi trả</a></li>
<li id="menu-item-147" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-147"><a href="https://#">Hướng dẫn gửi trả hàng</a></li>
</ul></div></li></ul>


</div></div>
<div class="col hide-for-medium medium-3 small-6 large-3"  data-animate="bounceInLeft"><div class="col-inner"  >

<ul class="sidebar-wrapper ul-reset "><li id="nav_menu-4" class="widget widget_nav_menu"><h2 class="widgettitle">Hỗ trợ khách hàng</h2>
<div class="menu-ho-tro-khach-hang-container"><ul id="menu-ho-tro-khach-hang" class="menu"><li id="menu-item-148" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-148"><a href="https://#">Giới thiệu chung</a></li>
<li id="menu-item-149" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-149"><a href="https://#">Chính sách bảo hành</a></li>
<li id="menu-item-150" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-150"><a href="https://#">Chính sách vận chuyển</a></li>
<li id="menu-item-151" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-151"><a href="https://#">Chính sách đổi, trả hàng</a></li>
<li id="menu-item-152" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-152"><a href="https://#">Quy định và hình thức thanh toán</a></li>
<li id="menu-item-153" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-153"><a href="https://#">Chính sách bảo mật thông tin</a></li>
</ul></div></li></ul>


</div></div>
<div class="col hide-for-medium medium-3 small-6 large-3"  data-animate="bounceInLeft"><div class="col-inner"  >

<ul class="sidebar-wrapper ul-reset "><li id="media_image-2" class="widget widget_media_image"><h2 class="widgettitle">Bản đồ</h2>
<img width="335" height="172" src="http://hptmedia.vn/matong/wp-content/uploads/2019/02/bd.jpg" class="image wp-image-161  attachment-full size-full" alt="" style="max-width: 100%; height: auto;" srcset="http://hptmedia.vn/matong/wp-content/uploads/2019/02/bd.jpg 335w, http://hptmedia.vn/matong/wp-content/uploads/2019/02/bd-300x154.jpg 300w" sizes="(max-width: 335px) 100vw, 335px" /></li></ul>


</div></div>


<style scope="scope">

</style>
</div>
<div class="row row-footer"  id="row-152136445">

<div class="col small-12 large-12"  data-animate="flipInY"><div class="col-inner" style="margin:0px 0px -15px 0px;" >

<ul class="sidebar-wrapper ul-reset "><li id="nav_menu-5" class="widget widget_nav_menu"><div class="menu-menu-top-container"><ul id="menu-menu-top-1" class="menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-11 current_page_item menu-item-126"><a href="http://hptmedia.vn/matong/" aria-current="page">Trang chủ</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-127"><a href="http://hptmedia.vn/matong/gioi-thieu/">Giới thiệu</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-129"><a href="http://hptmedia.vn/matong/danh-muc/san-pham/">Sản phẩm</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-128"><a href="http://hptmedia.vn/matong/tin-tuc/">Tin tức</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-243"><a href="http://hptmedia.vn/matong/lien-he/">Liên hệ</a></li>
</ul></div></li></ul>

<ul class="sidebar-wrapper ul-reset "><li id="text-2" class="widget widget_text">			<div class="textwidget"><p><center style="font-size: 22px;">SPA AND BEAUTY</center><center>Địa chỉ: 81 ngõ 129 Trương Định,Hai Bà Trưng,Hà Nội<br />
Điện thoại: 091 1234 567 / E-mail: HPT@gmail.com</center></p>
</div>
		</li></ul>


</div></div>


<style scope="scope">

</style>
</div>
		</div>		
		        
		</div><!-- end row -->
</div><!-- end footer 2 -->



<div class="absolute-footer dark medium-text-center small-text-center">
  <div class="container clearfix">

          <div class="footer-secondary pull-right">
                <div class="payment-icons inline-block"><div class="payment-icon"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  viewBox="0 0 64 32">
<path d="M10.781 7.688c-0.251-1.283-1.219-1.688-2.344-1.688h-8.376l-0.061 0.405c5.749 1.469 10.469 4.595 12.595 10.501l-1.813-9.219zM13.125 19.688l-0.531-2.781c-1.096-2.907-3.752-5.594-6.752-6.813l4.219 15.939h5.469l8.157-20.032h-5.501l-5.062 13.688zM27.72 26.061l3.248-20.061h-5.187l-3.251 20.061h5.189zM41.875 5.656c-5.125 0-8.717 2.72-8.749 6.624-0.032 2.877 2.563 4.469 4.531 5.439 2.032 0.968 2.688 1.624 2.688 2.499 0 1.344-1.624 1.939-3.093 1.939-2.093 0-3.219-0.251-4.875-1.032l-0.688-0.344-0.719 4.499c1.219 0.563 3.437 1.064 5.781 1.064 5.437 0.032 8.97-2.688 9.032-6.843 0-2.282-1.405-4-4.376-5.439-1.811-0.904-2.904-1.563-2.904-2.499 0-0.843 0.936-1.72 2.968-1.72 1.688-0.029 2.936 0.314 3.875 0.752l0.469 0.248 0.717-4.344c-1.032-0.406-2.656-0.844-4.656-0.844zM55.813 6c-1.251 0-2.189 0.376-2.72 1.688l-7.688 18.374h5.437c0.877-2.467 1.096-3 1.096-3 0.592 0 5.875 0 6.624 0 0 0 0.157 0.688 0.624 3h4.813l-4.187-20.061h-4zM53.405 18.938c0 0 0.437-1.157 2.064-5.594-0.032 0.032 0.437-1.157 0.688-1.907l0.374 1.72c0.968 4.781 1.189 5.781 1.189 5.781-0.813 0-3.283 0-4.315 0z"></path>
</svg>
</div><div class="payment-icon"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  viewBox="0 0 64 32">
<path d="M35.255 12.078h-2.396c-0.229 0-0.444 0.114-0.572 0.303l-3.306 4.868-1.4-4.678c-0.088-0.292-0.358-0.493-0.663-0.493h-2.355c-0.284 0-0.485 0.28-0.393 0.548l2.638 7.745-2.481 3.501c-0.195 0.275 0.002 0.655 0.339 0.655h2.394c0.227 0 0.439-0.111 0.569-0.297l7.968-11.501c0.191-0.275-0.006-0.652-0.341-0.652zM19.237 16.718c-0.23 1.362-1.311 2.276-2.691 2.276-0.691 0-1.245-0.223-1.601-0.644-0.353-0.417-0.485-1.012-0.374-1.674 0.214-1.35 1.313-2.294 2.671-2.294 0.677 0 1.227 0.225 1.589 0.65 0.365 0.428 0.509 1.027 0.404 1.686zM22.559 12.078h-2.384c-0.204 0-0.378 0.148-0.41 0.351l-0.104 0.666-0.166-0.241c-0.517-0.749-1.667-1-2.817-1-2.634 0-4.883 1.996-5.321 4.796-0.228 1.396 0.095 2.731 0.888 3.662 0.727 0.856 1.765 1.212 3.002 1.212 2.123 0 3.3-1.363 3.3-1.363l-0.106 0.662c-0.040 0.252 0.155 0.479 0.41 0.479h2.147c0.341 0 0.63-0.247 0.684-0.584l1.289-8.161c0.040-0.251-0.155-0.479-0.41-0.479zM8.254 12.135c-0.272 1.787-1.636 1.787-2.957 1.787h-0.751l0.527-3.336c0.031-0.202 0.205-0.35 0.41-0.35h0.345c0.899 0 1.747 0 2.185 0.511 0.262 0.307 0.341 0.761 0.242 1.388zM7.68 7.473h-4.979c-0.341 0-0.63 0.248-0.684 0.584l-2.013 12.765c-0.040 0.252 0.155 0.479 0.41 0.479h2.378c0.34 0 0.63-0.248 0.683-0.584l0.543-3.444c0.053-0.337 0.343-0.584 0.683-0.584h1.575c3.279 0 5.172-1.587 5.666-4.732 0.223-1.375 0.009-2.456-0.635-3.212-0.707-0.832-1.962-1.272-3.628-1.272zM60.876 7.823l-2.043 12.998c-0.040 0.252 0.155 0.479 0.41 0.479h2.055c0.34 0 0.63-0.248 0.683-0.584l2.015-12.765c0.040-0.252-0.155-0.479-0.41-0.479h-2.299c-0.205 0.001-0.379 0.148-0.41 0.351zM54.744 16.718c-0.23 1.362-1.311 2.276-2.691 2.276-0.691 0-1.245-0.223-1.601-0.644-0.353-0.417-0.485-1.012-0.374-1.674 0.214-1.35 1.313-2.294 2.671-2.294 0.677 0 1.227 0.225 1.589 0.65 0.365 0.428 0.509 1.027 0.404 1.686zM58.066 12.078h-2.384c-0.204 0-0.378 0.148-0.41 0.351l-0.104 0.666-0.167-0.241c-0.516-0.749-1.667-1-2.816-1-2.634 0-4.883 1.996-5.321 4.796-0.228 1.396 0.095 2.731 0.888 3.662 0.727 0.856 1.765 1.212 3.002 1.212 2.123 0 3.3-1.363 3.3-1.363l-0.106 0.662c-0.040 0.252 0.155 0.479 0.41 0.479h2.147c0.341 0 0.63-0.247 0.684-0.584l1.289-8.161c0.040-0.252-0.156-0.479-0.41-0.479zM43.761 12.135c-0.272 1.787-1.636 1.787-2.957 1.787h-0.751l0.527-3.336c0.031-0.202 0.205-0.35 0.41-0.35h0.345c0.899 0 1.747 0 2.185 0.511 0.261 0.307 0.34 0.761 0.241 1.388zM43.187 7.473h-4.979c-0.341 0-0.63 0.248-0.684 0.584l-2.013 12.765c-0.040 0.252 0.156 0.479 0.41 0.479h2.554c0.238 0 0.441-0.173 0.478-0.408l0.572-3.619c0.053-0.337 0.343-0.584 0.683-0.584h1.575c3.279 0 5.172-1.587 5.666-4.732 0.223-1.375 0.009-2.456-0.635-3.212-0.707-0.832-1.962-1.272-3.627-1.272z"></path>
</svg>
</div><div class="payment-icon"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  viewBox="0 0 64 32">
<path d="M7.114 14.656c-1.375-0.5-2.125-0.906-2.125-1.531 0-0.531 0.437-0.812 1.188-0.812 1.437 0 2.875 0.531 3.875 1.031l0.563-3.5c-0.781-0.375-2.406-1-4.656-1-1.594 0-2.906 0.406-3.844 1.188-1 0.812-1.5 2-1.5 3.406 0 2.563 1.563 3.688 4.125 4.594 1.625 0.594 2.188 1 2.188 1.656 0 0.625-0.531 0.969-1.5 0.969-1.188 0-3.156-0.594-4.437-1.343l-0.563 3.531c1.094 0.625 3.125 1.281 5.25 1.281 1.688 0 3.063-0.406 4.031-1.157 1.063-0.843 1.594-2.062 1.594-3.656-0.001-2.625-1.595-3.719-4.188-4.657zM21.114 9.125h-3v-4.219l-4.031 0.656-0.563 3.563-1.437 0.25-0.531 3.219h1.937v6.844c0 1.781 0.469 3 1.375 3.75 0.781 0.625 1.907 0.938 3.469 0.938 1.219 0 1.937-0.219 2.468-0.344v-3.688c-0.282 0.063-0.938 0.22-1.375 0.22-0.906 0-1.313-0.5-1.313-1.563v-6.156h2.406l0.595-3.469zM30.396 9.031c-0.313-0.062-0.594-0.093-0.876-0.093-1.312 0-2.374 0.687-2.781 1.937l-0.313-1.75h-4.093v14.719h4.687v-9.563c0.594-0.719 1.437-0.968 2.563-0.968 0.25 0 0.5 0 0.812 0.062v-4.344zM33.895 2.719c-1.375 0-2.468 1.094-2.468 2.469s1.094 2.5 2.468 2.5 2.469-1.124 2.469-2.5-1.094-2.469-2.469-2.469zM36.239 23.844v-14.719h-4.687v14.719h4.687zM49.583 10.468c-0.843-1.094-2-1.625-3.469-1.625-1.343 0-2.531 0.563-3.656 1.75l-0.25-1.469h-4.125v20.155l4.688-0.781v-4.719c0.719 0.219 1.469 0.344 2.125 0.344 1.157 0 2.876-0.313 4.188-1.75 1.281-1.375 1.907-3.5 1.907-6.313 0-2.499-0.469-4.405-1.407-5.593zM45.677 19.532c-0.375 0.687-0.969 1.094-1.625 1.094-0.468 0-0.906-0.093-1.281-0.281v-7c0.812-0.844 1.531-0.938 1.781-0.938 1.188 0 1.781 1.313 1.781 3.812 0.001 1.437-0.219 2.531-0.656 3.313zM62.927 10.843c-1.032-1.312-2.563-2-4.501-2-4 0-6.468 2.938-6.468 7.688 0 2.625 0.656 4.625 1.968 5.875 1.157 1.157 2.844 1.719 5.032 1.719 2 0 3.844-0.469 5-1.251l-0.501-3.219c-1.157 0.625-2.5 0.969-4 0.969-0.906 0-1.532-0.188-1.969-0.594-0.5-0.406-0.781-1.094-0.875-2.062h7.75c0.031-0.219 0.062-1.281 0.062-1.625 0.001-2.344-0.5-4.188-1.499-5.5zM56.583 15.094c0.125-2.093 0.687-3.062 1.75-3.062s1.625 1 1.687 3.062h-3.437z"></path>
</svg>
</div><div class="payment-icon"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  viewBox="0 0 64 32">
<path d="M42.667-0c-4.099 0-7.836 1.543-10.667 4.077-2.831-2.534-6.568-4.077-10.667-4.077-8.836 0-16 7.163-16 16s7.164 16 16 16c4.099 0 7.835-1.543 10.667-4.077 2.831 2.534 6.568 4.077 10.667 4.077 8.837 0 16-7.163 16-16s-7.163-16-16-16zM11.934 19.828l0.924-5.809-2.112 5.809h-1.188v-5.809l-1.056 5.809h-1.584l1.32-7.657h2.376v4.753l1.716-4.753h2.508l-1.32 7.657h-1.585zM19.327 18.244c-0.088 0.528-0.178 0.924-0.264 1.188v0.396h-1.32v-0.66c-0.353 0.528-0.924 0.792-1.716 0.792-0.442 0-0.792-0.132-1.056-0.396-0.264-0.351-0.396-0.792-0.396-1.32 0-0.792 0.218-1.364 0.66-1.716 0.614-0.44 1.364-0.66 2.244-0.66h0.66v-0.396c0-0.351-0.353-0.528-1.056-0.528-0.442 0-1.012 0.088-1.716 0.264 0.086-0.351 0.175-0.792 0.264-1.32 0.703-0.264 1.32-0.396 1.848-0.396 1.496 0 2.244 0.616 2.244 1.848 0 0.353-0.046 0.749-0.132 1.188-0.089 0.616-0.179 1.188-0.264 1.716zM24.079 15.076c-0.264-0.086-0.66-0.132-1.188-0.132s-0.792 0.177-0.792 0.528c0 0.177 0.044 0.31 0.132 0.396l0.528 0.264c0.792 0.442 1.188 1.012 1.188 1.716 0 1.409-0.838 2.112-2.508 2.112-0.792 0-1.366-0.044-1.716-0.132 0.086-0.351 0.175-0.836 0.264-1.452 0.703 0.177 1.188 0.264 1.452 0.264 0.614 0 0.924-0.175 0.924-0.528 0-0.175-0.046-0.308-0.132-0.396-0.178-0.175-0.396-0.308-0.66-0.396-0.792-0.351-1.188-0.924-1.188-1.716 0-1.407 0.792-2.112 2.376-2.112 0.792 0 1.32 0.045 1.584 0.132l-0.265 1.451zM27.512 15.208h-0.924c0 0.442-0.046 0.838-0.132 1.188 0 0.088-0.022 0.264-0.066 0.528-0.046 0.264-0.112 0.442-0.198 0.528v0.528c0 0.353 0.175 0.528 0.528 0.528 0.175 0 0.35-0.044 0.528-0.132l-0.264 1.452c-0.264 0.088-0.66 0.132-1.188 0.132-0.881 0-1.32-0.44-1.32-1.32 0-0.528 0.086-1.099 0.264-1.716l0.66-4.225h1.584l-0.132 0.924h0.792l-0.132 1.585zM32.66 17.32h-3.3c0 0.442 0.086 0.749 0.264 0.924 0.264 0.264 0.66 0.396 1.188 0.396s1.1-0.175 1.716-0.528l-0.264 1.584c-0.442 0.177-1.012 0.264-1.716 0.264-1.848 0-2.772-0.924-2.772-2.773 0-1.142 0.264-2.024 0.792-2.64 0.528-0.703 1.188-1.056 1.98-1.056 0.703 0 1.274 0.22 1.716 0.66 0.35 0.353 0.528 0.881 0.528 1.584 0.001 0.617-0.046 1.145-0.132 1.585zM35.3 16.132c-0.264 0.97-0.484 2.201-0.66 3.697h-1.716l0.132-0.396c0.35-2.463 0.614-4.4 0.792-5.809h1.584l-0.132 0.924c0.264-0.44 0.528-0.703 0.792-0.792 0.264-0.264 0.528-0.308 0.792-0.132-0.088 0.088-0.31 0.706-0.66 1.848-0.353-0.086-0.661 0.132-0.925 0.66zM41.241 19.697c-0.353 0.177-0.838 0.264-1.452 0.264-0.881 0-1.584-0.308-2.112-0.924-0.528-0.528-0.792-1.32-0.792-2.376 0-1.32 0.35-2.42 1.056-3.3 0.614-0.879 1.496-1.32 2.64-1.32 0.44 0 1.056 0.132 1.848 0.396l-0.264 1.584c-0.528-0.264-1.012-0.396-1.452-0.396-0.707 0-1.235 0.264-1.584 0.792-0.353 0.442-0.528 1.144-0.528 2.112 0 0.616 0.132 1.056 0.396 1.32 0.264 0.353 0.614 0.528 1.056 0.528 0.44 0 0.924-0.132 1.452-0.396l-0.264 1.717zM47.115 15.868c-0.046 0.264-0.066 0.484-0.066 0.66-0.088 0.442-0.178 1.035-0.264 1.782-0.088 0.749-0.178 1.254-0.264 1.518h-1.32v-0.66c-0.353 0.528-0.924 0.792-1.716 0.792-0.442 0-0.792-0.132-1.056-0.396-0.264-0.351-0.396-0.792-0.396-1.32 0-0.792 0.218-1.364 0.66-1.716 0.614-0.44 1.32-0.66 2.112-0.66h0.66c0.086-0.086 0.132-0.218 0.132-0.396 0-0.351-0.353-0.528-1.056-0.528-0.442 0-1.012 0.088-1.716 0.264 0-0.351 0.086-0.792 0.264-1.32 0.703-0.264 1.32-0.396 1.848-0.396 1.496 0 2.245 0.616 2.245 1.848 0.001 0.089-0.021 0.264-0.065 0.529zM49.69 16.132c-0.178 0.528-0.396 1.762-0.66 3.697h-1.716l0.132-0.396c0.35-1.935 0.614-3.872 0.792-5.809h1.584c0 0.353-0.046 0.66-0.132 0.924 0.264-0.44 0.528-0.703 0.792-0.792 0.35-0.175 0.614-0.218 0.792-0.132-0.353 0.442-0.574 1.056-0.66 1.848-0.353-0.086-0.66 0.132-0.925 0.66zM54.178 19.828l0.132-0.528c-0.353 0.442-0.838 0.66-1.452 0.66-0.707 0-1.188-0.218-1.452-0.66-0.442-0.614-0.66-1.232-0.66-1.848 0-1.142 0.308-2.067 0.924-2.773 0.44-0.703 1.056-1.056 1.848-1.056 0.528 0 1.056 0.264 1.584 0.792l0.264-2.244h1.716l-1.32 7.657h-1.585zM16.159 17.98c0 0.442 0.175 0.66 0.528 0.66 0.35 0 0.614-0.132 0.792-0.396 0.264-0.264 0.396-0.66 0.396-1.188h-0.397c-0.881 0-1.32 0.31-1.32 0.924zM31.076 15.076c-0.088 0-0.178-0.043-0.264-0.132h-0.264c-0.528 0-0.881 0.353-1.056 1.056h1.848v-0.396l-0.132-0.264c-0.001-0.086-0.047-0.175-0.133-0.264zM43.617 17.98c0 0.442 0.175 0.66 0.528 0.66 0.35 0 0.614-0.132 0.792-0.396 0.264-0.264 0.396-0.66 0.396-1.188h-0.396c-0.881 0-1.32 0.31-1.32 0.924zM53.782 15.076c-0.353 0-0.66 0.22-0.924 0.66-0.178 0.264-0.264 0.749-0.264 1.452 0 0.792 0.264 1.188 0.792 1.188 0.35 0 0.66-0.175 0.924-0.528 0.264-0.351 0.396-0.879 0.396-1.584-0.001-0.792-0.311-1.188-0.925-1.188z"></path>
</svg>
</div><div class="payment-icon"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  viewBox="0 0 64 32">
<path d="M13.043 8.356c-0.46 0-0.873 0.138-1.24 0.413s-0.662 0.681-0.885 1.217c-0.223 0.536-0.334 1.112-0.334 1.727 0 0.568 0.119 0.99 0.358 1.265s0.619 0.413 1.141 0.413c0.508 0 1.096-0.131 1.765-0.393v1.327c-0.693 0.262-1.389 0.393-2.089 0.393-0.884 0-1.572-0.254-2.063-0.763s-0.736-1.229-0.736-2.161c0-0.892 0.181-1.712 0.543-2.462s0.846-1.32 1.452-1.709 1.302-0.584 2.089-0.584c0.435 0 0.822 0.038 1.159 0.115s0.7 0.217 1.086 0.421l-0.616 1.276c-0.369-0.201-0.673-0.333-0.914-0.398s-0.478-0.097-0.715-0.097zM19.524 12.842h-2.47l-0.898 1.776h-1.671l3.999-7.491h1.948l0.767 7.491h-1.551l-0.125-1.776zM19.446 11.515l-0.136-1.786c-0.035-0.445-0.052-0.876-0.052-1.291v-0.184c-0.153 0.408-0.343 0.84-0.569 1.296l-0.982 1.965h1.739zM27.049 12.413c0 0.711-0.257 1.273-0.773 1.686s-1.213 0.62-2.094 0.62c-0.769 0-1.389-0.153-1.859-0.46v-1.398c0.672 0.367 1.295 0.551 1.869 0.551 0.39 0 0.694-0.072 0.914-0.217s0.329-0.343 0.329-0.595c0-0.147-0.024-0.275-0.070-0.385s-0.114-0.214-0.201-0.309c-0.087-0.095-0.303-0.269-0.648-0.52-0.481-0.337-0.818-0.67-1.013-1s-0.293-0.685-0.293-1.066c0-0.439 0.108-0.831 0.324-1.176s0.523-0.614 0.922-0.806 0.857-0.288 1.376-0.288c0.755 0 1.446 0.168 2.073 0.505l-0.569 1.189c-0.543-0.252-1.044-0.378-1.504-0.378-0.289 0-0.525 0.077-0.71 0.23s-0.276 0.355-0.276 0.607c0 0.207 0.058 0.389 0.172 0.543s0.372 0.36 0.773 0.615c0.421 0.272 0.736 0.572 0.945 0.9s0.313 0.712 0.313 1.151zM33.969 14.618h-1.597l0.7-3.22h-2.46l-0.7 3.22h-1.592l1.613-7.46h1.597l-0.632 2.924h2.459l0.632-2.924h1.592l-1.613 7.46zM46.319 9.831c0 0.963-0.172 1.824-0.517 2.585s-0.816 1.334-1.415 1.722c-0.598 0.388-1.288 0.582-2.067 0.582-0.891 0-1.587-0.251-2.086-0.753s-0.749-1.198-0.749-2.090c0-0.902 0.172-1.731 0.517-2.488s0.82-1.338 1.425-1.743c0.605-0.405 1.306-0.607 2.099-0.607 0.888 0 1.575 0.245 2.063 0.735s0.73 1.176 0.73 2.056zM43.395 8.356c-0.421 0-0.808 0.155-1.159 0.467s-0.627 0.739-0.828 1.283-0.3 1.135-0.3 1.771c0 0.5 0.116 0.877 0.348 1.133s0.558 0.383 0.979 0.383 0.805-0.148 1.151-0.444c0.346-0.296 0.617-0.714 0.812-1.255s0.292-1.148 0.292-1.822c0-0.483-0.113-0.856-0.339-1.12-0.227-0.264-0.546-0.396-0.957-0.396zM53.427 14.618h-1.786l-1.859-5.644h-0.031l-0.021 0.163c-0.111 0.735-0.227 1.391-0.344 1.97l-0.757 3.511h-1.436l1.613-7.46h1.864l1.775 5.496h0.021c0.042-0.259 0.109-0.628 0.203-1.107s0.407-1.942 0.94-4.388h1.43l-1.613 7.461zM13.296 20.185c0 0.98-0.177 1.832-0.532 2.556s-0.868 1.274-1.539 1.652c-0.672 0.379-1.464 0.568-2.376 0.568h-2.449l1.678-7.68h2.15c0.977 0 1.733 0.25 2.267 0.751s0.801 1.219 0.801 2.154zM8.925 23.615c0.536 0 1.003-0.133 1.401-0.399s0.71-0.657 0.934-1.174c0.225-0.517 0.337-1.108 0.337-1.773 0-0.54-0.131-0.95-0.394-1.232s-0.64-0.423-1.132-0.423h-0.624l-1.097 5.001h0.575zM18.64 24.96h-4.436l1.678-7.68h4.442l-0.293 1.334h-2.78l-0.364 1.686h2.59l-0.299 1.334h-2.59l-0.435 1.98h2.78l-0.293 1.345zM20.509 24.96l1.678-7.68h1.661l-1.39 6.335h2.78l-0.294 1.345h-4.436zM26.547 24.96l1.694-7.68h1.656l-1.694 7.68h-1.656zM33.021 23.389c0.282-0.774 0.481-1.27 0.597-1.487l2.346-4.623h1.716l-4.061 7.68h-1.814l-0.689-7.68h1.602l0.277 4.623c0.015 0.157 0.022 0.39 0.022 0.699-0.007 0.361-0.018 0.623-0.033 0.788h0.038zM41.678 24.96h-4.437l1.678-7.68h4.442l-0.293 1.334h-2.78l-0.364 1.686h2.59l-0.299 1.334h-2.59l-0.435 1.98h2.78l-0.293 1.345zM45.849 22.013l-0.646 2.947h-1.656l1.678-7.68h1.949c0.858 0 1.502 0.179 1.933 0.536s0.646 0.881 0.646 1.571c0 0.554-0.15 1.029-0.451 1.426s-0.733 0.692-1.298 0.885l1.417 3.263h-1.803l-1.124-2.947h-0.646zM46.137 20.689h0.424c0.474 0 0.843-0.1 1.108-0.3s0.396-0.504 0.396-0.914c0-0.287-0.086-0.502-0.258-0.646s-0.442-0.216-0.812-0.216h-0.402l-0.456 2.076zM53.712 20.39l2.031-3.11h1.857l-3.355 4.744-0.646 2.936h-1.645l0.646-2.936-1.281-4.744h1.694l0.7 3.11z"></path>
</svg>
</div></div>      </div><!-- -right -->
    
    <div class="footer-primary pull-left">
            <div class="copyright-footer">
        Copyright 2020 &copy; <strong>UX Themes</strong>      </div>
          </div><!-- .left -->
  </div><!-- .container -->
</div><!-- .absolute-footer -->

<a href="#top" class="back-to-top button icon invert plain fixed bottom z-1 is-outline hide-for-medium circle" id="top-link"><i class="icon-angle-up" ></i></a>

</footer><!-- .footer-wrapper -->

</div><!-- #wrapper -->

<!-- Mobile Sidebar -->
<div id="main-menu" class="mobile-sidebar no-scrollbar mfp-hide">
    <div class="sidebar-menu no-scrollbar ">
        <ul class="nav nav-sidebar  nav-vertical nav-uppercase">
              <li class="header-search-form search-form html relative has-icon">
	<div class="header-search-form-wrapper">
		<div class="searchform-wrapper ux-search-box relative form- is-normal"><form role="search" method="get" class="searchform" action="http://hptmedia.vn/matong/">
		<div class="flex-row relative">
						<div class="flex-col search-form-categories">
			<select class="search_categories resize-select mb-0" name="product_cat"><option value="" selected='selected'>Tất cả</option><option value="san-pham">Sản phẩm</option></select>			</div><!-- .flex-col -->
									<div class="flex-col flex-grow">
			  <input type="search" class="search-field mb-0" name="s" value="" placeholder="Tìm kiếm&hellip;" />
		    <input type="hidden" name="post_type" value="product" />
        			</div><!-- .flex-col -->
			<div class="flex-col">
				<button type="submit" class="ux-search-submit submit-button secondary button icon mb-0">
					<i class="icon-search" ></i>				</button>
			</div><!-- .flex-col -->
		</div><!-- .flex-row -->
	 <div class="live-search-results text-left z-top"></div>
</form>
</div>	</div>
</li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-11 current_page_item menu-item-126"><a href="http://hptmedia.vn/matong/" class="nav-top-link">Trang chủ</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-127"><a href="http://hptmedia.vn/matong/gioi-thieu/" class="nav-top-link">Giới thiệu</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-129"><a href="http://hptmedia.vn/matong/danh-muc/san-pham/" class="nav-top-link">Sản phẩm</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-128"><a href="http://hptmedia.vn/matong/tin-tuc/" class="nav-top-link">Tin tức</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-243"><a href="http://hptmedia.vn/matong/lien-he/" class="nav-top-link">Liên hệ</a></li>
<li class="account-item has-icon menu-item">
<a href="http://hptmedia.vn/matong/tai-khoan/"
    class="nav-top-link nav-top-not-logged-in">
    <span class="header-account-title">
    Đăng nhập  </span>
</a><!-- .account-login-link -->

</li>
<li class="header-newsletter-item has-icon">

  <a href="#header-newsletter-signup" class="tooltip" title="Sign up for Newsletter">

    <i class="icon-envelop"></i>
    <span class="header-newsletter-title">
      Newsletter    </span>
  </a><!-- .newsletter-link -->

</li><li class="html header-social-icons ml-0">
	<div class="social-icons follow-icons " ><a href="https://url" target="_blank" data-label="Facebook"  rel="noopener noreferrer nofollow" class="icon plain facebook tooltip" title="Follow on Facebook"><i class="icon-facebook" ></i></a><a href="https://url" target="_blank" rel="noopener noreferrer nofollow" data-label="Instagram" class="icon plain  instagram tooltip" title="Follow on Instagram"><i class="icon-instagram" ></i></a><a href="https://url" target="_blank"  data-label="Twitter"  rel="noopener noreferrer nofollow" class="icon plain  twitter tooltip" title="Follow on Twitter"><i class="icon-twitter" ></i></a><a href="mailto:your@email" data-label="E-mail"  rel="nofollow" class="icon plain  email tooltip" title="Send us an email"><i class="icon-envelop" ></i></a></div></li>        </ul>
    </div><!-- inner -->
</div><!-- #mobile-menu -->
  <script id="lazy-load-icons">
    /* Lazy load icons css file */
    var fl_icons = document.createElement('link');
    fl_icons.rel = 'stylesheet';
    fl_icons.href = 'http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/css/fl-icons.css';
    fl_icons.type = 'text/css';
    var fl_icons_insert = document.getElementsByTagName('link')[0];
    fl_icons_insert.parentNode.insertBefore(fl_icons, fl_icons_insert);
  </script>
      <div id="login-form-popup" class="lightbox-content mfp-hide">
            <div class="woocommerce-notices-wrapper"></div>
<div class="account-container lightbox-inner">

	
		<div class="account-login-inner">

			<h3 class="uppercase">Đăng nhập</h3>

			<form class="woocommerce-form woocommerce-form-login login" method="post">

				
				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label for="username">Tên tài khoản hoặc địa chỉ email&nbsp;<span class="required">*</span></label>
					<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" autocomplete="username" value="" />				</p>
				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label for="password">Mật khẩu&nbsp;<span class="required">*</span></label>
					<input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" autocomplete="current-password" />
				</p>

				
				<p class="form-row">
					<input type="hidden" id="woocommerce-login-nonce" name="woocommerce-login-nonce" value="f2a294bee8" /><input type="hidden" name="_wp_http_referer" value="/matong/" />					<button type="submit" class="woocommerce-Button button" name="login" value="Đăng nhập">Đăng nhập</button>
					<label class="woocommerce-form__label woocommerce-form__label-for-checkbox inline">
						<input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span>Ghi nhớ mật khẩu</span>
					</label>
				</p>
				<p class="woocommerce-LostPassword lost_password">
					<a href="http://hptmedia.vn/matong/tai-khoan/lost-password/">Quên mật khẩu?</a>
				</p>

				
			</form>
		</div><!-- .login-inner -->


</div><!-- .account-login-container -->

          </div>
  	<script type="text/javascript">
		var c = document.body.className;
		c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
		document.body.className = c;
	</script>
			<script type="text/javascript">
			var wc_product_block_data = JSON.parse( decodeURIComponent( '%7B%22min_columns%22%3A1%2C%22max_columns%22%3A6%2C%22default_columns%22%3A3%2C%22min_rows%22%3A1%2C%22max_rows%22%3A6%2C%22default_rows%22%3A1%2C%22thumbnail_size%22%3A300%2C%22placeholderImgSrc%22%3A%22http%3A%5C%2F%5C%2Fhptmedia.vn%5C%2Fmatong%5C%2Fwp-content%5C%2Fuploads%5C%2Fwoocommerce-placeholder-247x296.png%22%2C%22min_height%22%3A500%2C%22default_height%22%3A500%2C%22isLargeCatalog%22%3Afalse%2C%22limitTags%22%3Afalse%2C%22hasTags%22%3Afalse%2C%22productCategories%22%3A%5B%7B%22term_id%22%3A15%2C%22name%22%3A%22S%5Cu1ea3n%20ph%5Cu1ea9m%22%2C%22slug%22%3A%22san-pham%22%2C%22term_group%22%3A0%2C%22term_taxonomy_id%22%3A15%2C%22taxonomy%22%3A%22product_cat%22%2C%22description%22%3A%22%3Cimg%20class%3D%5C%22aligncenter%20wp-image-222%20size-full%5C%22%20src%3D%5C%22https%3A%5C%2F%5C%2Fhptmedia.vn%5C%2Fmatong%5C%2Fwp-content%5C%2Fuploads%5C%2F2019%5C%2F02%5C%2Fbn.jpg%5C%22%20alt%3D%5C%22%5C%22%20width%3D%5C%221170%5C%22%20height%3D%5C%22220%5C%22%20%5C%2F%3E%22%2C%22parent%22%3A0%2C%22count%22%3A10%2C%22filter%22%3A%22raw%22%2C%22link%22%3A%22http%3A%5C%2F%5C%2Fhptmedia.vn%5C%2Fmatong%5C%2Fdanh-muc%5C%2Fsan-pham%5C%2F%22%7D%5D%2C%22homeUrl%22%3A%22http%3A%5C%2F%5C%2Fhptmedia.vn%5C%2Fmatong%5C%2F%22%7D' ) );
		</script>
		<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"http:\/\/hptmedia.vn\/matong\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script type='text/javascript' src='http://hptmedia.vn/matong/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.4'></script>
<script type='text/javascript' src='http://hptmedia.vn/matong/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/matong\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/matong\/?wc-ajax=%%endpoint%%","i18n_view_cart":"Xem gi\u1ecf h\u00e0ng","cart_url":"http:\/\/hptmedia.vn\/matong\/gio-hang\/","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script>
<script type='text/javascript' src='http://hptmedia.vn/matong/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=3.7.1'></script>
<script type='text/javascript' src='http://hptmedia.vn/matong/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/matong\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/matong\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script type='text/javascript' src='http://hptmedia.vn/matong/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=3.7.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/matong\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/matong\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_39ffeddaf809eedaa47475ad9ebcf3f2","fragment_name":"wc_fragments_39ffeddaf809eedaa47475ad9ebcf3f2","request_timeout":"5000"};
/* ]]> */
</script>
<script type='text/javascript' src='http://hptmedia.vn/matong/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=3.7.1'></script>
<script type='text/javascript' src='http://hptmedia.vn/matong/wp-content/themes/flatsome/inc/extensions/flatsome-live-search/flatsome-live-search.js?ver=3.7.2'></script>
<script type='text/javascript' src='http://hptmedia.vn/matong/wp-includes/js/hoverIntent.min.js?ver=1.8.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var flatsomeVars = {"ajaxurl":"http:\/\/hptmedia.vn\/matong\/wp-admin\/admin-ajax.php","rtl":"","sticky_height":"70","user":{"can_edit_pages":false}};
/* ]]> */
</script>
<script type='text/javascript' src='http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/js/flatsome.js?ver=3.7.2'></script>
<script type='text/javascript' src='http://hptmedia.vn/matong/wp-content/themes/flatsome/inc/extensions/flatsome-lazy-load/flatsome-lazy-load.js?ver=1.0'></script>
<script type='text/javascript' src='http://hptmedia.vn/matong/wp-content/themes/flatsome/assets/js/woocommerce.js?ver=3.7.2'></script>
<script type='text/javascript' src='http://hptmedia.vn/matong/wp-includes/js/wp-embed.min.js?ver=5.2.7'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _zxcvbnSettings = {"src":"http:\/\/hptmedia.vn\/matong\/wp-includes\/js\/zxcvbn.min.js"};
/* ]]> */
</script>
<script type='text/javascript' src='http://hptmedia.vn/matong/wp-includes/js/zxcvbn-async.min.js?ver=1.0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var pwsL10n = {"unknown":"M\u1eadt kh\u1ea9u m\u1ea1nh kh\u00f4ng x\u00e1c \u0111\u1ecbnh","short":"R\u1ea5t y\u1ebfu","bad":"Y\u1ebfu","good":"Trung b\u00ecnh","strong":"M\u1ea1nh","mismatch":"M\u1eadt kh\u1ea9u kh\u00f4ng kh\u1edbp"};
/* ]]> */
</script>
<script type='text/javascript' src='http://hptmedia.vn/matong/wp-admin/js/password-strength-meter.min.js?ver=5.2.7'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_password_strength_meter_params = {"min_password_strength":"3","stop_checkout":"","i18n_password_error":"Vui l\u00f2ng nh\u1eadp m\u1eadt kh\u1ea9u kh\u00f3 h\u01a1n.","i18n_password_hint":"G\u1ee3i \u00fd: M\u1eadt kh\u1ea9u ph\u1ea3i c\u00f3 \u00edt nh\u1ea5t 12 k\u00fd t\u1ef1. \u0110\u1ec3 n\u00e2ng cao \u0111\u1ed9 b\u1ea3o m\u1eadt, s\u1eed d\u1ee5ng ch\u1eef in hoa, in th\u01b0\u1eddng, ch\u1eef s\u1ed1 v\u00e0 c\u00e1c k\u00fd t\u1ef1 \u0111\u1eb7c bi\u1ec7t nh\u01b0 ! \" ? $ % ^ & )."};
/* ]]> */
</script>
<script type='text/javascript' src='http://hptmedia.vn/matong/wp-content/plugins/woocommerce/assets/js/frontend/password-strength-meter.min.js?ver=3.7.1'></script>

</body>
</html>
