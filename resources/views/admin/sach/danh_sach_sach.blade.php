@extends('admin.layout.index')
@section('title')
    Danh sách sách
@endsection

@section('content')
<style>
  .dinhkem a{
    color: blue;
  }
  .dinhkem a:hover{
    color:#b5b5ec;
  }
</style>

<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Danh sách sách</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item active">Danh sách</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

<div class="animated fadeIn">

                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <strong class="card-title">Danh sách sách  <a href="tong/sach/them_sach" class="pull-right badge badge-warning"><i class="fas fa-plus-circle" aria-hidden="true"></i> Thêm mới</a></strong>
                        </div>
                        <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>STT</th>
                        <th>Tên sách</th>
                        {{-- <th>Tóm tắt</th> --}}
                        <th>Thể loại</th>
                        <th>Tác giả</th>
                        <th>Nhà xuất bản</th>
                        <th>Phòng</th>
                        <th>Số lượng</th>
                        <th>Ngày nhập</th>
                        <th>Giá tiền</th>
                       
                        
                        <th>Thao tác</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($sach as $key => $sh)
                      <tr>
                        <td>{{$key+1}}</td>
                        <td>
                           <p>{{$sh->ten_sach}}</p>
                           <img width="100px" src="img/{{$sh->anh}}"/>
                        </td>
                        {{-- <td>{!!$sh->tomtat!!}</td> --}}
                        <td>{{$sh->the_loai_sach->ten_the_loai_sach}}</td>
                        <td>
                         {{$sh->tac_gia->ten_tac_gia}}
                        </td>
                        <td>{{$sh->nha_xuat_ban['ten_nha_xuat_ban']}}</td>
                        <td>{{$sh->phong['ten_phong']}}</td>
                        <td>{{$sh->so_luong}}</td>
                        <td>{{$sh->ngay_nhap}}</td>
                        
                        <td>
                            <?php echo number_format($sh->gia_tien_cu); ?>
                        </td>
                       
                        <td>
                          <a href="tong/sach/sua_sach/{{$sh->ma_sach}}" class="badge badge-warning" style="padding:5px 10px;"><i class="fas fa-pen"></i> Sửa</a>
                          <a onclick="return confirm('Bạn có muốn xóa?')" href=" tong/sach/xoa_sach/{{$sh->ma_sach}}" class="badge badge-danger" style="padding:5px 10px;"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                        </div>
                    </div>
                </div>

    </div>

@endsection