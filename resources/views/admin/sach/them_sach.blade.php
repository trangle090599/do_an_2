@extends('admin.layout.index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Sách
                            <small>Thêm Sách</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all()
                            as $err)
                            {{$err}}<br>
                            @endforeach
                        </div>
                        @endif

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                        <form action="tong/sach/them_sach" method="POST" enctype="multipart/form-data"/>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">

                        <div class="form-group">
                            <label>Thể Loại Sách</label>
                            <select class="form-control" name="the_loai_sach">
                              @foreach ($the_loai_sach as $tl)
                                <option value="{{ $tl->ma_the_loai_sach }}">
                                    {{$tl->ten_the_loai_sach}}
                                </option>
                            @endforeach    
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Tác Giả</label>
                            <select class="form-control" name="tac_gia">
                              @foreach ($tac_gia as $tg)
                                <option value="{{ $tg->ma_tac_gia }}">
                                    {{$tg->ten_tac_gia}}
                                </option>
                            @endforeach    
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label>Nhà Xuất Bản</label>
                            <select class="form-control" name="nha_xuat_ban">
                              @foreach ($nha_xuat_ban as $nxb)
                                <option value="{{ $nxb->ma_nha_xuat_ban }}">
                                    {{$nxb->ten_nha_xuat_ban}}
                                </option>
                            @endforeach    
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Phòng</label>
                            <select class="form-control" name="phong">
                              @foreach ($phong as $ph)
                                <option value="{{ $ph->ma_phong }}">
                                    {{$ph->ten_phong}}
                                </option>
                            @endforeach    
                            </select>
                        </div>
                        <div class="form-group">
                                <label>Tên Sách</label>
                                <input class="form-control" name="ten_sach" placeholder="Nhập tên sach" />
                        </div>
                        <div class="form-group">
                                <label>Ảnh</label>
                                <input class="form-control" type="file" name="anh" placeholder="Nhập ảnh" />
                        </div>
                        <div class="form-group">
                                <label>Giới thiệu</label>
                                <textarea name="gioi_thieu"  class="form-control ckeditor" rows="5" value= "" placeholder="Nhập giới thiệu" ></textarea>

                                @if($errors->has('content'))
                                  <span class="error-text">
                                  {{$errors->first('content')}}
                                  </span>
                                @endif

                            </div>
                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea name="noi_dung" id="noi_dung" class="form-control ckeditor" rows="5" ></textarea>

                                @if($errors->has('content'))
                                  <span class="error-text">
                                  {{$errors->first('content')}}
                                  </span>
                                @endif

                            </div>
                            <div class="form-group">
                                <label>Số Lượng</label>
                                <input class="form-control" type="number" name="so_luong" placeholder="Nhập số lượng" />
                            </div>
                            <div class="form-group">
                                <label>Ngày Nhập</label>
                                <input class="form-control" type="date" name="ngay_nhap" placeholder="Nhập ngày nhập" />
                            </div>
                            
                            <div class="form-group">
                                <label>Giá Tiền</label>
                                <input class="form-control" type="number" name="gia_tien_cu" placeholder="Nhập giá tiền" />
                            </div>
                            
                             <div class="form-group">
                                <label>Trạng Thái</label>
                                <label class="radio-inline">
                                    <input name="trang_thai" value="1" checked="" type="radio"> Sản Phẩm Mới
                                </label>
                                <label class="radio-inline">
                                    <input name="trang_thai" value="2" type="radio">Sản Phẩm Cũ
                                </label>
                                <label class="radio-inline">
                                    <input name="trang_thai" value="3" type="radio">Sản Phẩm khuyến mãi
                                </label>
                            </div>
                            <button type="submit" class="btn btn-default">Thêm</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

@endsection