{{--  <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href=""><img src="theme_admin/images/logo.png" alt="Logo"></a>
                <a class="navbar-brand hidden" href="./"><img src="theme_admin/images/logo2.png" alt="Logo"></a>
            </div>
            
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a href=""> <i class="menu-icon fa fa-dashboard"></i> Sách </a>
                    </li>
                 
                    <li class="nav navbar-nav ">
                        <a href=""> <i class="menu-icon fa fa-tasks"></i>Thể loại</a>
                    </li>

                    <li class="nav navbar-nav ">
                        <a href=""> <i class="menu-icon fa fa-bars"></i> Tác giả</a>
                    </li>

                    <li class="nav navbar-nav">
                        <a href="nav navbar-nav"> <i class="menu-icon fa fa-book"></i>Nhà xuất bản</a>
                    </li>

                    <li class="nav navbar-nav">
                        <a href=""> <i class="menu-icon fa fa-id-card-o"></i>Phòng</a>
                    </li>

                   
                    <li class="nav navbar-nav">
                        <a href=""> <i class="menu-icon fa fa-puzzle-piece"></i>Người dùng</a>
                    </li>
                    
                    <li class="nav navbar-nav">
                        <a href=""> <i class="menu-icon fa fa-puzzle-piece"></i>Đặt mượn</a>
                    </li>

                    <li class="nav navbar-nav">
                        <a href=""> <i class="menu-icon fa fa-comments-o"></i>Lương</a>
                    </li>

                     <li class="nav navbar-nav">
                        <a href=""> <i class="menu-icon fa fa-spinner"></i>Vật tư</a>
                    </li>
                    <li class="nav navbar-nav">
                        <a href=""> <i class="menu-icon fa fa-comments-o"></i>Slide</a>
                    </li>
                    <li class="nav navbar-nav">
                        <a href=""> <i class="menu-icon fa fa-comments-o"></i>Thống kê</a>
                    </li>
                    <li class="nav navbar-nav">
                        <a href="logout_kh"> <i class="menu-icon fa fa-comments-o"></i>Logout</a>
                    </li>

                </ul>
            </div>
           
            <!-- /.navbar-collapse -->
        </nav>
    </aside> --}}


<nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="" class="nav-link }">
              <i class="nav-icon fas fa-cogs"></i>
              <p>
                Trang chủ
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="" class="nav-link ">
              <i class="nav-icon fas fa fa-book"></i>
              <p>
               Sách
              </p>
              <i class="right fas fa-angle-left"></i>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="tong/sach/danh_sach_sach" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Kho sách</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="tong/sach/them_sach" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm mới</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Thể loại
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>

            <ul class="nav nav-treeview">

             <li class="nav-item">
                <a href="tong/the_loai_sach/danh_sach_the_loai_sach" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách thể loại</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="tong/the_loai_sach/them_the_loai_sach" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm mới</p>
                </a>
              </li>
            </ul>
          </li>
  

                 
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Tác giả
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="tong/tac_gia/danh_sach_tac_gia" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách tác giả</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="tong/tac_gia/them_tac_gia" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm mới</p>
                </a>
              </li>
              
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="" class="nav-link ">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Nhà xuất bản
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="tong/nha_xuat_ban/danh_sach_nha_xuat_ban" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách nhà xuất bản</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="tong/nha_xuat_ban/them_nha_xuat_ban" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm mới</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="" class="nav-link ">
              <i class="nav-icon fas fa-star"></i>
              <p>
                Ngành
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="tong/nganh/danh_sach_nganh" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách ngành</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="tong/nganh/them_nganh" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm mới</p>
                </a>
              </li>
            </ul>
          </li>


          <li class="nav-item has-treeview">
            <a href="" class="nav-link ">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Phòng
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="tong/phong/danh_sach_phong" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách phòng</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="tong/phong/them_phong" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm mới</p>
                </a>
              </li>
            </ul>
          </li>

          
          <li class="nav-item has-treeview">
            <a href="" class="nav-link ">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Người dùng
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="tong/user/danh_sach_user" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách người dùng</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="tong/user/them_user" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm mới</p>
                </a>
              </li>
            </ul>
          </li>
        
          <li class="nav-item has-treeview">
            <a href="" class="nav-link ">
              <i class="nav-icon fas fa fa-book"></i>
              <p>
                Đặt mượn
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="tong/dat_muon/danh_sach_dat_muon" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách đặt mượn</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="tong/dat_muon/them_dat_muon" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm mới</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="" class="nav-link ">
              <i class="nav-icon fas fa-star"></i>
              <p>
                Lương
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="tong/luong/danh_sach_luong" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách lương</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="tong/luong/them_luong" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm mới</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="" class="nav-link ">
              <i class="nav-icon fas fa-gavel"></i>
              <p>
                Vật tư
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="tong/vat_chat/danh_sach_vat_chat" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách vật tư</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="tong/vat_chat/them_vat_chat" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm mới</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="" class="nav-link ">
              <i class="nav-icon fas fa-star"></i>
              <p>
                Slide
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="tong/slide/danh_sach_slide" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách slide</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="tong/slide/them_slide" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm mới</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="" class="nav-link ">
              <i class="nav-icon fas fa-flag"></i>
              <p>
                Thống kê
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="tong/thong_ke/thong_ke_sach" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thống kê sách</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="logout_kh" class="nav-link ">
              <i class="nav-icon fas fa-cogs"></i>
              <p>
                Logout
                
              </p>
            </a>
          </li>
            
          </li>
         
         
         
         
            </ul>
      </nav>
