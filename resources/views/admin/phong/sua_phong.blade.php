@extends('admin.layout.index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Sửa Phòng
                            <small>{{$phong -> ten_phong}}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all()
                            as $err)
                            {{$err}}<br>
                            @endforeach
                        </div>
                        @endif

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                        <form action="tong/phong/sua_phong/{{$phong->ma_phong}}" method="POST"/>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <label>Tên Phòng</label>
                                <input class="form-control" name="ten_phong" placeholder="Nhập Tên Phòng" value="{{$phong->ten_phong}}" />
                            </div>
                            <div class="form-group">

                            <label>Tên Dụng Cụ</label>
                            <select class="form-control" name="ma_vat_chat">
                              @foreach ($vat_chat as $vc)
                                @if($phong->ma_vat_chat == $vc->ma_phong)
                                {{"selected"}}
                                @endif
                                <option value="{{ $vc->ma_vat_chat }}">
                                    {{$vc->ten_vat_chat}}
                                </option>
                            @endforeach    
                            </select>
                        </div>
                            <div class="form-group">
                                <label>Số Lượng dụng cụ có trong phòng</label>
                                <input class="form-control" name="so_luong_vc" type="number" placeholder="Nhập Tên Phòng" value="{{$phong->so_luong_vc}}" />
                            </div>
                            
                            <button type="submit" class="btn btn-default">Sửa</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection



