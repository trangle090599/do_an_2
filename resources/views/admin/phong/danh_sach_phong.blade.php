@extends('admin.layout.index')
@section('title')
    Danh sách phòng
@endsection

@section('content')
<style>
  .dinhkem a{
    color: blue;
  }
  .dinhkem a:hover{
    color:#b5b5ec;
  }
</style>

<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Danh sách phòng</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item active">Danh sách phòng</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

<div class="animated fadeIn">

                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <strong class="card-title">Danh sách phòng <a href="tong/phong/them_phong" class="pull-right badge badge-warning"><i class="fas fa-plus-circle" aria-hidden="true"></i> Thêm mới</a></strong>
                        </div>
                        <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr align="center">
                                <th>Mã phòng</th>
                                <th>Tên Dụng cụ</th>
                                <th>Tên Phòng</th>             
                                <th>Số Lượng Dụng Cụ Có Trong Phòng </th>
                                <th>Thao tác</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($phong as $ph)
                            <tr class="even gradeC" align="center">
                                <td>{{$ph['ma_phong']}}</td>
                                <td>{{$ph->vat_chat['ten_vat_chat']}}</td>
                             
                                <td>{{$ph['ten_phong']}}</td>
                                <td>{{$ph['so_luong_vc']}}</td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="tong/phong/sua_phong/{{$ph->ma_phong}}" class="badge badge-warning" style="padding:5px 10px;"><i class="fas fa-pen"></i> Sửa</a>
                          <a onclick="return confirm('Bạn có muốn xóa?')" href=" tong/phong/xoa_phong/{{$ph->ma_phong}}" class="badge badge-danger" style="padding:5px 10px;"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td></td>
                            </tr>
                            @endforeach
                        </tbody>
                  </table>
                        </div>
                    </div>
                </div>

    </div>

@endsection