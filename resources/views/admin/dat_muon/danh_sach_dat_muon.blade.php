@extends('admin.layout.index')
@section('title')
    Danh sách đơn đặt mượn
@endsection

@section('content')
<style>
  .dinhkem a{
    color: blue;
  }
  .dinhkem a:hover{
    color:#b5b5ec;
  }
</style>

<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Danh sách đơn đặt mượn</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item active">Danh sách</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

<div class="animated fadeIn">

                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <strong class="card-title">Danh sách đơn đặt mượn <a href="tong/dat_muon/them_dat_muon" class="pull-right badge badge-warning"><i class="fas fa-plus-circle" aria-hidden="true"></i> Thêm mới</a></strong>
                        </div>
                        <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                            <tr align="center">
                               
                                <th>Tên Sách</th>
                                <th>Tên Độc Giả</th>
                                <th>Ngày Đặt</th>
                                <th>Trạng Thái</th>
                                <th>Số Lượng</th>
                                <th>Sửa</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($dat_muon as $dm)
                            <tr class="even gradeC" align="center">
                                
                                <td>{{$dm->sach->ten_sach}}</td>
                                <td>{{$dm->user->ten_user}}</td>
                                <td>{{$dm->ngay_dat}}</td>
                                 <td>@if($dm->trang_thai== 1)
                                    {{"Đã Trả"}}
                                    @else
                                    {{"Chưa Trả"}}
                                    @endif</td>
                               <td> {{$dm->so_luong}}</td>
                                
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="tong/dat_muon/sua_dat_muon/{{$dm->ma_dat_muon}}" class="badge badge-warning" style="padding:5px 10px;"><i class="fas fa-pen"></i> Sửa</a>
                                <?php
                                if ($dm->cap_do == 0) {
                                ?>
                                <a onclick="return confirm('Bạn có muốn xóa?')" href="tong/dat_muon/xoa_dat_muon/{{$dm->ma_dat_muon}}" class="badge badge-danger" style="padding:5px 10px;"><i class="fas fa-trash-alt"></i> Xóa</a>
                                <?php } ?>
                            </tr>
                            @endforeach
                        </tbody>
                  </table>
                        </div>
                    </div>
                </div>

    </div>

@endsection