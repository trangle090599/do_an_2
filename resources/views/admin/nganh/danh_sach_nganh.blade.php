@extends('admin.layout.index')
@section('title')
    Danh sách ngành
@endsection

@section('content')
<style>
  .dinhkem a{
    color: blue;
  }
  .dinhkem a:hover{
    color:#b5b5ec;
  }
</style>

<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Danh sách ngành</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item active">Danh sách ngành</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

<div class="animated fadeIn">

                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <strong class="card-title">Danh sách ngành <a href="tong/phong/them_phong" class="pull-right badge badge-warning"><i class="fas fa-plus-circle" aria-hidden="true"></i> Thêm mới</a></strong>
                        </div>
                        <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                            <tr align="center">
                                <th>Mã Ngành</th>
                                <th>Tên Ngành</th>                             
                                <th>Thao tác</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($nganh as $nganh)
                            <tr class="even gradeC" align="center">
                                <td>{{$nganh->ma_nganh}}</td>
                                <td>{{$nganh->ten_nganh}}</td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="tong/nganh/them_nganh/{{$nganh->ma_nganh}}" class="badge badge-warning" style="padding:5px 10px;"><i class="fas fa-pen"></i> Sửa</a>
                          <a onclick="return confirm('Bạn có muốn xóa?')" href=" tong/nganh/xoa_nganh/{{$nganh->ma_nganh}}" class="badge badge-danger" style="padding:5px 10px;"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td></td>
                            </tr>
                            @endforeach
                        </tbody>
                  </table>
                        </div>
                    </div>
                </div>

    </div>

@endsection