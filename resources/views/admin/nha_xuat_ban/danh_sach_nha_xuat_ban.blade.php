@extends('admin.layout.index')
@section('title')
    Danh sách nhà xuất bản
@endsection

@section('content')
<style>
  .dinhkem a{
    color: blue;
  }
  .dinhkem a:hover{
    color:#b5b5ec;
  }
</style>

<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Danh sách nhà xuất bản</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item active">Danh sách nhà xuất bản</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

<div class="animated fadeIn">

                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <strong class="card-title">Danh sách nhà xuất bản <a href="tong/dat_muon/them_dat_muon" class="pull-right badge badge-warning"><i class="fas fa-plus-circle" aria-hidden="true"></i> Thêm mới</a></strong>
                        </div>
                        <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                            <tr align="center">
                                <th>Mã nhà xuất bản</th>
                                <th>Tên nhà xuất bản</th>                             
                                <th>Thao tác</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($nha_xuat_ban as $xb)
                            <tr class="even gradeC" align="center">
                                <td>{{$xb->ma_nha_xuat_ban}}</td>
                                <td>{{$xb->ten_nha_xuat_ban}}</td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="tong/nha_xuat_ban/sua_nha_xuat_ban/{{$xb->ma_nha_xuat_ban}}" class="badge badge-warning" style="padding:5px 10px;"><i class="fas fa-pen"></i> Sửa</a>
                          <a onclick="return confirm('Bạn có muốn xóa?')" href=" tong/nha_xuat_ban/xoa_nha_xuat_ban/{{$xb->ma_nha_xuat_ban}}" class="badge badge-danger" style="padding:5px 10px;"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td></td>
                            </tr>
                            @endforeach
                        </tbody>
                  </table>
                        </div>
                    </div>
                </div>

    </div>

@endsection