@extends('admin.layout.index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <small>Thêm Dụng Cụ</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all()
                            as $err)
                            {{$err}}<br>
                            @endforeach
                        </div>
                        @endif

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                        <form action="tong/vat_chat/them_vat_chat" method="POST"/>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <label>Tên Dụng Cụ</label>
                                <input class="form-control" name="ten_vat_chat" placeholder="Nhập tên Dụng cụ" />
                            </div>
                            <div class="form-group">
                                <label>Tổng Số Lương</label>
                                <input class="form-control" name="so_luong" placeholder="Nhập số lượng" />
                            </div>
                            <div class="form-group">
                                <label>Giá Tiền/ 1 chiếc</label>
                                <input class="form-control" name="gia_tien" placeholder="Nhập giá tiền" />
                            </div>
                            <button type="submit" class="btn btn-default">Thêm</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection