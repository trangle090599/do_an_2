@extends('admin.layout.index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Sửa Dụng Cụ
                            <small>{{$vat_chat -> ten_vat_chat}}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all()
                            as $err)
                            {{$err}}<br>
                            @endforeach
                        </div>
                        @endif

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                        <form action="tong/vat_chat/sua_vat_chat/{{$vat_chat->ma_vat_chat}}" method="POST"/>  
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <label>Tên Dụng Cụ</label>
                                <input class="form-control" name="ten_vat_chat" placeholder="Nhập Tên Dụng Cụ" value="{{$vat_chat->ten_vat_chat}}" />
                            </div>
                            <div class="form-group">
                                <label>Sô Lượng</label>
                                <input class="form-control" name="so_luong" placeholder="Nhập Số Lượng" value="{{$vat_chat->so_luong}}" />
                            </div>
                            <div class="form-group">
                                <label>Giá Tiền</label>
                                <input class="form-control" name="gia_tien" placeholder="Nhập Giá Tiền" value="{{$vat_chat->gia_tien}}" />
                            </div>
                            <button type="submit" class="btn btn-default">Sửa</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection



