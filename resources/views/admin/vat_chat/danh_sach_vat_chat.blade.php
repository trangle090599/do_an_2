@extends('admin.layout.index')
@section('title')
    Danh sách Vật tư
@endsection

@section('content')
<style>
  .dinhkem a{
    color: blue;
  }
  .dinhkem a:hover{
    color:#b5b5ec;
  }
</style>

<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Danh sách Vật tư</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item active">Danh sách Vật tư</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

<div class="animated fadeIn">
  <div class="col-md-12">
    <div class="card card-info">
      <div class="card-header">
          <strong class="card-title">Danh sách Vật tư <a href="tong/vat_chat/them_vat_chat" class="pull-right badge badge-warning"><i class="fas fa-plus-circle" aria-hidden="true"></i> Thêm mới</a></strong>
      </div>
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr align="center">
                <th>Mã Vật tư</th>
                <th>Tên Vật tư</th> 
                <th>Số Lương</th>
                <th>Giá Tiền</th>                           
                <th>Thao tác</th>
            </tr>
        </thead>
        <tbody>
             @foreach($vat_chat as $vc)
            <tr class="even gradeC" align="center">
                <td>{{$vc->ma_vat_chat}}</td>
                <td>{{$vc->ten_vat_chat}}</td>
                <td>{{$vc->so_luong}}</td>
                <td> <?php echo number_format($vc->gia_tien); ?></td>
                <td class="center"><i class="fa fa-pencil fa-fw"></i> 
                <a href="tong/vat_chat/sua_vat_chat/{{$vc->ma_vat_chat}}" class="badge badge-warning" style="padding:5px 10px;"><i class="fas fa-pen"></i> Sửa</a>
                <a onclick="return confirm('Bạn có muốn xóa?')" href="tong/vat_chat/xoa_vat_chat/{{$vc->ma_vat_chat}}" class="badge badge-danger" style="padding:5px 10px;"><i class="fas fa-trash-alt"></i> Xóa</a>
              </td></td>
            </tr>
            @endforeach
          </tbody>
        </table>
              </div>
          </div>
      </div>

    </div>

@endsection