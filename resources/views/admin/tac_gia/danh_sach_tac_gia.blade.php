@extends('admin.layout.index')
@section('title')
    Danh sách tác giả
@endsection

@section('content')
<style>
  .dinhkem a{
    color: blue;
  }
  .dinhkem a:hover{
    color:#b5b5ec;
  }
</style>

<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Danh sách tác giả</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item active">Danh sách tác giả</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

<div class="animated fadeIn">

                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <strong class="card-title">Danh sách tác giả <a href="tong/tac_gia/them_tac_gia" class="pull-right badge badge-warning"><i class="fas fa-plus-circle" aria-hidden="true"></i> Thêm mới</a></strong>
                        </div>
                        <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                            <tr align="center">
                                <th>Mã Tác Giả</th>
                                <th>Tên Tác Giả</th>   
                                <th>Giới Thiệu</th>                             
                                <th>Thao tác</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($tac_gia as $tg)
                            <tr class="even gradeC" align="center">
                                <td>{{$tg->ma_tac_gia}}</td>
                                <td>{{$tg->ten_tac_gia}}</td>
                                <td>{{$tg->gioi_thieu}}</td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="tong/tac_gia/sua_tac_gia/{{$tg->ma_tac_gia}}" class="badge badge-warning" style="padding:5px 10px;"><i class="fas fa-pen"></i> Sửa</a>
                          <a onclick="return confirm('Bạn có muốn xóa?')" href="tong/tac_gia/xoa_tac_gia/{{$tg->ma_tac_gia}}" class="badge badge-danger" style="padding:5px 10px;"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td></td>
                            </tr>
                            @endforeach
                        </tbody>
                  </table>
                        </div>
                    </div>
                </div>
    </div>

@endsection