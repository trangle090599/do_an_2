@extends('admin.layout.index')
@section('title')
    Danh sách Người dùng
@endsection

@section('content')
<style>
  .dinhkem a{
    color: blue;
  }
  .dinhkem a:hover{
    color:#b5b5ec;
  }
</style>

<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Danh sách Người dùng</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item active">Danh sách Người dùng</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

<div class="animated fadeIn">
  <div class="col-md-12">
    <div class="card card-info">
      <div class="card-header">
          <strong class="card-title">Danh sách Người dùng <a href="tong/user/them_user" class="pull-right badge badge-warning"><i class="fas fa-plus-circle" aria-hidden="true"></i> Thêm mới</a></strong>
      </div>
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
        <thead>
            
            <tr align="center">
                <th>Mã user</th>
                <th>Ngành</th>
                <th>Tên user</th>
                <th>Email</th>        
                <th>Địa chỉ</th>
                <th>Ngày sinh</th>
                <th>Giới tính</th>
                <th>SĐT</th>
                <th>Cấp độ</th>
                @if(Auth::check())
                @if( Auth::user()->cap_do == 0)
                <th>Thao tác</th>
                @endif
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach($user as $ad)
            <tr class="even gradeC" align="center">
                <td>{{$ad->ma_user}}</td>
                <td>{{$ad->nganh->ten_nganh}}</td>
                <td>{{$ad->ten_user}}</td>
                <td>{{$ad->email}}</td>
                <td>{{$ad->dia_chi}}</td>
                <td>{{$ad->ngay_sinh}}</td>
                <td>@if($ad->gioi_tinh == 1)
                    {{"Nữ"}}
                    @else
                    {{"Nam"}}
                    @endif</td>
                <td>{{$ad->sdt}}</td>
                <td>
                    @if($ad->cap_do == 0)
                    {{"Admin"}}
                    @else
                    {{"Độc giả"}}
                    @endif
                </td>
                @if(Auth::check())
                @if( Auth::user()->cap_do == 0)
                <td class="center">
                
                <a href="tong/user/sua_user/{{$ad->ma_user}}" class="badge badge-warning" style="padding:5px 10px;"><i class="fas fa-pen"></i> Sửa</a>
                <a onclick="return confirm('Bạn có muốn xóa?')" href="tong/user/xoa_user/{{$ad->ma_user}}" class="badge badge-danger" style="padding:5px 10px;"><i class="fas fa-trash-alt"></i> Xóa</a>
              </td></td>
              @endif
              @endif
            </tr>
            @endforeach
          </tbody>
        </table>
              </div>
          </div>
      </div>

    </div>

@endsection