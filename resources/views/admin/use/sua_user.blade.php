@extends('admin.layout.index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Sửa Admin
                            <small>{{$user -> ten_user}}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all()
                            as $err)
                            {{$err}}<br>
                            @endforeach
                        </div>
                        @endif

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                        <form action="tong/user/sua_user/{{$user->ma_user}}" method="POST"/>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="form-group">
                            <label>Ngành</label>
                            <select class="form-control" name="nganh">
                            @foreach ($nganh as $ng)
                                @if($user->ma_nganh == $ng->ma_user)
                                {{"selected"}}
                                @endif
                                <option value="{{ $ng->ma_nganh }}">
                                    {{$ng->ten_nganh}}
                                </option>
                            @endforeach    
                            </select>
                        </div>
                            <div class="form-group">
                                <label>Tên Admin</label>
                                <input class="form-control" name="ten_user" placeholder="Nhập tên admin" value="{{$user->ten_user}}" />
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" placeholder="Nhập email" 
                                value="{{$user->email}}" readonly="" />
                            </div>
                             <div class="form-group">
                                <label>Địa Chỉ</label>
                                <input class="form-control " name="dia_chi" placeholder="Nhập địa chỉ"
                                 value="{{$user->dia_chi}}"/>
                            </div>
                             <div class="form-group">
                                <label>Ngày Sinh</label>
                                <input type = "date"class="form-control" name="ngay_sinh" placeholder="Nhập ngày sinh" value="{{$user->ngay_sinh}}"/>
                            </div>
                             <div class="form-group">
                                <label>SĐT</label>
                                <input type="number" class="form-control" name="sdt" placeholder="Nhập số điện thoại"
                                value="{{$user->sdt}}" />
                            </div>
                           
                            <div class="form-group">
                                <label>Giới Tính</label>
                                <label class="radio-inline">
                                    <input name="gioi_tinh" value="0" checked="" type="radio">Nam
                                </label>
                                <label class="radio-inline">
                                    <input name="gioi_tinh" value="1" type="radio">Nữ
                                </label>
                            </div>
                            @if($user->cap_do == 0)
                            <div class="form-group">
                               <label>Cấp độ</label>
                                <label class="radio-inline">
                                    <input name="cap_do" value="0" checked="" type="radio">Super admin
                                </label>
                                <label class="radio-inline">
                                    <input name="cap_do" value="1" type="radio">Admin
                                </label>
                            </div>
                            @endif
                            <button type="submit" class="btn btn-default">Sửa</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection



