@extends('admin.layout.index')
@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tài khoản</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Tài khoản</a></li>
              <li class="breadcrumb-item active"><a href="">Danh sách</a></li>
              <li class="breadcrumb-item active">Chỉnh sửa thông tin cá nhân</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
<div class="animated fadeIn">
   <div class="row">
      <div class="col-md-12">
         <div class="card card-warning">
            <div class="card-header">
               <strong class="card-title">Sửa tài khoản</strong>
            </div>

            <div class="card-body">
              
               <form action="tong/user/sua_thongtincanhan_user/{{$user->ma_user}}" method="POST" enctype="multipart/form-data">
               @csrf
               <div class="col-md-12">
                
                  <style>
                    .error-text{
                      color:red;
                    }
                  </style>
                  @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all()
                            as $err)
                            {{$err}}<br>
                            @endforeach
                        </div>
                        @endif

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                <span class="badge badge-pill badge-success">{{session('thongbao')}}</span>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                                </button>
                            </div>
                        @endif
                          
          

                <div class="row">
               <div class="col-md-6">
                  <div class="form-group">
                     <label>Nhập tên tài khoản:</label>
                     <input type="text" name="ten_user" value="{{$user->ten_user}}" class="form-control" placeholder="Nhập tên">
                     @if($errors->has('ten_user'))
                     <span class="error-text">
                     {{$errors->first('ten_user')}}
                     </span>
                     @endif
                  </div>
                  <div class="form-group">
                     <label>Email:</label>
                     <input type="text" name="email" value="{{$user->email}}" class="form-control" placeholder="Nhập email" readonly="">
                     @if($errors->has('email'))
                     <span class="error-text">
                     {{$errors->first('email')}}
                     </span>
                     @endif
                  </div>
                  <div class="form-group">
                     <label>SĐT:</label>
                     <input type="number" name="sdt" value="{{$user->sdt}}" class="form-control" placeholder="Nhập phone">
                  </div>
                  <div class="form-group">
                     <label>Mật khẩu:</label>      
                     <input type="Password" name="password" class="form-control" value="" placeholder="Nhập mật khẩu">
                     @if($errors->has('password'))
                     <span class="error-text">
                     {{$errors->first('password')}}
                     </span>
                     @endif
                  </div>
                  <div class="form-group">
                     <label>Nhập lại Mật khẩu:</label>
                     <input type="Password" name="passwordAgain" value=""class="form-control" placeholder="Nhập lại mật khẩu" />
                     @if($errors->has('passwordAgain'))
                     <span class="error-text">
                     {{$errors->first('passwordAgain')}}
                     </span>
                     @endif
                  </div>
                  
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <label>Địa chỉ:</label>
                     <input type="text" name="dia_chi" value="{{$user->dia_chi}}" class="form-control" placeholder="Nhập địa chỉ">
                  </div>
                  <div class="form-group">
                     <label>Ngành:</label>
                     <select class="form-control" name="nganh">
                            @foreach ($nganh as $ng)
                                @if($user->ma_nganh == $ng->ma_user)
                                {{"selected"}}
                                @endif
                                <option value="{{$ng->ma_nganh }}">
                                    {{$ng->ten_nganh}}
                                </option>
                            @endforeach    
                            </select>
                  </div>
                  <div class="form-group">
                     <label>Ngày sinh:</label>
                     <input type = "date"class="form-control" name="ngay_sinh" placeholder="Nhập ngày sinh" value="{{$user->ngay_sinh}}"/>
                  </div>
               </div>
             </div>
             </div>
               <button type="submit" class="btn btn-primary btn-sm">Sửa</button>
               <button type="reset" class="btn btn-danger btn-sm">Làm mới</button>
               <form>
            </div>

         </div>

      </div>
   </div>
</div>
</div>
@endsection