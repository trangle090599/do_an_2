@extends('admin.layout.index')
@section('title')
    Danh sách thể loại sách
@endsection

@section('content')
<style>
  .dinhkem a{
    color: blue;
  }
  .dinhkem a:hover{
    color:#b5b5ec;
  }
</style>

<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Danh sách thể loại sách</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item active">Danh sách thể loại sách</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

<div class="animated fadeIn">
  <div class="col-md-12">
    <div class="card card-info">
      <div class="card-header">
          <strong class="card-title">Danh sách thể loại sách <a href="tong/the_loai_sach/them_the_loai_sach" class="pull-right badge badge-warning"><i class="fas fa-plus-circle" aria-hidden="true"></i> Thêm mới</a></strong>
      </div>
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr align="center">
                <th>Mã thể loại sách</th>
                <th>Tên thể loại sách</th>                             
                <th>Thao tác</th>
                
            </tr>
        </thead>
        <tbody>
            @foreach($the_loai_sach as $tl)
            <tr class="even gradeC" align="center">
                <td>{{$tl->ma_the_loai_sach}}</td>
                <td>{{$tl->ten_the_loai_sach}}</td>
                <td class="center"><i class="fa fa-pencil fa-fw"></i> 
                <a href="tong/the_loai_sach/sua_the_loai_sach/{{$tl->ma_the_loai_sach}}" class="badge badge-warning" style="padding:5px 10px;"><i class="fas fa-pen"></i> Sửa</a>
                <a onclick="return confirm('Bạn có muốn xóa?')" href="tong/the_loai_sach/xoa_the_loai_sach/{{$tl->ma_the_loai_sach}}" class="badge badge-danger" style="padding:5px 10px;"><i class="fas fa-trash-alt"></i> Xóa</a>
              </td></td>
            </tr>
            @endforeach
          </tbody>
        </table>
              </div>
          </div>
      </div>

    </div>

@endsection