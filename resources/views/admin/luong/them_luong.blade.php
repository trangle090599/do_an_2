@extends('admin.layout.index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Lương
                            <small>Thêm Lương</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all()
                            as $err)
                            {{$err}}<br>
                            @endforeach
                        </div>
                        @endif

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                        <form action="tong/luong/them_luong" method="POST"/>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                 <label>Tên Người Nhận</label>
                                 <select class="form-control" name="user">
                              @foreach ($user as $ad)
                                <option value="{{ $ad->ma_user }}">
                                    {{$ad->ten_user}}
                                </option>
                            @endforeach    
                            </select>
                            </div>
                            <div class="form-group">
                                <label>Tên Lương</label>
                                <input class="form-control"  name="thang" placeholder="Nhập tên lương" />
                            </div>
                            <div class="form-group">
                                <label>Số Tiền Nhận</label>
                                <input class="form-control" name="tien" placeholder="Nhập tiền" />
                            </div>
                             
                             <div class="form-group">
                                <label>Ngày nhận</label>
                                <input type = "date" class="form-control" name="ngay_nhan" placeholder="Nhập ngày nhận" />
                            </div>
                            
                            <button type="submit" class="btn btn-default">Thêm</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection