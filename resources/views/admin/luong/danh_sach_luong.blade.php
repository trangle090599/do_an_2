@extends('admin.layout.index')
@section('title')
    Danh sách lương
@endsection

@section('content')
<style>
  .dinhkem a{
    color: blue;
  }
  .dinhkem a:hover{
    color:#b5b5ec;
  }
</style>

<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Danh sách lương</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item active">Danh sách</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

<div class="animated fadeIn">

                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <strong class="card-title">Danh sách lương <a href="tong/luong/them_luong" class="pull-right badge badge-warning"><i class="fas fa-plus-circle" aria-hidden="true"></i> Thêm mới</a></strong>
                        </div>
                        <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                            <tr align="center">
                                <th>Mã Lương</th>
                                <th>Tên người nhận</th> 
                         
                                <th>Tên Lương</th>
                                <th>Số Tiền</th>
                                <th>Ngày Nhận</th>                            
                                <th>Sửa</th>
                                <th>Xóa</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($luong as $lg)
                            <tr class="even gradeC" align="center">
                                <td>{{$lg->ma_luong}}</td>
                                <td>{{$lg->user['ten_user']}}</td>
                                <td>{{$lg->thang}}</td>
                                <td><?php echo number_format($lg->tien); ?></td>
                                <td>{{$lg->ngay_nhan}}</td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="tong/luong/sua_luong/{{$lg->ma_luong}}" class="badge badge-warning" style="padding:5px 10px;"><i class="fas fa-pen"></i> Sửa</a>
                          <a onclick="return confirm('Bạn có muốn xóa?')" href="tong/luong/xoa_luong/{{$lg->ma_luong}}" class="badge badge-danger" style="padding:5px 10px;"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td></td>
                            </tr>
                            @endforeach
                        </tbody>
                  </table>
                        </div>
                    </div>
                </div>

    </div>

@endsection