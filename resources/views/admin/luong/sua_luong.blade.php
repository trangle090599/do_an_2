@extends('usern.layout.index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Sửa Lương
                            
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all()
                            as $err)
                            {{$err}}<br>
                            @endforeach
                        </div>
                        @endif

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                        <form action="tong/luong/sua_luong/{{$luong->ma_luong}}" method="POST"/>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">

                        <div class="form-group">
                            <label>Người Nhận</label>
                            <select class="form-control" name="user">
                              @foreach ($user as $ad)
                                @if($luong->ma_user == $ad->ma_luong)
                                    {{"selected"}}
                                @endif
                                <option value="{{ $ad->ma_user }}">
                                    {{$ad->ten_user}}
                                </option>
                            @endforeach    
                            </select>
                        </div>
                            <div class="form-group">
                                <label>Tên Lương</label>
                                <input class="form-control" name="thang" placeholder="Nhập Tháng Nhận" value="{{$luong->thang}}" />
                            </div>
                            <div class="form-group">
                                <label>Tiền Nhận</label>
                                <input class="form-control" name="tien" placeholder="Nhập Tiền Nhận" value="{{$luong->tien}}" />
                            </div>
                            <div class="form-group">
                                <label>Ngày Nhận</label>
                                <input class="form-control" type="date" name="ngay_nhan" placeholder="Nhập Ngà Nhận" value="{{$luong->ngay_nhan}}" />
                            </div>
                            
                            <button type="submit" class="btn btn-default">Sửa</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection



