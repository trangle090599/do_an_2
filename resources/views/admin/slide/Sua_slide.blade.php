@extends('admin.layout.index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"> Sửa Slide
                            <small>{{$slide ->link}}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-1Â2 -->
                    
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all()
                            as $err)
                            {{$err}}<br>
                            @endforeach
                        </div>
                        @endif

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                        <form action="tong/slide/sua_slide/{{$slide->ma_slide}}" method="POST" enctype="multipart/form-data"/>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                           
                            <div class="form-group">
                                <label>Link</label>
                                <input class="form-control" name="link" placeholder="Nhập link" value="{{$slide->link}}" />
                            </div>
                            <div class="form-group">
                                <label>Ảnh</label>
                                <p><img width="200px" height="200px" src="img_slide/{{$slide->anh}}"></p>
                                <input class="form-control" type="file" name="anh" placeholder="Nhập ảnh" />
                             </div>
                            
                            
                            <button type="submit" class="btn btn-default">Sửa</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection



