
@extends('admin.layout.index')
@section('content')

<!DOCTYPE html>
<html>
<head>
  <title></title>
  <style type="text/css">

  </style>
</head>
<body>
<form action="tong/thong_ke/thong_ke_sach" method="get">

 <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<h2 align="center" ><u><i>Số Lượng Sách Trong Thư Viện</i></u></h1>

<div id="container" style="width: 63%; height: 525px; margin: 0 auto;"></div>
</form>
</body>
</html>
<script type="text/javascript">
 Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Tổng số sách'
        }

    },
    
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y} quyển'
            }
        }
    },

    

    series: [
        {
            name: "Browsers",
            colorByPoint: true,
            data: [
                {
                    name: "Chuyên Ngành",
                    y: 102,
                    drilldown: "Chrome"
                },
                {
                    name: "Toán Học",
                    y: 80,
                    drilldown: "Firefox"
                },
                {
                    name: "Văn Học",
                    y: 77,
                    drilldown: "Internet Explorer"
                },
                {
                    name: "Vật Lý",
                    y: 40,
                    drilldown: "Safari"
                },
                {
                    name: "Hóa Học",
                    y: 54,
                    drilldown: "Edge"
                },
                {
                    name: "Lịch Sử",
                    y: 29,
                    drilldown: "Opera"
                },
                {
                    name: "Địa Lý",
                    y: 31,
                    drilldown:"Opera"
                },
                {
                    name: "Truyện Tranh",
                    y: 0,
                    drilldown:"Opera"
                }

            ]
        }
    ],
   
});
</script>
@endsection