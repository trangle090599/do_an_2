 
<?php
  $slide = App\Slide::select('anh')->get();
 ?>  
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel" style="margin-bottom: 100px ">
  <div class="carousel-inner" style="height: 400px">
    @foreach($slide as $sl)

    <div class="carousel-item @if($loop->first) active @endif">
      <img class="d-block w-100" src="img_slide/{{$sl->anh}}">
    </div>
    
    @endforeach
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>




