@extends('master')
@section('content')

		
<div class="container" style="margin:2% 5% 7% 7%">
	 {!! csrf_field() !!}
		<div class="col-sm-6 col-md-4 col-md-offset-4">
			<div class="panel panel-default">
				 @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all()
                            as $err)
                            {{$err}}<br>
                            @endforeach
                        </div>
                        @endif

                        @if(session('thongbao'))
                            
                                {{session('thongbao')}}
                          
                        @endif
				<div class="panel-body">
					<form role="form" action="{{ url('/login') }}" method="POST">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						
						<fieldset>
							
							<div class="row">
								<div class="col-sm-12 col-md-10  col-md-offset-1 ">
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span> 
											<input class="form-control" placeholder="Email" name="email" type="text" value="{{ old('email') }}" autofocus>
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
											<input class="form-control" placeholder="Mật khẩu" name="password" type="password" value="">
										</div>
									</div>
									<div class="form-group">
										<input type="submit" class="btn btn-lg btn-primary btn-block" value="Đăng nhập">
									</div>
									
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>

<style>


</style>
@endsection
