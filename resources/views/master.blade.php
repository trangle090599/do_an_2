<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Thư viện  - @yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <base href="{{asset("")}}">

     <link rel="shortcut icon" type="image/x-icon" href="theme_page/images/logo1.png">
    
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800,900&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="theme_page/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="theme_page/css/animate.css">
    
    <link rel="stylesheet" href="theme_page/css/owl.carousel.min.css">
    <link rel="stylesheet" href="theme_page/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="theme_page/css/magnific-popup.css">

    <link rel="stylesheet" href="theme_page/css/aos.css">

    <link rel="stylesheet" href="theme_page/css/ionicons.min.css">
    
    <link rel="stylesheet" href="theme_page/css/flaticon.css">
    <link rel="stylesheet" href="theme_page/css/icomoon.css">
    <link rel="stylesheet" href="theme_page/css/style.css">

{{--     <script type="text/javascript" charset="utf-8" src="https://ajax.googleapis.com/ajax/libs/dojo/1.5.1/dijit/_base/sniff.xd.js">
      
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v5.0"></script> --}}
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>




    {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script> --}}

  </head>
  <body>


    @include('header')


    <div id="myDIV">
   
    @yield('content')

     
    
   @include('footer')
   {{-- @include('contact') --}}
<button onclick="topFunction()" id="myBtn" title="Go to top"> Top </button>

  <script src="theme_page/js/jquery.min.js"></script>
  <script src="theme_page/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="theme_page/js/popper.min.js"></script>
  <script src="theme_page/js/bootstrap.min.js"></script>
  <script src="theme_page/js/jquery.easing.1.3.js"></script>
  <script src="theme_page/js/jquery.waypoints.min.js"></script>
  <script src="theme_page/js/jquery.stellar.min.js"></script>
  <script src="theme_page/js/owl.carousel.min.js"></script>
  <script src="theme_page/js/jquery.magnific-popup.min.js"></script>
  <script src="theme_page/js/aos.js"></script>
  <script src="theme_page/js/jquery.animateNumber.min.js"></script>
  <script src="theme_page/js/scrollax.min.js"></script>
  <script src="theme_page/js/main.js"></script>


  <script>
    
    $('.carousel').carousel({
      interval: 30000
    })
  </script>

<script>
      $(document).ready(function(){
        $("#myInput").on("keyup", function() {
          var value = $(this).val().toLowerCase();
          $("#myDIV *").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
        });
      });
</script>


<!--Liên hệ mới -->
<script>
    
       $(document).ready(function(){
       $(".nut-btn").click(function(){
         $(".tieudeC").animate({bottom:'260',height:'100px'});
       });
       $(".dongthe").click(function(){
         $(".tieudeC").animate({bottom:'4px',height:'50px'});
       });
     });

</script>
<!--Liên hệ mới -->

<!--Liên hệ-->
<script>
    

       $(document).ready(function(){
       $(".quick-btn").click(function(){
         $(".quick-start-account").animate({right:'0'});
       });
       $(".closeForm").click(function(){
         $(".quick-start-account").animate({right:'-330px'});
       });
     });

</script>
    
<!--Buttom UpPages-->
<script>
      //Get the button
      var mybutton = document.getElementById("myBtn");

      // When the user scrolls down 20px from the top of the document, show the button
      window.onscroll = function() {scrollFunction()};

      function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
          mybutton.style.display = "block";
        } else {
          mybutton.style.display = "none";
        }
      }

      // When the user clicks on the button, scroll to the top of the document
      function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
      }
</script>

{{-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> --}}
<script type="text/javascript">
$(document).ready(function() {
    var elements = $("input");
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                 e.target.setCustomValidity(e.target.getAttribute("requiredmsg"));
            }
        };
        elements[i].oninput = function(e) {
            e.target.setCustomValidity("");
        };
    }
})
</script>

  </body>
</html>