<section class="ftco-section testimony-section">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-8 text-center heading-section ftco-animate">
            <h2 class="mb-4">TẠI SAO BẠN LÊN ĐỌC SÁCH? </h2>
          </div>
        </div>
        <div class="row ftco-animate justify-content-center">
         
          <div class="col-md-12">
            <div class="carousel-testimony owl-carousel">


            
              <div class="item" style="height: 280px;">
                <div class="testimony-wrap d-flex">
                  <div class="user-img" style="background-image: url(img/27qw_ToKillaMockingbirdposter.jpg)">
                  </div>
                  <div class="text pl-4">
                    <span class="quote d-flex align-items-center justify-content-center">
                      
                    </span>
                    <p style="overflow: hidden;height: 140px;"> “Nếu bạn chỉ đọc những cuốn sách mà tất cả mọi người đều đọc, bạn chỉ có thể nghĩ tới điều tất cả mọi người đều nghĩ tới.”</p> 
                    
                  </div>

                
                </div>
                <br>
                
              </div>
            

              <div class="item" style="height: 280px;">
                <div class="testimony-wrap d-flex">
                  <div class="user-img" style="background-image: url(img/2lux_cau-be-nha-que.png)">
                  </div>
                  <div class="text pl-4">
                    <span class="quote d-flex align-items-center justify-content-center">
                      
                    </span>
                    <p style="overflow: hidden;height: 140px;"> Những gì sách dạy chúng ta cũng giống như lửa. Chúng ta lấy nó từ nhà hàng xóm, thắp nó trong nhà ta, đem nó truyền cho người khác, và nó trở thành tài sản của tất cả mọi người</p> 
                    
                  </div>

                
                </div>
                <br>
                
              </div>



              <div class="item" style="height: 280px;">
                <div class="testimony-wrap d-flex">
                  <div class="user-img" style="background-image: url(img/6RQA_download.jpg)">
                  </div>
                  <div class="text pl-4">
                    <span class="quote d-flex align-items-center justify-content-center">
                      
                    </span>
                    <p style="overflow: hidden;height: 140px;">  “Không cần phải đốt sách để phá hủy một nền văn hóa. Chỉ cần buộc người ta ngừng đọc mà thôi.”</p> 
                    
                  </div>

                
                </div>
                <br>
                
              </div>


            </div>
          </div>
        </div>
      </div>
    </section>