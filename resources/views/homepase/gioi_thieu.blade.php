@extends('master')
@section('content')
<div class="inner-header">
		<div class="container">
			<div class="pull-left">
				<h6 class="inner-title">Giới thiệu</h6>
			</div>
			<div class="pull-right">
				<div class="beta-breadcrumb font-large">
					<a href="index.html">Home</a> / <span>Giới thiệu</span>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="container">
		<div id="content">
			<div class="our-history">
				<h2 class="text-center wow fadeInDown">Our History</h2>
				<div class="space35">&nbsp;</div>

				<div class="history-slider">
					

					<div class="history-slides">
						<div> 
							<div class="row">
							<div class="col-sm-5">
								<img src="assets/dest/images/history.jpg" alt="">
							</div>
							<div class="col-sm-7">
								<h5 class="other-title">Giới thiệu</h5>
								<p>
									Quản Lý Thư Viện
								</p>
								<div class="space20">&nbsp;</div>
								<p>Thư viện là kho sưu tập sách, báo và tạp chí. Tuy nó có thể chỉ đến kho sưu tập cá nhân của người riêng, nhưng nó thường chỉ đến nhà lớn sưu tập sách báo xuất bản được bảo quản bởi thành phố hay học viện hay nhận tiền góp của họ. Những nhà sưu tập này thường được sử dụng bởi những người không muốn (hay không có thể) mua nhiều sách cho mình.
									<br>
								Do đó, các thư viện hiện đại ngày càng trở thành nơi để truy cập thông tin vô hạn chế bằng nhiều cách khác nhau. 
								Song bên cạnh đó phương tiện đi lại và thời gian hạn chế hiện nay đã trở nên bất cập và khó khăn hơn trong việc tìm kiếm và lựa chọn. Nên Thư viện điện thử đã được sinh ra để trở thành một ưu điểm lớn cho các thư viện hiện nay. Người sử dụng có thể quản lý các đầu sách, vật tư, nhân viên. Khách ghé thăm có thể tìm kiếm dễ dàng hơn cuốn sách mình cần nằm ở chỗ nào và tới lấy. 
								</p>
								<br>
								<br>
								<br>
							</div>
							</div> 
						</div>
					</div>
				</div>
			</div>

			
			</div>
		</div> <!-- #content -->
	</div> <!-- .container -->
	@endsection