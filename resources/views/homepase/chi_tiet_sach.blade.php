@extends('master')
@section('content')
<div class="inner-header">
		<div class="container">
			<div class="pull-left">
				<h6 class="inner-title">Sách {{$chi_tiet_san_pham->ten_sach}}</h6>
			</div>
			
		</div>
	</div>

<div class="container">
		<div id="content">
			<div class="row">
				<div class="col-sm-9">

					<div class="row">

						<div class="col-sm-4">
							<img src="img/{{$chi_tiet_san_pham->anh}}" alt="" style="width: 150%">
						</div>
						<div class="col-sm-8" style="margin-right: 50px">
							<div class="single-item-body">
								<p class="single-item-title"><b><h2 style="color:#FF9900">{{$chi_tiet_san_pham->ten_sach}}</h2></b></p>
	
										<div class="single-item-body">
											
											<p class="single-item-price">
												
												<span class="flash-del">Giá tiền: <?php echo number_format($chi_tiet_san_pham->gia_tien_cu); ?>VNĐ</span>
												
											</p>
											<p class="single-item-price">
												
												<span class="flash-del">{{$chi_tiet_san_pham->phong['ten_phong']}}</span>
												
											</p>
										</div>
							<div class="clearfix"></div>
							<div class="space20">&nbsp;</div>
							<br>
							<div class="single-item-desc">
									<b><h1 style="font-size: 20px">Giới Thiệu Sách</h1> </b>
								<div class="panel" id="tab-description">
								<p>
									{!!$chi_tiet_san_pham->gioi_thieu!!}</p>
								</div>
							</div>
							<!-- <div class="space20">&nbsp;</div>
								<p><b >Số Lượng Đặt Mua:</b></p>
							<br> -->
							<!-- <div class="single-item-options">
							
								<select class="wc-select" name="color">
									
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
								</select>
								
								<a class="add-to-cart pull-left" href="{{route ('them_gioi_hang',$chi_tiet_san_pham->ma_sach)}}"><i class="fa fa-shopping-cart"></i></a>
							</div> -->
						</div>
						<div class="clearfix"></div>
						<div class="space40">&nbsp;</div>
						<div class="single-item-desc" style="padding-right: -10px">
							<b><h1 style="font-size: 20px">Đọc tham khảo</h1> </b>
							<div class="panel" id="tab-description">
								<p style="font-color: black">
									{!!$chi_tiet_san_pham->noi_dung!!}</p>
							</div>
						</div>
					</div>
					
					<!-- best sellers widget -->
				</div>
				<div class="beta-products-list" >
						<b><h4>Sản Phẩm Cùng Loại</h4></b>
<br><Br>
						<div class="row">
							
							@foreach($san_pham_cung_loai as $spcl)
								<div class="col-sm-4">
									<div class="single-item">
										<!-- @if($spcl->gia_tien_moi!= 0)
										<div class="ribbon-wrapper">
											<div class="ribbon sale">Sale</div>
										</div>
										@endif -->
										<div class="single-item-header">
											<a href="{{route ('chi_tiet_sach',$spcl->ma_sach)}}"><img src="img/{{$spcl->anh}}" alt="" height="280px" width="250px"></a>
										</div>
										
										<div class="single-item-body">
											<p class="single-item-title">{{$spcl->ten_sach}}</p>
											<!-- <p class="single-item-price" style="font-size: 18px">
												@if($spcl->gia_tien_moi== 0)
												<span class="flash-sale"><?php echo number_format($spcl->gia_tien_cu); ?>VNĐ</span>
												@else
												<span class="flash-del"><?php echo number_format($spcl->gia_tien_cu); ?>VNĐ</span>
												<span class="flash-sale"><?php echo number_format($spcl->gia_tien_moi); ?>VNĐ</span>
												@endif
											</p> -->
										</div>
										<!-- <div class="single-item-caption">
											<a class="add-to-cart pull-left" href="{{route ('them_gioi_hang',$spcl->ma_sach)}}"><i class="fa fa-shopping-cart"></i></a>
											
										</div> -->
									</div>
								</div>
								@endforeach
							</div>	
							<div class="row">
								{{$san_pham_cung_loai->links()}}
							</div>
							
						</div>
						<!-- <!-- <div class="col-sm-3 aside">
					<div class="widget">
						<h3 class="widget-title">Best Sellers</h3
						<div class="widget-body">
							<div class="beta-sales beta-lists">
								<div class="media beta-sales-item">
									<a class="pull-left" href="product.html"><img src="assets/dest/images/products/sales/1.png" alt=""></a>
									<div class="media-body">
										Sample Woman Top
										<span class="beta-sales-price">$34.55</span>
									</div>
								</div>
								<div class="media beta-sales-item">
									<a class="pull-left" href="product.html"><img src="assets/dest/images/products/sales/2.png" alt=""></a>
									<div class="media-body">
										Sample Woman Top
										<span class="beta-sales-price">$34.55</span>
									</div>
								</div>
								<div class="media beta-sales-item">
									<a class="pull-left" href="product.html"><img src="assets/dest/images/products/sales/3.png" alt=""></a>
									<div class="media-body">
										Sample Woman Top
										<span class="beta-sales-price">$34.55</span>
									</div>
								</div>
								<div class="media beta-sales-item">
									<a class="pull-left" href="product.html"><img src="assets/dest/images/products/sales/4.png" alt=""></a>
									<div class="media-body">
										Sample Woman Top
										<span class="beta-sales-price">$34.55</span>
									</div>
								</div>
							</div>
						</div>
					</div> <!-- best sellers widget -->
					<!-- <div class="widget">
						<h3 class="widget-title">New Products</h3>
						<div class="widget-body">
							<div class="beta-sales beta-lists">
								<div class="media beta-sales-item">
									<a class="pull-left" href="product.html"><img src="assets/dest/images/products/sales/1.png" alt=""></a>
									<div class="media-body">
										Sample Woman Top
										<span class="beta-sales-price">$34.55</span>
									</div>
								</div>
								<div class="media beta-sales-item">
									<a class="pull-left" href="product.html"><img src="assets/dest/images/products/sales/2.png" alt=""></a>
									<div class="media-body">
										Sample Woman Top
										<span class="beta-sales-price">$34.55</span>
									</div>
								</div>
								<div class="media beta-sales-item">
									<a class="pull-left" href="product.html"><img src="assets/dest/images/products/sales/3.png" alt=""></a>
									<div class="media-body">
										Sample Woman Top
										<span class="beta-sales-price">$34.55</span>
									</div>
								</div>
								<div class="media beta-sales-item">
									<a class="pull-left" href="product.html"><img src="assets/dest/images/products/sales/4.png" alt=""></a>
									<div class="media-body">
										Sample Woman Top
										<span class="beta-sales-price">$34.55</span>
									</div>
								</div>
							</div>
						</div>
					</div> <!-- best sellers widget -->
				</div>
			</div>
		</div> <!-- #content -->
	</div> 
	@endsection