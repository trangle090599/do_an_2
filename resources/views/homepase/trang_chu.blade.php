@extends('master')
@section('title')
Trang chủ
@endsection
@section('content')
<!--Page-Content-->  
<!--Slide-->
@include('slide')

<section class="ftco-section" style="margin-top: -130px">
      <div class="container">
        <div class="row d-flex">
          <div class="col-md-12 wrap-about pr-md-4 ftco-animate">
            <h2 class="mb-4 text-center">Dịch Vụ Của Thư Viện</h2>
            <p class="text-center">Thư Viện Bách Khoa - Dịch vụ chuyên nghiệp - Công nghệ vượt trội</p>
          </div>
        </div>
      </div>
    </section>
    
    <section class="ftco-no-pb img" style="background-image: url(img/book_PNG2115.jpg); height: 200px">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-10 text-center heading-section heading-section-white ftco-animate">
            
          </div>
        </div>  
      </div>
    </section>

    <section class="ftco-counter" id="section-counter">
      <div class="container">
        <div class="row d-md-flex align-items-center justify-content-center">
          <div class="wrapper">
            <div class="row d-md-flex align-items-center">
              <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18">
                  <div class="icon"><span class="flaticon-doctor"></span></div>
                  <div class="text">
                    <strong class="number" data-number="204">0</strong>
                    <span>Số Sách Trong Thư Viện</span>
                  </div>
                </div>
              </div>
              <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18">
                  <div class="icon"><span class="flaticon-doctor"></span></div>
                  <div class="text">
                    <strong class="number" data-number="420">0</strong>
                    <span>Số Sách Đã Nhập Về</span>
                  </div>
                </div>
              </div>
              <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18">
                  <div class="icon"><span class="flaticon-doctor"></span></div>
                  <div class="text">
                    <strong class="number" data-number="15">0</strong>
                    <span>GIẢI THƯỞNG ĐÃ NHẬN</span>
                  </div>
                </div>
              </div>
              <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18">
                  <div class="icon"><span class="flaticon-doctor"></span></div>
                  <div class="text">
                    <strong class="number" data-number="60">0</strong>
                    <span>NĂM KINH NGHIỆM</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>


<section class="ftco-section ftco-no-pb">
   <div class="container-fluid px-0">
      <div class="row no-gutters justify-content-center mb-5">
         <div class="col-md-7 text-center heading-section ftco-animate">
            <h3 class="mb-4">Bạn cô đơn ư? Đừng lo lắng. Mọi cuốn sách đều sẵn sàng kết thân với bạn!</h3>
         </div>
      </div>
      <div class="row no-gutters">
         <div class="col-md-3">
            <div class="project img ftco-animate d-flex justify-content-center align-items-center" style="background-image: url(img/Bookshelves_7.jpg);">
               <a href="#" class="btn-site d-flex align-items-center justify-content-center"><span class="icon-subdirectory_arrow_right"></span></a>
            </div>
         </div>
         <div class="col-md-3">
            <div class="project img ftco-animate d-flex justify-content-center align-items-center" style="background-image: url(img/dung-bo-phi-cuon-sach-ma-ban-thich-1.jpg);">
               <a href="#" class="btn-site d-flex align-items-center justify-content-center"><span class="icon-subdirectory_arrow_right"></span></a>
            </div>
         </div>
         <div class="col-md-3">
            <div class="project img ftco-animate d-flex justify-content-center align-items-center" style="background-image: url(img/anh2.jpg);">
            </div>
         </div>
         <div class="col-md-3">
            <div class="project img ftco-animate d-flex justify-content-center align-items-center" style="background-image: url(img/logo1.png);">
               <a href="#" class="btn-site d-flex align-items-center justify-content-center"><span class="icon-subdirectory_arrow_right"></span></a>
            </div>
         </div>
      </div>
   </div>
</section>

    <section class="ftco-section bg-light">
      <div class="container">
        <div class="row justify-content-center mb-5 pb-2">
          <div class="col-md-8 text-center heading-section ftco-animate">
            <h2 class="mb-4">CÂU NÓI HAY VỀ SÁCH </h2>
            <p>"Cuốn sách tốt nhất cho bạn là cuốn sách nói nhiều nhất với bạn vào lúc bạn đọc nói. Tôi không nói tới cuốn sách cho bạn nhiều bài học nhất mà là cuốn sách nuôi dưỡng tâm hồn bạn. Và điều đó phụ thuộc vào tuổi tác, trải nghiệm, nhu cầu về tâm lý và tinh thần."</p>
            <br>
            <p> "Một cuốn sách thực sự hay nên đọc trong tuổi trẻ, rồi đọc lại khi đã trưởng thành, và một nửa lúc tuổi già, giống như một tòa nhà đẹp nên được chiêm ngưỡng trong ánh bình minh, nắng trưa và ánh trăng.”</p>
          </div>
        </div>
      </div>
    </section>

    @include('rating')

@endsection