@extends('master')
@section('content')
	<div class="container">
		<div id="content">
			@if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all()
                            as $err)
                            {{$err}}<br>
                            @endforeach
                        </div>
                        @endif

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
			<form action="{{route ('dat_hang')}}" method="post" class="beta-form-checkout">
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<div class="row">
					<div class="col-sm-6">
						<h4>Đặt hàng</h4>
						<div class="space20">&nbsp;</div>
						<div class="form-group">
                            <label>Độc Giả</label>
                            <select class="form-control" name="user">
                              @foreach ($user as $us)
                                <option value="{{ $us->ma_user }}">
                                    {{$us->ten_user}}
                                </option>
                            @endforeach    
                            </select>
                        </div>
						
                        <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" placeholder="Nhập email" />
                            </div>
                             <div class="form-group">
                                <label>Địa Chỉ</label>
                                <input class="form-control" name="dia_chi" placeholder="Nhập địa chỉ" />
                            </div>
                            <div class="form-group">
                                <label>Ngày Mua</label>
                                <input type = "date"class="form-control" name="ngay_mua" placeholder="Nhập ngày mua" />
                            </div>
                             <div class="form-group">
                                <label>SĐT</label>
                                <input type="number" class="form-control" name="sdt" placeholder="Nhập số điện thoại" />
                            </div>
                           
                            <div class="form-group">
                                <label>Giới Tính</label>
                                <label class="radio-inline">
                                    <input name="gioi_tinh" value="0" checked="" type="radio">Nam
                                </label>
                                <label class="radio-inline">
                                    <input name="gioi_tinh" value="1" type="radio">Nữ
                                </label>
                            </div>
						<div class="form-group">
							<label >Ghi chú</label>
							<textarea id="notes" name="notes"></textarea>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="your-order">
							<div class="your-order-head"><h5>Đơn hàng của bạn</h5></div>
							<div class="your-order-body" style="padding: 0px 10px">
								<div class="your-order-item">
									<div>
									@if(Session::has('cart'))
									@foreach($product_cart as $product )
										<div class="media">
											<img width="35%" height="200px" src="img/{{$product['item']['anh']}}" alt="" class="pull-left">
											<div class="media-body">
												
												<p class="font-large">Tên Sách:  {{$product['item']['ten_sach']}}</p>
												
												<br>
												<span class="font-large">Số Lượng:  {{$product['qty']}}</span>
												<br>
												<br>
												<span class="font-large">
												<h>Giá Tiền: 
												@if($product['item']->gia_tien_moi == $product['item']->gia_tien_cu)
												<span class="flash-sale"><?php echo number_format($product['item']['gia_tien_cu']); ?>VNĐ</span>
												@else
												<span class="flash-sale"><?php echo number_format($product['item']['gia_tien_moi']); ?>VNĐ</span>
												</h>
												@endif
											</span>
											</div>
										</div>
										<div class="your-order-item">
									<div class="pull-left"><p class="your-order-f18">Tổng tiền:</p></div>
									<div class="pull-right"><h5 class="color-black">{{number_format(Session('cart')->totalPrice)}} VNĐ</h5></div>
									<div class="clearfix"></div>
								</div>
									<!-- end one item -->
									@endforeach
									@endif
							
									</div>
									
								</div>
								
							</div>
							<div class="your-order-head"><h5>Hình thức thanh toán</h5></div>
							
							<div class="your-order-body">
								<ul class="payment_methods methods">
									<li class="payment_method_bacs">
										<input id="payment_method_bacs" type="radio" class="input-radio" name="payment_method" value="COD" checked="checked" data-order_button_text="" name="hinh_thuc_thanh_toan">
										<label for="payment_method_bacs">Thanh toán khi nhận hàng </label>
										<div class="payment_box payment_method_bacs" style="display: block;">
											Cửa hàng sẽ gửi hàng đến địa chỉ của bạn, bạn xem hàng rồi thanh toán tiền cho nhân viên giao hàng
										</div>						
									</li>

									<li class="payment_method_cheque">
										<input id="payment_method_cheque" type="radio" class="input-radio" name="payment_method" value="ATM" data-order_button_text="" name="hinh_thuc_thanh_toan">
										<label for="payment_method_cheque">Chuyển khoản </label>
										<div class="payment_box payment_method_cheque" style="display: none;">
											Chuyển tiền đến tài khoản sau:
											<br>- Số tài khoản: 123 456 789
											<br>- Chủ TK: Nguyễn A
											<br>- Ngân hàng ACB, Chi nhánh TPHCM
										</div>						
									</li>
									
								</ul>
							</div>

							<div class="text-center"><button tyle="submit"  class="beta-btn primary">Đặt hàng <i class="fa fa-chevron-right"></i></button></div>
						</div> <!-- .your-order -->
					</div>
				</div>
			</form>
		</div> <!-- #content -->
	</div> <!-- .container -->
	@endsection