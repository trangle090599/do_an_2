@extends('master')
@section('content')

<div class="container">
		<div id="content">
				<div class="row">
					<div class="col-sm-3"></div>
					<div class="col-sm-6">
						<h1 align="center">Đăng Ký Độc Giả</h1>
						<div class="space20">&nbsp;</div>
						<div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all()
                            as $err)
                            {{$err}}<br>
                            @endforeach
                        </div>
                        @endif

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                        <form action="{{route('dang_ky')}}" method="POST" class="beta-form-checkout" />
                        <input type="hidden" name="_token" value="{{csrf_token()}}">

                        <div class="form-group" style="width:500px">
                            <label>Ngành</label>
                            <select class="form-control" name="nganh">
                              @foreach ($nganh as $ng)
                                <option value="{{ $ng->ma_nganh }}">
                                    {{$ng->ten_nganh}}
                                </option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group" style="width:500px">
                                <label>Tên độc giả</label>
                                <input class="form-control" name="ten_user" placeholder="Nhập tên độc giả" />
                        </div>
                        <div class="form-group" style="width:500px">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" placeholder="Nhập email" />
                            </div>
                            <div class="form-group" style="width:500px">
                                <label>Mật Khẩu</label>
                                <input type = "password"class="form-control" name="password" placeholder="Nhập mật khẩu" />
                            </div>
                             <div class="form-group" style="width:500px">
                                <label>Nhập lại Mật Khẩu</label>
                                <input type = "password"class="form-control" name="passwordAgain" placeholder="Nhập mật lại khẩu" />
                            </div>
                             <div class="form-group" style="width:500px">
                                <label>Địa Chỉ</label>
                                <input class="form-control" name="dia_chi" placeholder="Nhập địa chỉ" />
                            </div>
                             <div class="form-group" style="width:500px">
                                <label>Ngày Sinh</label>
                                <input type = "date"class="form-control" name="ngay_sinh" placeholder="Nhập ngày sinh" />
                            </div>
                             <div class="form-group" style="width:500px">
                                <label>SĐT</label>
                                <input type="number" class="form-control" name="sdt" placeholder="Nhập số điện thoại" />
                            </div>

                            <div class="form-group" style="width:500px">
                                <label>Giới Tính</label>
                                <label class="radio-inline">
                                    <input name="gioi_tinh" value="0" checked="" type="radio">Nam
                                </label>
                                <label class="radio-inline">
                                    <input name="gioi_tinh" value="1" type="radio">Nữ
                                </label>
                            </div>
                            <div class="form-group" style="width:500px">
                                <label>Bạn là </label>
                                <label class="radio-inline">

                                    <input name="cap_do" value="3" checked="" type="radio">Khách Hàng
                                </label>

                            </div>

                            <div class="form-block" >
							<button type="submit" class="btn btn-primary" >Đăng Ký</button>
							</div>

                        <form>
                    </div>
					</div>
					<div class="col-sm-3"></div>
				</div>
			</form>
		</div> <!-- #content -->
	</div> <!-- .container -->
	@endsection
