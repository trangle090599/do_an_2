@extends('master')
@section('title')
Loại tin
@endsection

@section('content')
   <section class="hero-wrap " style="margin-bottom: -530px" >
      
      
    </section>


    <section class="ftco-section bg-light">
         <div class="container">
            <div class="row">

 
     @foreach($the_loai_sach as $tls)
          <div class="col-md-6 col-lg-4 ftco-animate" style="width: 400px">
            <div class="blog-entry" style="width: 18rem;">
        <div class="anh" >
              <a href="{{route ('them_gioi_hang',$tls->ma_sach)}}" class="block-20 d-flex align-items-end"><img style="height: 300px;width: 100%;" src="img/{{$tls->anh}}">
            </a>
          </div>
              <div class="text bg-white p-4">
                <div class="chu" style="font-size: 16px; ">
                <b><p  style="height: 25px;">{{$tls->ten_sach}}</a></p></b>
                <p>{{$tls->phong['ten_phong']}}</p>
              </div>
                <div class="d-flex align-items-center mt-4">
                   <p class="mb-0"><a href="{{route ('chi_tiet_sach',$tls->ma_sach)}}" class="btn btn-primary">Xem chi tiết <span class="ion-ios-arrow-round-forward"></span></a></p>
                </div>
              </div>
            </div>
          </div>
          @endforeach
          
        </div>
        <div class="row mt-5">
          <div class="col text-center">
            <div class="block-27">
              {{$the_loai_sach->links()}}
            </div>
          </div>
        </div>
         </div>
      </section>

      @include('rating')
@endsection
<style type="text/css" media="screen">
  .anh {

transition: all 0.5s ease-in-out 0s;
font-size: 16px



}


.anh:hover {background-color:#E8E800;transform: rotateY(180deg);-webkit-transform: rotateY(180deg);-moz-transform: rotateY(180deg);-ms-transform: rotateY(180deg);-o-transform: rotateY(180deg);}



</style>