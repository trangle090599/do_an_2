 <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-4">
            <div class="ftco-footer-widget mb-5">
              <h2 class="ftco-heading-2">Liện hệ theo địa chỉ:</h2>
              <div class="block-23 mb-3">
                <ul>
                  <li><span class="icon icon-map-marker"></span><span class="text">Bắc Ninh</span></li>
                  <li><span class="icon icon-map-marker"></span><span class="text">Bát tràng</span></li>
                  <li><span class="icon icon-map-marker"></span><span class="text">Hà Nội</span></li>
                  <li><a href="#"><span class="icon icon-phone"></span><span class="text">01234528982</span></a></li>
                  <li><a href="#"><span class="icon icon-envelope"></span><span class="text">contact@lientrang.dou</span></a></li>
                </ul>
              </div>
            </div>
          </div>
          
          <div class="col-md-6 col-lg-4">
            <div class="ftco-footer-widget  ml-md-4">
              <h2 class="ftco-heading-2">Thông tin về chúng tôi:</h2>
              <ul class="list-unstyled">
                <li><a href=""><span class="ion-ios-arrow-round-forward mr-2"></span>Trang chủ</a></li>
                 
              </ul>
            </div>
          </div>

          <div class="col-md-6 col-lg-4">
            <div class="ftco-footer-widget ml-md-4">
              <h2 class="ftco-heading-2">Fanpage:</h2>
              <ul class="ftco-footer-social list-unstyled float-md-left float-left">
                <li class="ftco-animate"><a href=""><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href=""><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-youtube"></span></a></li>
              </ul>
            <div class="fb-page" data-href="" data-tabs="timeline" data-width="350px" data-height="135px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="" class="fb-xfbml-parse-ignore"><a href="">Thư Viện</a></blockquote></div>
          </div>
          </div>
          
        </div>
        <div class="row coppyright">
          <div class="col-md-12 text-center">
            <p>{{-- &copy;<script>document.write(new Date().getFullYear());</script> --}}   <a href="" target="_blank"> With love </a></p>
          </div>
        </div>
      </div>
    </footer>
    
