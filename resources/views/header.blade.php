<div class="top-nav-fixed" style="position: fixed;width: 100%;top: 0;left: 0;right: 0;z-index: 1000;">
   <div class="top-contact">
      <div class="max-top-head">
         <div class="uk-clearfix">
            <ul class="infor-top">
               <li>
                  <a href="tel:(949) 480-1639"><i class="icon icon-phone"></i>
                  <span>(+84<span></span>) 696<span></span> 859 422</span></a>
               </li>
               <li>
                  <a class="trigger-scroll" href="mailto:contact@thuanthanhtech.com"><i class="icon icon-envelope"
                     aria-hidden="true"></i>
                  contact@.com</a>
               </li>
            </ul>
         </div>
      </div>
   </div>
   <div class="top-menu">
      <div class="max-top-head">
         <div class="row">
            <div class="col-md-2 d-flex align-items-center align-middle logothuanthanh">
               <a class="navbar-brand" href="index"><img src="img/logo2.png" style="height: 65px;"></a>
            </div>
            <nav class="col-md-10 navbar navbar-expand-lg navbar-light  ftco-navbar-light" id="ftco-navbar">
               <div class="container d-flex align-items-center">
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                     aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="oi oi-menu"></span> Menu
                  </button>
                  <div class="collapse navbar-collapse" id="ftco-nav">
                        <ul class="navbar-nav mr-auto">
                        
                        <li class="nav-item dropdown">
                          <a class="nav-link dropdown-toggle" role="button" >
                           Thể Loại sách
                           </a>
                            <div class="dropdown-menu bg-light" aria-labelledby="navbarDropdown">
                              @foreach($the_loai_sach as $tls)
                                <ul class="navbar-nav ">
                                   <li class="nav-item dropdown"> <a class="dropdown-item" href="{{route ('the_loai_sach',['id' => $tls])}}" >{{$tls->ten_the_loai_sach}}</a></li>
                                </ul>
                                @endforeach
                           </div>
                        </li>
                        
                        <li class="nav-item">
                           <a class="nav-link" href="gioi_thieu">Giới Thiệu</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" href="lien_he">Liên hệ</a>
                        </li>
                       
                        {{--! Thay đổi ngôn ngữ--}}
                         
                        {{-- <li class="language">
                           <a href="{{URL::asset('')}}locale/vi" class="nav-link" ><img src="theme_page/images/tiengviet.png" style="height: 28px;"></a>
                        </li>
                        <li class="language">
                           <a href="{{URL::asset('')}}locale/en" class="nav-link"><img src="theme_page/images/tienganh.png" style="height: 20px;"></a>
                        </li>
                        <li class="language">
                           <a href="{{URL::asset('')}}locale/japan" class="nav-link"><img src="theme_page/images/tiengnhat.png" style="height: 20px;"></a>
                        </li> --}}
                       
                       
                     </ul>
                  </div>
               </div>
            </nav>


         </div>
      </div>
   </div>
</div>

<div style="clear: both;"></div>